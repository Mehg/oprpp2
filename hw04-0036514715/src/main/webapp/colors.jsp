<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Color chooser</title>
    <c:url value="/style.jsp" var="style" />
    <link rel="stylesheet" href=${style}>
</head>

<body>
<ul style="list-style-type: none">
    <li><a href="setcolor?pickedBgCol=FFFFFF">WHITE</a></li>
    <li><a href="setcolor?pickedBgCol=D44F49">RED</a></li>
    <li><a href="setcolor?pickedBgCol=2EB398">GREEN</a></li>
    <li><a href="setcolor?pickedBgCol=a6ffff">CYAN</a></li>
    <li><a href="setcolor?pickedBgCol=e8e8e8">GRAY</a></li>
</ul>


</body>
</html>
