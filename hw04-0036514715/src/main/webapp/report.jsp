<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <c:url value="/style.jsp" var="style" />
    <link rel="stylesheet" href=${style}>
    <title>Report</title>
</head>
<body>
<h1>Os usage</h1>
<p>Here are the results of OS usage in survey that we completed</p>
<img src="${pageContext.request.contextPath}/reportImage" alt="Image of OS usage">

</body>
</html>
