<%@ page import="java.time.Duration" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%!
    private String getElapsedTime(){
        LocalDateTime start = (LocalDateTime) getServletConfig().getServletContext().getAttribute("start");
        LocalDateTime current = LocalDateTime.now();


        Duration duration = Duration.between(start, current).abs();
        return duration.toDaysPart()+" days "+duration.toHoursPart()+" hours " +duration.toMinutesPart()+" minutes "+duration.toSecondsPart()+" seconds "+duration.toMillisPart()+" milliseconds";
    }
%>
<html>
<head>
    <c:url value="/style.jsp" var="style" />
    <link rel="stylesheet" href=${style}>
    <title>App info</title>
</head>
<body>
<p>App has been running for: <%= getElapsedTime()%> </p>
</body>
</html>
