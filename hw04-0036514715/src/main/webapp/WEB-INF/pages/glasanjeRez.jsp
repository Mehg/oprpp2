<%@ page import="java.util.List" %>
<%@ page import="hr.fer.oprpp2.hw04.servlets.glasanje.Band" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%!
    private List<Band> getBest(List<Band> given) {
        int max = given.get(0).getVotes();
        return given.stream().filter(b->b.getVotes()==max).collect(Collectors.toList());
    }
%>
<html>
<head>
    <c:url value="/style.jsp" var="style"/>
    <link rel="stylesheet" href=${style}>
    <title>Rezultati glasanja</title>
    <style>
        table {
            text-align: center;
            border: 1px solid black;
        }
        td{
            border: 1px solid black;
        }
    </style>
</head>
<body>
<h1>Rezultati glasanja</h1>
<p>Ovo su rezultati glasanja.</p>
<table>
    <thead>
    <tr>
        <th>Bend</th>
        <th>Broj glasova</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="band" items="${bands}">
        <tr>
            <td>${band.name}</td>
            <td>${band.votes}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<h2>Grafički prikaz rezultata</h2>
<img src="${pageContext.request.contextPath}/glasanje-grafika" alt="Pie-chart" width="400" height="400"/>

<h2>Rezultati u XLS formatu</h2>
<p>Rezultati u XLS formatu dostupni su <a href="${pageContext.request.contextPath}/glasanje-xls">ovdje</a></p>

<h2>Razno</h2>
<p>Primjeri pjesama pobjedničkih bendova:</p>

<ul>
    <c:forEach var="band" items='<%= getBest((List<Band>) request.getAttribute("bands"))%>'>
        <li><a href="${band.link}" target="_blank">${band.name}</a></li>
    </c:forEach>
</ul>
</body>
</html>