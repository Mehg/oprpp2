<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <c:url value="/style.jsp" var="style" />
    <link rel="stylesheet" href=${style}>
    <title>Trigonometric functions</title>
</head>
<body>
<table>
    <thead>
    <tr>
        <th>x</th>
        <th>sin(x)</th>
        <th>cos(x)</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="entry" items="${SinAndCos}">
    <tr><td>${entry.degree}</td><td>${entry.sine}</td><td>${entry.cosine}</td></tr>
    </c:forEach>
    </tbody>
</table>

</body>
</html>
