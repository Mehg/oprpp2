<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>Index</title>
    <link rel="stylesheet" href="style.jsp">
</head>
<body>

<ul style="list-style-type: none">
    <li><a href="${pageContext.request.contextPath}/colors.jsp">Background color chooser</a></li>
    <li><a href="${pageContext.request.contextPath}/trigonometric?a=0&b=90">Trigonometric functions</a></li>
    <li><a href="${pageContext.request.contextPath}/stories/funny.jsp">Funny story</a></li>
    <li><a href="${pageContext.request.contextPath}/report.jsp">Report</a></li>
    <li><a href="${pageContext.request.contextPath}/powers?a=1&b=100&n=3">Powers</a></li>
    <li><a href="${pageContext.request.contextPath}/appinfo.jsp">App info</a></li>
</ul>

<hr>

<form action="trigonometric" method="GET">
    Početni kut:<br><input type="number" name="a" min="0" max="360" step="1" value="0"><br>
    Završni kut:<br><input type="number" name="b" min="0" max="360" step="1" value="360"><br>
    <input type="submit" value="Tabeliraj"><input type="reset" value="Reset">
</form>

</body>
</html>
