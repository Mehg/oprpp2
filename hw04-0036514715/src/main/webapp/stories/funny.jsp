<%@ page import="java.util.Random" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%!
    private static final String[] list = {"#ff2424", "#ff7c24", "#24ff28", "#2469ff", "#9924ff", "#ff24f0", "#000000", "#404040"};
    private static final List<String> colors = List.of(list);

    private String getColor() {
        int index = new Random().nextInt(colors.size());
        return colors.get(index);
    }
%>
<html>
<head>
    <title>Funny story</title>
    <c:url value="/style.jsp" var="style" />
    <link rel="stylesheet" href=${style}>
    <style>
        body {
            color: <%= getColor()%>;
        }
    </style>
</head>
<body>
<p>What do you call sad coffee?</p>
<p>Depresso.</p>
</body>
</html>
