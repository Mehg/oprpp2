<%@ page contentType="text/css;charset=UTF-8" pageEncoding="UTF-8" %>

body {
    background-color: <%= session.getAttribute("pickedBgCol") %>;
    font-family: 'Roboto', sans-serif;
}

a{
    color: black;
    text-decoration: none;
}
a:hover{
    text-decoration: underline;
}