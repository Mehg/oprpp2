package hr.fer.oprpp2.hw04.servlets.glasanje;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Class which models a band
 */
public class Band {
    /**
     * Band id
     */
    int id;
    /**
     * Band name
     */
    String name;
    /**
     * URL to a song of the band
     */
    String link;
    /**
     * earned votes
     */
    int votes;

    /**
     * Constructs a band from given parameters
     *
     * @param id   band ID
     * @param name band name
     * @param link URL of song
     */
    public Band(String id, String name, String link) {
        this.id = Integer.parseInt(id);
        this.name = name;
        this.link = link;
    }

    /**
     * Returns the band ID
     *
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * Returns band name
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the URL to a song of the band
     *
     * @return link
     */
    public String getLink() {
        return link;
    }

    /**
     * Returns earned votes
     *
     * @return votes
     */
    public int getVotes() {
        return votes;
    }

    /**
     * Exctracts a band from given string
     *
     * @param bandString given string
     * @return new band from given string
     */
    private static Band fromString(String bandString) {
        String[] bandParts = bandString.split("\t");
        return new Band(bandParts[0], bandParts[1], bandParts[2]);
    }

    /**
     * Parses all bands from given file
     *
     * @param path given path to file
     * @return ArrayList of parsed bands
     * @throws IOException if something went wrong with reading files
     */
    private static ArrayList<Band> getFromFile(Path path) throws IOException {
        List<String> fileString = Files.readAllLines(path);
        ArrayList<Band> bands = new ArrayList<>();

        for (String part : fileString) {
            bands.add(Band.fromString(part));
        }

        bands.sort(Comparator.comparing(Band::getId));
        return bands;
    }

    /**
     * Loads bands from default definition file and sets them as a request attribute
     *
     * @param request given request
     * @throws IOException if something went wrong while reading from file
     */
    public static void loadBands(HttpServletRequest request) throws IOException {
        String fileName = request.getServletContext().getRealPath(Keys.glasanjeDefinicija);
        ArrayList<Band> bands = Band.getFromFile(Path.of(fileName));

        request.setAttribute("bands", bands);
    }

    /**
     * Loads votes from default votes file. Sets band votes from request attribute bands
     *
     * @param request given request
     * @throws IOException if something went wrong while reading from file
     */
    public static void loadVotes(HttpServletRequest request) throws IOException {
        String fileName = request.getServletContext().getRealPath(Keys.glasanjeRezultati);

        if (!Files.exists(Path.of(fileName))) {
            Files.createFile(Path.of(fileName));
        }

        List<String> bandVotes = Files.readAllLines(Path.of(fileName));
        Map<Integer, Integer> bandVoteMap = new HashMap<>();
        for (String string : bandVotes) {
            String[] parts = string.split("\t");
            bandVoteMap.put(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
        }

        ArrayList<Band> bands = (ArrayList<Band>) request.getAttribute("bands");
        for (Band band : bands) {
            if (bandVoteMap.containsKey(band.id))
                band.votes = bandVoteMap.get(band.id);
        }
        bands.sort(Comparator.comparing(Band::getVotes).reversed());
    }

    @Override
    public String toString() {
        return name;
    }
}
