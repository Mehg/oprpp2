package hr.fer.oprpp2.hw04.servlets.glasanje;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Servlet which handles adding a vote for a specific band
 * Receives a parameter id which represents id of band
 * For given band id adds vote to that band
 */
@WebServlet("/glasanje-glasaj")
public class GlasanjeGlasajServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        if (req.getParameter("id") == null)
            sendError("Didn't receive band id", req, resp);

        int id = Integer.parseInt(req.getParameter("id"));

        synchronized (Keys.lock) {
            if (req.getAttribute("bands") == null)
                Band.loadBands(req);
            Band.loadVotes(req);

            List<Band> bands = (List<Band>) req.getAttribute("bands");
            Optional<Band> bandVoted = bands.stream().filter(b -> b.id == id).findAny();

            if (bandVoted.isPresent()) {
                bandVoted.get().votes++;
            } else
                sendError("Not a valid band id", req, resp);

            List<String> votes = new ArrayList<>();
            for (Band band : bands) {
                votes.add(band.id + "\t" + band.votes);
            }
            String fileName = req.getServletContext().getRealPath(Keys.glasanjeRezultati);
            Files.write(Path.of(fileName), votes);
        }

        resp.sendRedirect(req.getContextPath() + "/glasanje-rezultati");
    }

    /**
     * Private method for sending error message
     *
     * @param message given message
     * @param req     request
     * @param res     response
     * @throws ServletException exception
     * @throws IOException      exception
     */
    private static void sendError(String message, HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.setAttribute("message", message);
        req.getRequestDispatcher("/WEB-INF/pages/error.jsp")
                .forward(req, res);
    }
}
