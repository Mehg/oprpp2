package hr.fer.oprpp2.hw04.servlets.glasanje;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

import javax.imageio.ImageIO;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Servlet which creates a PieChart from votes
 * If there are no bands or votes loads them
 */
@WebServlet("/glasanje-grafika")
public class GlasanjeGrafikaServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        ArrayList<Band> bands = (ArrayList<Band>) req.getAttribute("bands");

        synchronized (Keys.lock) {
            if (bands == null) {
                Band.loadBands(req);
                bands = (ArrayList<Band>) req.getAttribute("bands");
            }
            Band.loadVotes(req);
        }

        DefaultPieDataset dataset = new DefaultPieDataset();
        for (Band band : bands) {
            dataset.setValue(band.name, band.votes);
        }

        JFreeChart chart = ChartFactory.createPieChart(
                "Band popularity",
                dataset,
                true,
                true,
                false
        );

        BufferedImage img = chart.createBufferedImage(500, 270);

        resp.setContentType("image/png");
        ImageIO.write(img, "png", resp.getOutputStream());
    }
}
