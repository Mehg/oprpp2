package hr.fer.oprpp2.hw04.servlets;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet used for setting background color
 * Receives a parameter - pickedBgCol - and sets it as a session attribute which will be used as background color
 * If no parameter is given assumed white color
 */
@WebServlet("/setcolor")
public class SetColorServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String color = req.getParameter("pickedBgCol");

        if (color == null) {
            color = "FFFFFF";
        }

        color = "#" + color;

        req.getSession().setAttribute("pickedBgCol", color);
        resp.sendRedirect(req.getContextPath() + "/index.jsp");
    }
}
