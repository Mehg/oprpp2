package hr.fer.oprpp2.hw04.servlets.glasanje;

/**
 * Class which holds important information used across different Servlets
 */
public class Keys {
    /**
     * Band votes path
     */
    public static final String glasanjeRezultati = "/WEB-INF/glasanje-rezultati.txt";
    /**
     * Band definition path
     */
    public static final String glasanjeDefinicija = "/WEB-INF/glasanje-definicija.txt";
    /**
     * Lock used by threads
     */
    public static final Object lock = new Object();
}
