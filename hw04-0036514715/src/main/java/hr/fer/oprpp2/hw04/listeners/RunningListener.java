package hr.fer.oprpp2.hw04.listeners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.time.LocalDateTime;

/**
 * ServletContextListener which documents time at which the server started
 */
@WebListener
public class RunningListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        servletContextEvent.getServletContext().setAttribute("start", LocalDateTime.now());
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }
}
