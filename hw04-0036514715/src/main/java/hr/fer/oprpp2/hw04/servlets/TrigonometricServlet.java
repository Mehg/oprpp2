package hr.fer.oprpp2.hw04.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Servlet used for calculating trigonometric functions
 * Receives two parameters:
 * <pre>
 *     - a -> start angle - if not given assumed 0
 *     - b -> end angle - if not given assumed 360
 * </pre>
 * <p>
 * For each angle between a and b (included) calculates sin and cos
 */
@WebServlet("/trigonometric")
public class TrigonometricServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int a;
        int b;

        try {
            a = Integer.parseInt(req.getParameter("a"));
        } catch (NumberFormatException | NullPointerException e) {
            a = 0;
        }

        try {
            b = Integer.parseInt(req.getParameter("b"));
        } catch (NumberFormatException | NullPointerException e) {
            b = 360;
        }

        if (a > b) {
            int c = a;
            a = b;
            b = c;
        }

        if (b > a + 720) {
            b = a + 720;
        }

        ArrayList<SinAndCos> list = new ArrayList<>();
        for (int i = a; i <= b; i++) {
            list.add(new SinAndCos(i));
        }

        req.setAttribute("SinAndCos", list);
        req.getRequestDispatcher("/WEB-INF/pages/trigonometric.jsp").forward(req, resp);
    }

    /**
     * Class which keeps sin and cos values of degree
     */
    public static class SinAndCos {
        /**
         * degree
         */
        int degree;
        /**
         * sine
         */
        double sine;
        /**
         * cosine
         */
        double cosine;

        /**
         * Constructs a new SinAndCos from given degree - calculates sin and cos
         * @param degree given degree
         */
        public SinAndCos(int degree) {
            this.degree = degree;
            this.sine = round(Math.sin(Math.toRadians(degree)));
            this.cosine = round(Math.cos(Math.toRadians(degree)));
        }

        /**
         * Private helper method for rounding given value
         * @param value given value
         * @return rounded value
         */
        private double round(double value) {
            double scale = Math.pow(10, 5);
            return Math.round(value * scale) / scale;
        }

        /**
         * Returns degree
         * @return degree
         */
        public int getDegree() {
            return degree;
        }

        /**
         * Returns sin of degree
         * @return sin
         */
        public double getSine() {
            return sine;
        }

        /**
         * Returns cos of degree
         * @return cos
         */
        public double getCosine() {
            return cosine;
        }
    }
}


