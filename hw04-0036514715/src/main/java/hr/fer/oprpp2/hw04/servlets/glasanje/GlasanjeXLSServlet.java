package hr.fer.oprpp2.hw04.servlets.glasanje;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Servlet which generates an XLS file with all bands, their votes and links to their representative songs
 */
@WebServlet("/glasanje-xls")
public class GlasanjeXLSServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        ArrayList<Band> bands = (ArrayList<Band>) req.getAttribute("bands");

        synchronized (Keys.lock) {
            if (bands == null) {
                Band.loadBands(req);
                bands = (ArrayList<Band>) req.getAttribute("bands");
            }
            Band.loadVotes(req);
        }

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("band voting results");
        HSSFRow head = sheet.createRow((short) 0);

        head.createCell((short) 0).setCellValue("Band name");
        head.createCell((short) 1).setCellValue("Votes");
        head.createCell((short) 2).setCellValue("Popular song");

        for (int i = 0; i < bands.size(); i++) {
            Band band = bands.get(i);
            HSSFRow row = sheet.createRow((short) i + 1);
            row.createCell((short) 0).setCellValue(band.getName());
            row.createCell((short) 1).setCellValue(band.getVotes());
            row.createCell((short) 2).setCellValue(band.getLink());
        }

        resp.setContentType("application/vnd.ms-excel");
        resp.setHeader("Content-Disposition", "attachment; filename=\"table.xls\"");
        workbook.write(resp.getOutputStream());
    }
}
