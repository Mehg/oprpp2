package hr.fer.oprpp2.hw04.servlets;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet which generates an xls file which contains powers for given parameters
 * Receives three parameters
 * <pre>
 *     - a -> starting number (must be in range [-100, 100])
 *     - b -> ending number (must be in range [-100, 100])
 *     - n -> maximum power (must be in range [1, 5])
 * </pre>
 * If parameters are missing or wrong sends error message
 * Otherwise creates an XLS file with each sheet displaying po
 */
@WebServlet("/powers")
public class ExcelServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int a, b, n;

        try {
            a = Integer.parseInt(req.getParameter("a"));
            b = Integer.parseInt(req.getParameter("b"));
            n = Integer.parseInt(req.getParameter("n"));
        } catch (NumberFormatException | NullPointerException e) {
            sendError("Missing parameters or parameter format wrong", req, resp);
            return;
        }

        if (a < -100 || a > 100)
            sendError("Parameter a must be in range [-100, 100] but was " + a, req, resp);
        if (b < -100 || b > 100)
            sendError("Parameter b must be in range [-100, 100] but was " + b, req, resp);
        if (n < 1 || n > 5)
            sendError("Parameter n must be in range [1,5] but was " + n, req, resp);

        try {
            HSSFWorkbook workbook = new HSSFWorkbook();
            for (int i = 1; i <= n; i++) {
                HSSFSheet sheet = workbook.createSheet("sheet " + i);

                HSSFRow head = sheet.createRow((short) 0);
                head.createCell((short) 0).setCellValue("x");
                head.createCell((short) 1).setCellValue("pow(x," + i + ")");

                for (int j = a, rIndex = 1; j <= b; j++, rIndex++) {
                    HSSFRow row = sheet.createRow((short) rIndex);
                    row.createCell((short) 0).setCellValue(j);
                    row.createCell((short) 1).setCellValue(Math.pow(j, i));
                }
            }

            resp.setContentType("application/vnd.ms-excel");
            resp.setHeader("Content-Disposition", "attachment; filename=\"table.xls\"");
            workbook.write(resp.getOutputStream());
        } catch (Exception e) {
            sendError(e.getMessage(), req, resp);
        }

    }

    /**
     * Private method for sending error message
     *
     * @param message given message
     * @param req     request
     * @param res     response
     * @throws ServletException exception
     * @throws IOException      exception
     */
    private static void sendError(String message, HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.setAttribute("message", message);
        req.getRequestDispatcher("/WEB-INF/pages/error.jsp")
                .forward(req, res);
    }
}
