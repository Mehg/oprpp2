<html>
<head>
    <title>Error</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style.css">
</head>
<body>
<%@include file="header.jsp" %>
<h1 class="error">${error}</h1>
</body>
</html>
