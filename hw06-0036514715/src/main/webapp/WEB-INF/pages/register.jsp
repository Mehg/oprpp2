<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Register</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style.css">
</head>
<body>
<%@ include file="header.jsp" %>
<div>
    <div class="form-div">
        <h3>Register</h3>
        <c:if test="${form.hasErrors()}">
            <ul>
                <c:forEach var="error" items="${form.getErrors()}">
                    <li class="error">${error}</li>
                </c:forEach>
            </ul>
        </c:if>
        <form id="formID" action="${pageContext.request.contextPath}/servleti/register" method="post">
            <p>
                <label for="firstName">First name: </label>
                <input type="text" name="firstName" id="firstName" value="${form.getValue("firstName")}">
            </p>
            <p>
                <label for="lastName">Last name: </label>
                <input type="text" name="lastName" id="lastName" value="${form.getValue("lastName")}">
            </p>
            <p>
                <label for="email">E-mail: </label>
                <input type="text" name="email" id="email" value="${form.getValue("email")}">
            </p>
            <p>
                <label for="nick">Nickname: </label>
                <input type="text" name="nick" id="nick" value="${form.getValue("nick")}">
            </p>
            <p>
                <label for="password">Password: </label>
                <input type="password" name="password" id="password" value="${form.getValue("password")}">
            </p>
        </form>
        <input type="submit" form="formID">
    </div>
</div>

</body>
</html>
