<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="nav">
    <c:choose>
        <c:when test="${sessionScope['current.user.id'] == null}">
            You are not logged in.
            <div class="nav-right">
                <a href="<c:url value="/servleti/main"/>">Log in</a>
                or
                <a href="${pageContext.request.contextPath}/servleti/register">Register</a>
                |
                <a href="${pageContext.request.contextPath}/servleti/main">Home</a>
            </div>
        </c:when>
        <c:otherwise>
            ${sessionScope['current.user.fn']} ${sessionScope['current.user.ln']}
            <div class="nav-right">
                <a href="${pageContext.request.contextPath}/servleti/logout">Log out</a>
                |
                <a href="${pageContext.request.contextPath}/servleti/main">Home</a>
            </div>
        </c:otherwise>
    </c:choose>
</div>