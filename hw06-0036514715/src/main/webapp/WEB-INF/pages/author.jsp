<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${nick}</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style.css">
</head>
<body>
<%@ include file="header.jsp" %>
<div class="form-div">
    <c:forEach var="entry" items="${entries}">
        <a href="${pageContext.request.contextPath}/servleti/author/${nick}/${entry.id}">${entry.title}</a><br>
    </c:forEach>
</div>
<c:if test="${sessionScope['current.user.nick'].equals(nick)}">
    <div class="form-div">
        <hr>
        <a href="${pageContext.request.contextPath}/servleti/author/${nick}/new">New post</a>
    </div>
</c:if>
</body>
</html>
