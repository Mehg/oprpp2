<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Main page</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style.css">
</head>
<body>
<%@ include file="header.jsp" %>
<c:if test="${sessionScope['current.user.id']==null}">
    <div>
        <div class="form-div">
            <h3>Log in</h3>
            <c:if test="${form.hasErrors()}">
                <ul>
                    <c:forEach var="error" items="${form.getErrors()}">
                        <li class="error">${error}</li>
                    </c:forEach>
                </ul>
            </c:if>
            <form id="mainForm" action="${pageContext.request.contextPath}/servleti/main" method="post">
                <p>
                    <label for="nick">Nickname: </label>
                    <input type="text" name="nick" id="nick" value="${form.getValue("nick")}">
                </p>
                <p>
                    <label for="password">Password: </label>
                    <input type="password" name="password" id="password" value="${form.getValue("password")}">
                </p>
            </form>
            <input type="submit" form="mainForm">
        </div>
        or
        <h3><a href="${pageContext.request.contextPath}/servleti/register">Register</a></h3>
    </div>
</c:if>

<div class="form-div">
    <h3>Authors: </h3>
    <hr>
    <c:forEach var="user" items="${users}">
        <a href="${pageContext.request.contextPath}/servleti/author/${user.nick}">
                ${user.firstName} ${user.lastName}
        </a><br>
    </c:forEach>
</div>

</body>
</html>
