<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${entry.title}</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style.css">
</head>
<body>
<%@include file="header.jsp" %>
<div class="form-div">
    <h1>${entry.title}</h1>
    <p>${entry.text}</p>

    <c:if test="${sessionScope['current.user.nick'].equals(nick)}">
        <a href="${pageContext.request.contextPath}/servleti/author/${nick}/${entry.id}/edit">Edit this blog entry</a>
    </c:if>
    <hr>
</div>
<div style="background-color: white; margin: 10px auto; max-width:700px">
    <h2>Comments:</h2>
    <c:forEach var="comment" items="${entry.comments}">
    <pre>
        ${comment.usersEMail}: ${comment.message}
                                ${comment.postedOn}
    </pre>
        <hr>
    </c:forEach>
</div>

<div class="form-div">
    <h3>New comment:</h3>
    <c:if test="${form.hasErrors()}">
        <ul>
            <c:forEach var="error" items="${form.getErrors()}">
                <li class="error">${error}</li>
            </c:forEach>
        </ul>
    </c:if>

    <form id="commentID" method="post">
        <p>
            <label for="email">E-mail: </label>
            <input type="text" name="email" id="email" value="${form.getValue("email")}" style="width:500px">
        </p>
        <p>
            <label for="message"></label>
            <textarea name="message" id="message" form="commentID">
                ${form.getValue("message")}
            </textarea>
        </p>
    </form>
    <input type="submit" value="Post comment" form="commentID">
</div>
</body>
</html>
