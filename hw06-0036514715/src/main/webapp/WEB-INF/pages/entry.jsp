<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <c:choose>
        <c:when test="${form.getEntity()==null}">
            <title>New entry</title>
        </c:when>
        <c:otherwise>
            <title>Edit entry</title>
        </c:otherwise>
    </c:choose>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/style.css">
</head>
<body>
<%@ include file="header.jsp" %>
<div class="form-div">
    <c:choose>
        <c:when test="${form.getEntity()==null}">
            <h3>New entry</h3>
        </c:when>
        <c:otherwise>
            <h3>Edit entry</h3>
        </c:otherwise>
    </c:choose>

    <c:if test="${form.hasErrors()}">
        <ul>
            <c:forEach var="error" items="${form.getErrors()}">
                <li class="error">${error}</li>
            </c:forEach>
        </ul>
    </c:if>

    <form id="entryFormId" method="post">
        <p>
            <label for="title">Title: </label>
            <input type="text" name="title" id="title" value="${form.getValue("title")}" style="width:500px">
        </p>
        <p>
            <label for="text"> </label>
            <textarea name="text" id="text" form="entryFormId" style="width: 500px">
                ${form.getValue("text")}
            </textarea>
        </p>
    </form>
    <input form="entryFormId" type="submit">
</div>

</body>
</html>
