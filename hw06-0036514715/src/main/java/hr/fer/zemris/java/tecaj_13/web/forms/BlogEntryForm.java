package hr.fer.zemris.java.tecaj_13.web.forms;

import hr.fer.zemris.java.tecaj_13.model.BlogEntry;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Models a form which results in a new BlogEntry
 */
public class BlogEntryForm extends AbstractForm<BlogEntry> {
    private BlogEntry entity;

    /**
     * Creates a new BlogEntryForm with fields title and text
     */
    public BlogEntryForm() {
        super("title", "text");
    }

    /**
     * Creates a new BlogEntryForm with field values set to appropriate values from given entity
     *
     * @param entity given entity
     */
    public BlogEntryForm(BlogEntry entity) {
        this();
        readFieldsFromEntity(entity);
        this.entity = entity;
    }

    @Override
    public void readFieldsFromEntity(BlogEntry entity) {
        addValueForField("title", entity.getTitle());
        addValueForField("text", entity.getText());
    }

    @Override
    public void readFieldsFromRequest(HttpServletRequest request) {
        addValueForField("title", request.getParameter("title"));
        addValueForField("text", request.getParameter("text"));
    }

    @Override
    public void validate() {
        checkRequired("title", "Title");
        checkRequired("text", "Text");

        if (!hasErrors()) {
            Date current = new Date();
            if (entity == null) {
                entity = new BlogEntry();
                entity.setId(null);
                entity.setComments(null);
                entity.setCreatedAt(current);
            }
            entity.setLastModifiedAt(current);
            entity.setText(getValue("text"));
            entity.setTitle(getValue("title"));
        }
    }

    @Override
    public BlogEntry getEntity() {
        return entity;
    }
}
