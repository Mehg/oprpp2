package hr.fer.zemris.java.tecaj_13.web.forms;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Models a form with fields and values and all its errors
 *
 * @param <T> return type of form
 */
public abstract class AbstractForm<T> {
    /**
     * Map of value for field
     */
    private final HashMap<String, Value> fieldValueMap;

    /**
     * Creates a new form with given field names
     *
     * @param fieldNames given field names
     */
    public AbstractForm(String... fieldNames) {
        this.fieldValueMap = new HashMap<>();
        for (String fieldName : fieldNames)
            fieldValueMap.put(fieldName, new Value());
    }

    /**
     * Puts given value as value for given field
     *
     * @param fieldName given field name
     * @param value     given value
     */
    public void addValueForField(String fieldName, String value) {
        if (value == null)
            value = "";

        fieldValueMap.put(fieldName, new Value(value.trim()));
    }

    /**
     * Returns value for given field name
     *
     * @param fieldName given field name
     * @return vale of given field
     */
    public String getValue(String fieldName) {
        return fieldValueMap.get(fieldName).stringValue;
    }

    /**
     * Method which check if field with given field name has any values
     * Stores an error for that field if it's empty using the view name for that field
     *
     * @param fieldName given field name
     * @param viewName  given view name
     */
    public void checkRequired(String fieldName, String viewName) {
        Value value = fieldValueMap.get(fieldName);
        if (value.stringValue.isBlank()) {
            value.hasError = true;
            value.errorMessage = viewName + " cannot be empty";
        }
    }

    /**
     * Returns a list of all errors
     *
     * @return a list of all errors
     */
    public List<String> getErrors() {
        return fieldValueMap.values().stream().filter(v -> v.hasError).map(v -> v.errorMessage).collect(Collectors.toList());
    }

    /**
     * Checks if form has any errors
     *
     * @return true if it has any errors, otherwise false
     */
    public boolean hasErrors() {
        for (Value value : fieldValueMap.values()) {
            if (value.hasError)
                return true;
        }
        return false;
    }

    /**
     * Returns the error message for field with given field name
     * If there is no error returns an empty String
     *
     * @param fieldName given field name
     * @return error message
     */
    public String getErrorForField(String fieldName) {
        return fieldValueMap.get(fieldName).errorMessage;
    }

    /**
     * Checks if field with given field name has any errors
     *
     * @param fieldName given field name
     * @return true if field has errors, otherwise false
     */
    public boolean hasErrorForField(String fieldName) {
        return fieldValueMap.get(fieldName).hasError;
    }

    /**
     * Adds an error for the field with given field name
     *
     * @param fieldName given field name
     * @param error     given error
     */
    public void addError(String fieldName, String error) {
        Value value = fieldValueMap.get(fieldName);
        if (value == null)
            throw new IllegalArgumentException("Invalid fieldName");
        value.hasError = true;
        value.errorMessage = error;
    }

    /**
     * Method which reads values relative to the form from given entity
     *
     * @param entity given entity
     */
    public abstract void readFieldsFromEntity(T entity);

    /**
     * Method which reads values relative to the form from given HttpServletRequest
     *
     * @param request given request
     */
    public abstract void readFieldsFromRequest(HttpServletRequest request);

    /**
     * Method which validates the form, checking if all fields match constraints
     */
    public abstract void validate();

    /**
     * Returns result of form
     *
     * @return entity
     */
    public abstract T getEntity();

    /**
     * Class which models a form value
     */
    private static class Value {
        private final String stringValue;
        private boolean hasError;
        private String errorMessage;

        /**
         * Creates an empty value with no errors
         */
        public Value() {
            this("", false, "");
        }

        /**
         * Creates a value with string value set as given, and no errors
         *
         * @param stringValue given string value
         */
        public Value(String stringValue) {
            this(stringValue, false, "");
        }

        /**
         * Creates a value from given parameters
         *
         * @param stringValue  given string value
         * @param hasError     whether it has an error
         * @param errorMessage error message
         */
        public Value(String stringValue, boolean hasError, String errorMessage) {
            this.stringValue = stringValue;
            this.hasError = hasError;
            this.errorMessage = errorMessage;
        }
    }
}
