package hr.fer.zemris.java.tecaj_13.dao.jpa;

import hr.fer.zemris.java.tecaj_13.dao.DAO;
import hr.fer.zemris.java.tecaj_13.dao.DAOException;
import hr.fer.zemris.java.tecaj_13.model.BlogComment;
import hr.fer.zemris.java.tecaj_13.model.BlogEntry;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;

import java.util.List;

public class JPADAOImpl implements DAO {

    @Override
    public BlogEntry getBlogEntry(Long id) throws DAOException {
        return JPAEMProvider.getEntityManager().find(BlogEntry.class, id);
    }

    @Override
    public List<BlogUser> getAllBlogUsers() throws DAOException {
        return JPAEMProvider.getEntityManager()
                .createNamedQuery("BlogUser.allBlogUsers", BlogUser.class)
                .getResultList();
    }

    @Override
    public BlogUser getBlogUserByNickname(String nick) throws DAOException {
        List<BlogUser> users = JPAEMProvider.getEntityManager().createNamedQuery("BlogUser.byNick", BlogUser.class)
                .setParameter("nick", nick)
                .getResultList();
        if (users.size() == 0)
            return null;
        return users.get(0);
    }

    @Override
    public void addNewBlogUser(BlogUser user) throws DAOException {
        JPAEMProvider.getEntityManager().persist(user);
    }

    @Override
    public List<BlogEntry> getAllBlogEntriesForUser(String nick) throws DAOException {
        BlogUser user = getBlogUserByNickname(nick);
        return JPAEMProvider.getEntityManager().createNamedQuery("BlogEntry.forUser", BlogEntry.class)
                .setParameter("creator", user)
                .getResultList();
    }

    @Override
    public void addNewBlogEntry(BlogEntry entry) throws DAOException {
        JPAEMProvider.getEntityManager().persist(entry);
    }

    @Override
    public void addNewBlogComment(BlogComment comment) throws DAOException {
        JPAEMProvider.getEntityManager().persist(comment);
    }
}