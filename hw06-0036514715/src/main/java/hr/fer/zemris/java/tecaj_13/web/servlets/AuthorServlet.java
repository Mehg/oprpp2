package hr.fer.zemris.java.tecaj_13.web.servlets;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.BlogComment;
import hr.fer.zemris.java.tecaj_13.model.BlogEntry;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;
import hr.fer.zemris.java.tecaj_13.web.forms.BlogEntryForm;
import hr.fer.zemris.java.tecaj_13.web.forms.CommentForm;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/servleti/author/*")
public class AuthorServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pathInfo = req.getPathInfo();
        if (pathInfo == null) {
            sendError("Author nickname not provided", req, resp);
            return;
        }

        pathInfo = pathInfo.substring(1);
        String[] parts = pathInfo.split("/");

        if (parts.length == 1) {
            showAllTitles(parts[0], req, resp);
        } else if (parts.length == 2) {
            if (parts[1].equals("new")) {
                newBlogEntryGET(parts[0], req, resp);
            } else {
                showBlogEntryGET(parts[0], parts[1], req, resp);
            }

        } else if (parts.length == 3 && parts[2].equals("edit")) {
            editBlogEntryGET(parts[0], parts[1], req, resp);
        } else {
            sendError("Page not found", req, resp);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pathInfo = req.getPathInfo();
        if (pathInfo == null) {
            sendError("Author nickname not provided", req, resp);
            return;
        }
        pathInfo = pathInfo.substring(1);
        String[] parts = pathInfo.split("/");

        if (parts.length == 2) {
            if (parts[1].equals("new")) {
                newBlogEntryPOST(parts[0], req, resp);
            } else {
                showBlogEntryPOST(parts[0], parts[1], req, resp);
            }
        } else if (parts.length == 3 && parts[2].equals("edit")) {
            editBlogEntryPOST(parts[0], parts[1], req, resp);
        } else {
            sendError("Page not found", req, resp);
        }
    }

    /**
     * Method which handles GET request on /servleti/author/nick
     * Gets all blog entries whose creator has the given nick
     *
     * @param nick given nick from URL
     * @param req  HttpServletRequest
     * @param resp HttpServletResponse
     * @throws ServletException if something went wrong
     * @throws IOException      if something went wrong
     */
    private void showAllTitles(String nick, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<BlogEntry> entries = DAOProvider.getDAO().getAllBlogEntriesForUser(nick);

        req.setAttribute("nick", nick);
        req.setAttribute("entries", entries);

        req.getRequestDispatcher("/WEB-INF/pages/author.jsp").forward(req, resp);
    }

    /**
     * Method which handles GET request on /servleti/author/nick/new
     * Shows a new blog entry form to the user
     *
     * @param nick given nick
     * @param req  HttpServletRequest
     * @param resp HttpServletResponse
     * @throws ServletException if something went wrong
     * @throws IOException      if something went wrong
     */
    private void newBlogEntryGET(String nick, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("current.user.id") == null || !nick.equals(req.getSession().getAttribute("current.user.nick"))) {
            sendError("Permission denied", req, resp);
            return;
        }

        BlogEntryForm form = new BlogEntryForm();

        req.setAttribute("nick", nick);
        req.setAttribute("form", form);

        req.getRequestDispatcher("/WEB-INF/pages/entry.jsp").forward(req, resp);
    }

    /**
     * Method which handles POST request on /servleti/author/nick/new
     * Validates the form and saves new blog entry if everything's okay
     *
     * @param nick given nick
     * @param req  HttpServletRequest
     * @param resp HttpServletResponse
     * @throws ServletException if something went wrong
     * @throws IOException      if something went wrong
     */
    private void newBlogEntryPOST(String nick, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BlogEntryForm form = new BlogEntryForm();
        form.readFieldsFromRequest(req);
        form.validate();

        if (form.hasErrors()) {
            req.setAttribute("nick", nick);
            req.setAttribute("form", form);
            req.getRequestDispatcher("/WEB-INF/pages/entry.jsp").forward(req, resp);
            return;
        }

        BlogEntry entry = form.getEntity();
        BlogUser user = DAOProvider.getDAO().getBlogUserByNickname((String) req.getSession().getAttribute("current.user.nick"));
        entry.setCreator(user);
        DAOProvider.getDAO().addNewBlogEntry(entry);
        resp.sendRedirect(req.getContextPath() + "/servleti/author/" + nick + "/" + entry.getId());
    }

    /**
     * Method which handles GET request on /servleti/author/nick/eid
     * Shows requested blog entry
     * Shows new comment form
     *
     * @param nick given nick
     * @param eid  given blog entry id
     * @param req  HttpServletRequest
     * @param resp HttpServletResponse
     * @throws ServletException if something went wrong
     * @throws IOException      if something went wrong
     */
    private void showBlogEntryGET(String nick, String eid, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BlogEntry entry = DAOProvider.getDAO().getBlogEntry(Long.parseLong(eid));
        if (entry == null || !entry.getCreator().getNick().equals(nick)) {
            sendError("Page doesn't exist", req, resp);
            return;
        }
        CommentForm form = new CommentForm();

        req.setAttribute("entry", entry);
        req.setAttribute("nick", nick);
        req.setAttribute("form", form);

        req.getRequestDispatcher("/WEB-INF/pages/blogEntryPage.jsp").forward(req, resp);
    }

    /**
     * Method which handles POST request on /servleti/author/nick/eid
     * Validates form and if everything's okay saves the newly created comment
     *
     * @param nick given nick
     * @param eid  given blog entry id
     * @param req  HttpServletRequest
     * @param resp HttpServletResponse
     * @throws ServletException if something went wrong
     * @throws IOException      if something went wrong
     */
    private void showBlogEntryPOST(String nick, String eid, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BlogEntry entry = DAOProvider.getDAO().getBlogEntry(Long.parseLong(eid));
        CommentForm form = new CommentForm();
        form.readFieldsFromRequest(req);
        form.validate();

        if (form.hasErrors()) {
            req.setAttribute("entry", entry);
            req.setAttribute("nick", nick);
            req.setAttribute("form", form);

            req.getRequestDispatcher("/WEB-INF/pages/blogEntryPage.jsp").forward(req, resp);
            return;
        }

        BlogComment comment = form.getEntity();
        comment.setBlogEntry(entry);
        DAOProvider.getDAO().addNewBlogComment(comment);
        resp.sendRedirect(req.getContextPath() + "/servleti/author/" + nick + "/" + eid);
    }

    /**
     * Method which handles GET request on /servleti/author/nick/eid/edit
     * Shows a blog entry form to the user for editing requested blog entry
     *
     * @param nick given nick
     * @param eid  blog entry id
     * @param req  HttpServletRequest
     * @param resp HttpServletResponse
     * @throws ServletException if something went wrong
     * @throws IOException      if something went wrong
     */
    private void editBlogEntryGET(String nick, String eid, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("current.user.id") == null || !nick.equals(req.getSession().getAttribute("current.user.nick"))) {
            sendError("Permission denied", req, resp);
            return;
        }

        BlogEntry entry = DAOProvider.getDAO().getBlogEntry(Long.parseLong(eid));
        if (entry == null || !entry.getCreator().getNick().equals(nick)) {
            sendError("Page doesn't exist", req, resp);
            return;
        }
        BlogEntryForm form = new BlogEntryForm(entry);

        req.setAttribute("nick", nick);
        req.setAttribute("form", form);

        req.getRequestDispatcher("/WEB-INF/pages/entry.jsp").forward(req, resp);
    }

    /**
     * Method which handles POST request on /servleti/author/nick/eid/edit
     * Validates form and saves changes
     *
     * @param nick given nick
     * @param eid  given blog entry id
     * @param req  HttpServletRequest
     * @param resp HttpServletResponse
     * @throws ServletException if something went wrong
     * @throws IOException      if something went wrong
     */
    private void editBlogEntryPOST(String nick, String eid, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BlogEntryForm form = new BlogEntryForm();
        form.readFieldsFromRequest(req);
        form.validate();

        if (form.hasErrors()) {
            req.setAttribute("nick", nick);
            req.setAttribute("form", form);
            req.getRequestDispatcher("/WEB-INF/pages/entry.jsp").forward(req, resp);
            return;
        }

        BlogEntry entry = DAOProvider.getDAO().getBlogEntry(Long.parseLong(eid));
        entry.setText(form.getValue("text"));
        entry.setTitle(form.getValue("title"));

        resp.sendRedirect(req.getContextPath() + "/servleti/author/" + nick + "/" + eid);
    }

    /**
     * Method used for sending errors
     *
     * @param req  HttpServletRequest
     * @param resp HttpServletResponse
     * @throws ServletException if something went wrong
     * @throws IOException      if something went wrong
     */
    private void sendError(String message, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("error", message);
        req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
    }
}
