package hr.fer.zemris.java.tecaj_13.web.forms;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;

import javax.servlet.http.HttpServletRequest;

/**
 * Models a form which results in a new BlogUser
 */
public class RegisterForm extends AbstractForm<BlogUser> {
    private BlogUser entity;

    /**
     * Creates a new RegisterForm with fields: firstName, lastName, email, nick and password
     */
    public RegisterForm() {
        super("firstName", "lastName", "email", "nick", "password");
    }

    @Override
    public void readFieldsFromEntity(BlogUser entity) {
        addValueForField("firstName", entity.getFirstName());
        addValueForField("lastName", entity.getLastName());
        addValueForField("email", entity.getEmail());
        addValueForField("nick", entity.getNick());
        addValueForField("password", entity.getPasswordHash());
    }

    @Override
    public void readFieldsFromRequest(HttpServletRequest request) {
        addValueForField("firstName", request.getParameter("firstName"));
        addValueForField("lastName", request.getParameter("lastName"));
        addValueForField("email", request.getParameter("email"));
        addValueForField("nick", request.getParameter("nick"));
        addValueForField("password", request.getParameter("password"));
    }

    @Override
    public void validate() {
        checkRequired("firstName", "First name");
        checkRequired("lastName", "Last name");
        checkRequired("email", "E-mail");
        checkRequired("nick", "Nickname");
        checkRequired("password", "Password");

        if (!hasErrorForField("nick"))
            checkUniqueNickname(getValue("nick"));

        if (!hasErrors()) {
            entity = new BlogUser();
            entity.setId(null);
            entity.setEmail(getValue("email"));
            entity.setFirstName(getValue("firstName"));
            entity.setNick(getValue("nick"));
            entity.setLastName(getValue("lastName"));
            entity.setPasswordHash(entity.hashPassword(getValue("password")));
        }
    }

    /**
     * Method which checks whether the given nick is unique
     *
     * @param value given nick
     */
    private void checkUniqueNickname(String value) {
        boolean exists = DAOProvider.getDAO().getBlogUserByNickname(value) != null;
        if (exists) {
            addError("nick", "Nickname must be unique");
        }
    }

    @Override
    public BlogUser getEntity() {
        return entity;
    }
}
