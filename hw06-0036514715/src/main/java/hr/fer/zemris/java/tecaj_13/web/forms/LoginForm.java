package hr.fer.zemris.java.tecaj_13.web.forms;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;

import javax.servlet.http.HttpServletRequest;

/**
 * Models a form which logs in a BlogUser
 */
public class LoginForm extends AbstractForm<BlogUser> {
    private BlogUser entity;

    /**
     * Creates a new LoginForm with fields nick and password
     */
    public LoginForm() {
        super("nick", "password");
    }

    @Override
    public void readFieldsFromEntity(BlogUser entity) {
        addValueForField("nick", entity.getNick());
        addValueForField("password", entity.getPasswordHash());
    }

    @Override
    public void readFieldsFromRequest(HttpServletRequest request) {
        addValueForField("nick", request.getParameter("nick"));
        addValueForField("password", request.getParameter("password"));
    }

    @Override
    public void validate() {
        checkRequired("nick", "Nickname");
        checkRequired("password", "Password");
        if (!hasErrors()) {
            entity = DAOProvider.getDAO().getBlogUserByNickname(getValue("nick"));
            if (entity == null || !entity.getPasswordHash().equals(entity.hashPassword(getValue("password")))) {
                addError("nick", "Invalid username or password");
            }
        }
    }

    @Override
    public BlogUser getEntity() {
        return entity;
    }
}

