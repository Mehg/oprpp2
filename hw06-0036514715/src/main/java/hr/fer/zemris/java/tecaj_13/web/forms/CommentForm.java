package hr.fer.zemris.java.tecaj_13.web.forms;

import hr.fer.zemris.java.tecaj_13.model.BlogComment;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Models a form which results in a new Comment
 */
public class CommentForm extends AbstractForm<BlogComment> {
    private BlogComment entity;

    /**
     * Creates a new CommentForm with fields email and message
     */
    public CommentForm() {
        super("email", "message");
    }

    @Override
    public void readFieldsFromEntity(BlogComment entity) {
        addValueForField("email", entity.getUsersEMail());
        addValueForField("message", entity.getMessage());
    }

    @Override
    public void readFieldsFromRequest(HttpServletRequest request) {
        addValueForField("email", request.getParameter("email"));
        addValueForField("message", request.getParameter("message"));
    }

    @Override
    public void validate() {
        checkRequired("email", "E-mail");
        checkRequired("message", "Message");

        if (!hasErrors()) {
            entity = new BlogComment();
            entity.setId(null);
            entity.setMessage(getValue("message"));
            entity.setPostedOn(new Date());
            entity.setUsersEMail(getValue("email"));
        }
    }

    @Override
    public BlogComment getEntity() {
        return entity;
    }
}
