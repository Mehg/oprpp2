package hr.fer.zemris.java.tecaj_13.dao;

import hr.fer.zemris.java.tecaj_13.model.BlogComment;
import hr.fer.zemris.java.tecaj_13.model.BlogEntry;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;

import java.util.List;

public interface DAO {

    /**
     * Dohvaća entry sa zadanim <code>id</code>-em. Ako takav entry ne postoji,
     * vraća <code>null</code>.
     *
     * @param id ključ zapisa
     * @return entry ili <code>null</code> ako entry ne postoji
     * @throws DAOException ako dođe do pogreške pri dohvatu podataka
     */
    BlogEntry getBlogEntry(Long id) throws DAOException;

    /**
     * Returns a list of all BlogUsers
     *
     * @return list of all BlogUsers
     * @throws DAOException if an exception occurs during reading
     */
    List<BlogUser> getAllBlogUsers() throws DAOException;

    /**
     * Returns the blog user with given nickname
     *
     * @param nick given nickname
     * @return blog user with given nickname
     * @throws DAOException if an exception occurs during reading
     */
    BlogUser getBlogUserByNickname(String nick) throws DAOException;

    /**
     * Saves given BlogUser
     *
     * @param user given BlogUser
     * @throws DAOException if something happens during saving
     */
    void addNewBlogUser(BlogUser user) throws DAOException;

    /**
     * Returns a list of blog entries from given user
     *
     * @param nick nickname of user
     * @return list of blog entries whose creator has the given nickname
     * @throws DAOException if something happens during reading
     */
    List<BlogEntry> getAllBlogEntriesForUser(String nick) throws DAOException;

    /**
     * Saves given blog entry
     *
     * @param entry given blog entry
     * @throws DAOException if something happens during saving
     */
    void addNewBlogEntry(BlogEntry entry) throws DAOException;

    /**
     * Saves given blog comment
     *
     * @param comment given blog comment
     * @throws DAOException if something happens during saving
     */
    void addNewBlogComment(BlogComment comment) throws DAOException;

}