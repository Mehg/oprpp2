package oprpp2.hw02.messages;

import java.io.*;
import java.util.Objects;

/**
 * Abstract class which models a simple chat message
 */
public abstract class Message {
    /**
     * Message code
     * 1 - HELLO
     * 2 - ACK
     * 3 - BYE
     * 4 - OUTMSG
     * 5 - INMSG
     */
    private int code;
    /**
     * Number of message
     */
    private long number;

    /**
     * Constructs a new message from given parameters
     *
     * @param code   given code
     * @param number given number
     */
    public Message(int code, long number) {
        this.code = code;
        this.number = number;
    }

    /**
     * Constructs a message with code and number set to zero
     */
    public Message() {
        this.code = 0;
        this.number = 0;
    }

    /**
     * Returns the code of the message
     *
     * @return code
     */
    public int getCode() {
        return code;
    }

    /**
     * Sets the code of the message to the given code
     *
     * @param code given code
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * Returns the number of the message
     *
     * @return number
     */
    public long getNumber() {
        return number;
    }

    /**
     * Sets the number of the message to the given number
     *
     * @param number given number
     */
    public void setNumber(long number) {
        this.number = number;
    }

    /**
     * Translates the current message to a byte array and returns it
     *
     * @return byte array representation of this message
     */
    public byte[] messageToByte() {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(bos);

        byte[] buf = null;
        try {
            dos.writeByte(getCode());
            dos.writeLong(getNumber());
            additionToByte(dos);
            dos.close();
            buf = bos.toByteArray();
            dos.close();
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return buf;
    }

    /**
     * Helper method which adds byte representation of additional parameters
     * Used in messageToByte
     *
     * @param dos given DataOutputStream
     * @throws IOException if something went wrong with the DataOutputStream
     */
    abstract void additionToByte(DataOutputStream dos) throws IOException;

    /**
     * Changes this messages parameters to one's given by the byte array
     *
     * @param bytes given byte array representation of a message
     */
    public void byteToMessage(byte[] bytes) {
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        DataInputStream dis = new DataInputStream(bis);
        try {
            setCode(dis.readByte());
            setNumber(dis.readLong());
            additionToMessage(dis);
            dis.close();
            bis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Helper method which reads additional parameters from the DataInputStream
     * Sets parameters to given parameters
     *
     * @param dis given DataInputStream
     * @throws IOException if something went wrong with the DataInputStream
     */
    abstract void additionToMessage(DataInputStream dis) throws IOException;

    @Override
    public String toString() {
        return "Message(" +
                "code=" + code +
                ", number=" + number +
                childToString() +
                ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return code == message.code && number == message.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, number);
    }

    /**
     * Package private helper method for neat printing
     * Used in toString() method
     *
     * @return String representation of child parameters
     */
    abstract String childToString();
}
