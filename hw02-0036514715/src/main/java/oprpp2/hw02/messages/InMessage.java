package oprpp2.hw02.messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Objects;

/**
 * Message class which models a simple InMessage
 * Each InMessage has a number, name an text
 */
public class InMessage extends Message {
    /**
     * Name of sender of this InMessage
     */
    private String name;
    /**
     * Text of this InMessage
     */
    private String text;

    /**
     * Constructs a new InMessage from given parameters
     *
     * @param number given number
     * @param name   given name
     * @param text   given text
     * @throws NullPointerException if name or text are null
     */
    public InMessage(long number, String name, String text) {
        super(5, number);
        if (name == null || text == null) throw new NullPointerException("Given name and text cannot be null");
        this.name = name;
        this.text = text;
    }

    /**
     * Constructs a new InMessage with empty name and text
     */
    public InMessage() {
        super();
        this.name = "";
        this.text = "";
    }

    /**
     * Returns the name of this InMessage
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name to given name
     *
     * @param name given name
     * @throws NullPointerException if given name is null
     */
    public void setName(String name) {
        if (name == null) throw new NullPointerException("Given name cannot be null");
        this.name = name;
    }

    /**
     * Returns the text of this InMessage
     *
     * @return text
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the text of this InMessage to given text
     *
     * @param text given text
     * @throws NullPointerException if given text is null
     */
    public void setText(String text) {
        if (text == null) throw new NullPointerException("Given text cannot be null");
        this.text = text;
    }

    @Override
    void additionToByte(DataOutputStream dos) throws IOException {
        dos.writeUTF(getName());
        dos.writeUTF(getText());
    }

    @Override
    void additionToMessage(DataInputStream dis) throws IOException {
        setName(dis.readUTF());
        setText(dis.readUTF());
    }

    @Override
    public String childToString() {
        return ", name=" + name + ", text=" + text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        InMessage inMessage = (InMessage) o;
        return Objects.equals(name, inMessage.name) && Objects.equals(text, inMessage.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, text);
    }
}
