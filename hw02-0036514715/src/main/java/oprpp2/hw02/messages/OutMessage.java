package oprpp2.hw02.messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Objects;


/**
 * Message class which models a simple OutMessage
 * Each OutMessage has an additional UID and text
 */
public class OutMessage extends Message {
    /**
     * User ID
     */
    private long UID;
    /**
     * Content of the message
     */
    private String text;

    /**
     * Creates a new OutMessage with given parameters
     *
     * @param number given number
     * @param UID    given UID
     * @param text   given text
     * @throws NullPointerException if given text is null
     */
    public OutMessage(long number, long UID, String text) {
        super(4, number);
        if (text == null) throw new NullPointerException("Given text cannot be null");
        this.UID = UID;
        this.text = text;
    }

    /**
     * Creates a new OutMessage with UID set to zero and empty text
     */
    public OutMessage() {
        super();
        this.UID = 0;
        this.text = "";
    }

    /**
     * Returns UID of this OutMessage
     *
     * @return UID
     */
    public long getUID() {
        return UID;
    }

    /**
     * Sets UID to given UID
     *
     * @param UID given UID
     */
    public void setUID(long UID) {
        this.UID = UID;
    }

    /**
     * Returns text of this OutMessage
     *
     * @return text
     */
    public String getText() {
        return text;
    }

    /**
     * Sets text to given text
     *
     * @param text given text
     * @throws NullPointerException if given text is null
     */
    public void setText(String text) {
        if (text == null) throw new NullPointerException("Given text cannot be null");
        this.text = text;
    }

    @Override
    void additionToByte(DataOutputStream dos) throws IOException {
        dos.writeLong(getUID());
        dos.writeUTF(getText());
    }

    @Override
    void additionToMessage(DataInputStream dis) throws IOException {
        setUID(dis.readLong());
        setText(dis.readUTF());
    }

    @Override
    public String childToString() {
        return ", UID=" + UID + ", text=" + text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        OutMessage that = (OutMessage) o;
        return UID == that.UID && Objects.equals(text, that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), UID, text);
    }
}
