package oprpp2.hw02.messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Objects;


/**
 * Message class which models a simple HelloMessage
 * Each HelloMessage has a number, name and randkey
 */
public class HelloMessage extends Message {
    /**
     * User name of this HelloMessage
     */
    private String name;
    /**
     * Randkey of this HelloMessage
     */
    private long randkey;

    /**
     * Constructs a new HelloMessage with given parameters
     *
     * @param number  given number of message
     * @param name    given name
     * @param randkey given randkey
     * @throws NullPointerException if given name is null
     */
    public HelloMessage(long number, String name, long randkey) {
        super(1, number);
        if (name == null) throw new NullPointerException("Given name cannot be null");
        this.name = name;
        this.randkey = randkey;
    }

    /**
     * Constructs an empty HelloMessage with empty name and randkey set to 0
     */
    public HelloMessage() {
        super();
        this.name = "";
        this.randkey = 0;
    }

    /**
     * Returns the name of this HelloMessage
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name to given name
     *
     * @param name given name
     * @throws NullPointerException if given name is null
     */
    public void setName(String name) {
        if (name == null) throw new NullPointerException("Given name cannot be null");
        this.name = name;
    }

    /**
     * Returns the randkey of this HelloMessage
     *
     * @return randkey
     */
    public long getRandkey() {
        return randkey;
    }

    /**
     * Sets randkey to given randkey
     *
     * @param randkey given randkey
     */
    public void setRandkey(long randkey) {
        this.randkey = randkey;
    }


    @Override
    void additionToByte(DataOutputStream dos) throws IOException {
        dos.writeUTF(name);
        dos.writeLong(randkey);
    }

    @Override
    void additionToMessage(DataInputStream dis) throws IOException {
        setName(dis.readUTF());
        setRandkey(dis.readLong());
    }

    @Override
    public String childToString() {
        return ", name=" + name + ", randkey=" + randkey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        HelloMessage message = (HelloMessage) o;
        return randkey == message.randkey && Objects.equals(name, message.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, randkey);
    }
}
