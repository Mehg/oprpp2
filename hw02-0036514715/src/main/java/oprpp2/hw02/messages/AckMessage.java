package oprpp2.hw02.messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Objects;

/**
 * Message class which models a simple AckMessage
 * Each AckMessage has an additional UID (user ID)
 */
public class AckMessage extends Message {
    /**
     * User ID of this AckMessage
     */
    private long UID;

    /**
     * Constructs a new AckMessage with given parameters
     *
     * @param number given number of message
     * @param UID    given User ID
     */
    public AckMessage(long number, long UID) {
        super(2, number);
        this.UID = UID;
    }

    /**
     * Constructs an empty AckMessage with UID set to zero
     */
    public AckMessage() {
        super();
        this.UID = 0;
    }

    /**
     * Returns the User ID
     *
     * @return UID
     */
    public long getUID() {
        return UID;
    }

    /**
     * Sets User ID to given UID
     *
     * @param UID given UID
     */
    public void setUID(long UID) {
        this.UID = UID;
    }

    @Override
    void additionToByte(DataOutputStream dos) throws IOException {
        dos.writeLong(getUID());
    }

    @Override
    void additionToMessage(DataInputStream dis) throws IOException {
        setUID(dis.readLong());
    }

    @Override
    String childToString() {
        return ", UID=" + UID;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        AckMessage that = (AckMessage) o;
        return UID == that.UID;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), UID);
    }
}
