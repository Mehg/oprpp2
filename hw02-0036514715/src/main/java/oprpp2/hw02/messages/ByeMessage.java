package oprpp2.hw02.messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Objects;

/**
 * Message class which models a simple ByeMessage
 * Each ByeMessage has an additional UID (user ID)
 */
public class ByeMessage extends Message {
    /**
     * User ID of this ByeMessage
     */
    private long UID;

    /**
     * Constructs a new ByeMessage with given parameters
     *
     * @param number given number of message
     * @param UID    given User ID
     */
    public ByeMessage(long number, long UID) {
        super(3, number);
        this.UID = UID;
    }

    /**
     * Constructs an empty ByeMessage with UID set to zero
     */
    public ByeMessage() {
        super();
        this.UID = 0;
    }

    /**
     * Returns the User ID
     *
     * @return UID
     */
    public long getUID() {
        return UID;
    }

    /**
     * Sets User ID to given UID
     *
     * @param UID given UID
     */
    public void setUID(long UID) {
        this.UID = UID;
    }

    @Override
    void additionToByte(DataOutputStream dos) throws IOException {
        dos.writeLong(getUID());
    }

    @Override
    void additionToMessage(DataInputStream dis) throws IOException {
        setUID(dis.readLong());
    }

    @Override
    public String childToString() {
        return ", UID=" + UID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ByeMessage message = (ByeMessage) o;
        return UID == message.UID;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), UID);
    }
}
