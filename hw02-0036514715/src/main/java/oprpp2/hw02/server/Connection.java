package oprpp2.hw02.server;

import oprpp2.hw02.messages.Message;

import java.net.InetAddress;
import java.util.LinkedList;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Class which models a connection form Server to Client
 * It keeps track of information needed for communication between them
 */
public class Connection {
    /**
     * Users InetAddress
     */
    private final InetAddress address;
    /**
     * Users port
     */
    private final int port;
    /**
     * Users name
     */
    private final String name;
    /**
     * User's randkey
     */
    private final long randkey;
    /**
     * User ID
     */
    private long UID;
    /**
     * Next message number
     */
    private long numberOfNextMessage;

    /**
     * Queue of outgoing messages
     */
    private final BlockingQueue<Message> outgoing;
    /**
     * Queue of incoming messages
     */
    private final BlockingQueue<Message> incoming;

    /**
     * Creates a new Connection with given parameters
     * Sets the numberOfNextMessage to 1
     *
     * @param address given address
     * @param port    given port
     * @param name    given name
     * @param randkey given randkey
     * @throws NullPointerException if given address or name are null
     */
    public Connection(InetAddress address, int port, String name, long randkey) {
        if (address == null || name == null) throw new NullPointerException("Given address and name cannot be null");
        this.address = address;
        this.port = port;
        this.name = name;
        this.randkey = randkey;
        outgoing = new LinkedBlockingQueue<>();
        incoming = new LinkedBlockingQueue<>();
        this.numberOfNextMessage = 1;
    }

    /**
     * Returns UID
     *
     * @return UID
     */
    public long getUID() {
        return UID;
    }

    /**
     * Sets UID to given UID
     *
     * @param UID given UID
     */
    public void setUID(long UID) {
        this.UID = UID;
    }

    /**
     * Returns user address
     *
     * @return address
     */
    public InetAddress getAddress() {
        return address;
    }

    /**
     * Returns user port
     *
     * @return port
     */
    public int getPort() {
        return port;
    }

    /**
     * Returns user name
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Retrieves the list of outgoing messages
     *
     * @return outgoing messages
     */
    public BlockingQueue<Message> getOutgoing() {
        return outgoing;
    }

    /**
     * Returns the list of incoming messages
     *
     * @return incoming messages
     */
    public BlockingQueue<Message> getIncoming() {
        return incoming;
    }

    /**
     * Returns the number of the next message
     *
     * @return number of next message
     */
    public long getNumberOfNextMessage() {
        return numberOfNextMessage;
    }

    /**
     * Puts given message into outgoing list
     * Increments the number of the next message
     *
     * @param message given message
     * @throws NullPointerException if given message is null
     */
    public void putIntoOutgoing(Message message) {
        if (message == null) throw new NullPointerException("Given message cannot be null");
        outgoing.add(message);
        numberOfNextMessage++;
    }

    /**
     * Returns the next message from the outgoing queue
     *
     * @return next message
     */
    public Message getNextFromOutgoing() {
        return outgoing.poll();
    }

    /**
     * Puts the given message at the end of the incoming queue
     *
     * @param message given message
     * @throws NullPointerException if given message is null
     */
    public void putIntoIncoming(Message message) {
        if (message == null) throw new NullPointerException("Given message cannot be null");
        incoming.add(message);
    }

    /**
     * Checks if the incoming queue contains a message with given number
     *
     * @param number given number
     * @return true if incoming queue contains message with given number - otherwise false
     */
    public boolean hasInIncoming(long number) {
        for (Message message : incoming) {
            if (message.getNumber() == number)
                return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Connection that = (Connection) o;
        return port == that.port && randkey == that.randkey && Objects.equals(address, that.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address, port, randkey);
    }
}
