package oprpp2.hw02.server;

import oprpp2.hw02.messages.*;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class which models a Server
 * Clients connected to the server can send messages to all other clients
 */
public class Main {
    /**
     * Map of ClientThreads which are responsible for a client Connection
     */
    private static final Map<ClientThread, Connection> connections = new HashMap<>();
    /**
     * Socket used for sending and receiving Datagrams
     */
    private static DatagramSocket socket;
    /**
     * UID of next user
     */
    private static long nextUID;

    /**
     * Handles incoming messages. For each type of message invokes the right method
     *
     * @param args list of arguments - has to only have one - the port
     */
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Expected port");
            return;
        }

        int port = handlePort(args[0]);
        if (port == 0)
            return;

        nextUID = new Random().nextLong();

        try (DatagramSocket socket1 = new DatagramSocket(port)) {
            socket = socket1;
            while (true) {
                byte[] incoming = new byte[500];
                DatagramPacket incomingPacket = new DatagramPacket(incoming, incoming.length);

                try {
                    socket.receive(incomingPacket);
                } catch (IOException e) {
                    continue;
                }

                System.out.println("Received a packet from: " + incomingPacket.getSocketAddress());
                Message message = decodeMessage(incoming);
                System.out.println(message);
                System.out.println();
                switch (message.getCode()) {
                    case 1 -> handleHelloMessage((HelloMessage) message, incomingPacket.getAddress(), incomingPacket.getPort());
                    case 2 -> handleAckMessage((AckMessage) message);
                    case 3 -> handleByeMessage((ByeMessage) message);
                    case 4 -> handleOutMessage((OutMessage) message);
                }
            }
        } catch (SocketException e) {
            System.out.println("Something went wrong with creating a DatagramSocket");
            System.out.println(e.getMessage());
        }

    }

    /**
     * Private helper method for handling port
     *
     * @param arg given String representation of port
     * @return int representation of port
     */
    private static int handlePort(String arg) {
        int port = 0;
        try {
            port = Integer.parseInt(arg);
            if (port < 1 || port > 65535) {
                System.out.println("Invalid port - has to be between 1 and 65535");
                port = 0;
            }
        } catch (NumberFormatException e) {
            System.out.println(arg + " is not a valid port");
        }
        return port;
    }

    /**
     * Private helper method used for decoding the type of message int the byte array
     *
     * @param array given byte array
     * @return Message representation of the given byte array
     * @throws IllegalArgumentException if invalid message type
     */
    private static Message decodeMessage(byte[] array) {
        Message message;
        switch (array[0]) {
            case 1 -> message = new HelloMessage();
            case 2 -> message = new AckMessage();
            case 3 -> message = new ByeMessage();
            case 4 -> message = new OutMessage();
            default -> throw new IllegalArgumentException("Unexpected message type");
        }
        message.byteToMessage(array);
        return message;
    }

    /**
     * Method used for handling received HelloMessages.
     * Creates a new connection and thread responsible for user if a new user connects to the server
     * Sends AckMessage as response
     *
     * @param message given HelloMessage
     * @param address given InetAddress of user
     * @param port    given port of user
     */
    private static void handleHelloMessage(HelloMessage message, InetAddress address, int port) {
        Connection connection = new Connection(address, port, message.getName(), message.getRandkey());
        AckMessage ackMessage;

        if (connections.containsValue(connection)) {
            Connection finalConnection = connection;
            connection = connections.values().stream().filter(c -> c.equals(finalConnection)).findFirst().orElseThrow();
            ackMessage = new AckMessage(message.getNumber(), connection.getUID());
        } else {
            connection.setUID(nextUID);
            ackMessage = new AckMessage(message.getNumber(), nextUID++);

            ClientThread thread = new ClientThread(connection);
            thread.setDaemon(true);
            thread.start();
            connections.put(thread, connection);
        }
        sendAckMessage(ackMessage, connection);
    }

    /**
     * Method used for handling received AckMessages
     * Puts given AckMessage into the incoming queue of the right user and notifies their thread
     *
     * @param message given message
     */
    private static void handleAckMessage(AckMessage message) {
        for (var entry : connections.entrySet()) {
            if (entry.getValue().getUID() == message.getUID()) {
                entry.getValue().putIntoIncoming(message);
                synchronized (entry.getValue()) {
                    entry.getValue().notify();
                }
            }
        }

    }

    /**
     * Method used for handling received ByeMessages
     * Sends AckMessage to right user and disconnects them
     * Stops users thread
     *
     * @param message given message
     */
    private static void handleByeMessage(ByeMessage message) {
        AckMessage ackMessage = new AckMessage(message.getNumber(), message.getUID());

        for (var entry : connections.entrySet()) {
            if (entry.getValue().getUID() == message.getUID()) {
                sendAckMessage(ackMessage, entry.getValue());
                entry.getKey().getIsRunning().set(false);
                connections.remove(entry.getKey());
                break;
            }
        }
        System.out.println("Dead thread");
    }

    /**
     * Method used for handling received OutMessages
     * Sends AckMessage to the sender of the OutMessage
     * Sends InMessage to every connected user
     *
     * @param message given OutMessage
     */
    private static void handleOutMessage(OutMessage message) {
        AckMessage ackMessage = new AckMessage(message.getNumber(), message.getUID());
        Connection connection = getByUID(message.getUID());
        assert connection != null;
        sendAckMessage(ackMessage, connection);

        for (var c : connections.values()) {
            InMessage inMessage = new InMessage(c.getNumberOfNextMessage(), connection.getName(), message.getText());
            c.putIntoOutgoing(inMessage);
        }
    }

    /**
     * Private helper method for getting the connection with the given UID
     *
     * @param UID given UID
     * @return connection with given UID if one like that exists otherwise null
     */
    private static Connection getByUID(long UID) {
        for (Connection connection : connections.values()) {
            if (connection.getUID() == UID)
                return connection;
        }
        return null;
    }

    /**
     * Method used for sending AckMessages
     * Tries to send AckMessage up to 10 times until it succeeds
     *
     * @param message    given AckMessage
     * @param connection given Connection to which it sends AckMessage
     */
    private static void sendAckMessage(AckMessage message, Connection connection) {
        int retransmission = 0;

        while (retransmission < 10) {
            try {
                byte[] buffer = message.messageToByte();
                DatagramPacket datagramPacket = new DatagramPacket(buffer, buffer.length);
                datagramPacket.setAddress(connection.getAddress());
                datagramPacket.setPort(connection.getPort());
                socket.send(datagramPacket);
                break;
            } catch (IOException e) {
                retransmission++;
            }
        }
        if (retransmission == 10) System.out.println("Couldn't send AckMessage");
        else
            System.out.println("Sent: " + message.toString());
    }


    /**
     * Private class which models a daemon ClientThread
     * While the client is active and until it receives the right AckMessage
     * tries to send outgoing messages to the client
     */
    private static class ClientThread extends Thread {
        /**
         * Connection of client
         */
        private final Connection connection;
        /**
         * Tells if client is active
         */
        private final AtomicBoolean isRunning = new AtomicBoolean(true);

        /**
         * Creates a new thread for given connection
         *
         * @param connection given connection
         * @throws NullPointerException if given connection is null
         */
        public ClientThread(Connection connection) {
            if (connection == null) throw new NullPointerException("Given connection cannot be null");
            this.connection = connection;
            this.setDaemon(true);
        }

        /**
         * Returns whether the current user is active
         *
         * @return isRunning
         */
        public AtomicBoolean getIsRunning() {
            return isRunning;
        }

        @Override
        public void run() {
            while (isRunning.get()) {
                if (connection.getOutgoing().isEmpty())
                    continue;

                Message message = connection.getNextFromOutgoing();
                int retransmission = 0;
                long number = message.getNumber();


                while (retransmission < 10) {
                    retransmission++;
                    byte[] outgoing = message.messageToByte();
                    DatagramPacket outgoingPacket = new DatagramPacket(outgoing, outgoing.length);
                    outgoingPacket.setAddress(connection.getAddress());
                    outgoingPacket.setPort(connection.getPort());

                    try {
                        socket.send(outgoingPacket);
                        System.out.println("Sent " + message);
                        try {
                            synchronized (connection) {
                                connection.wait(5000);
                                if (connection.hasInIncoming(number)) {
                                    connection.getIncoming().removeIf(m -> m.getNumber() == number);
                                    System.out.println("Got right ack");
                                    break;
                                }
                            }
                        } catch (InterruptedException ignored) {
                        }
                    } catch (IOException e) {
                        break;
                    }
                }
            }
        }
    }
}
