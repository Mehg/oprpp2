package oprpp2.hw02.client;

import oprpp2.hw02.messages.ByeMessage;
import oprpp2.hw02.messages.OutMessage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Class which models a client GUI
 */
public class ClientGUI extends JFrame {
    /**
     * JTextArea for displaying InMessages
     */
    private JTextArea textArea;
    /**
     * JTextField for sending messages
     */
    private JTextField textField;
    /**
     * Client's name
     */
    private final String name;
    /**
     * Number of next message
     */
    private long number;
    /**
     * Clients UID
     */
    private final long UID;

    /**
     * Creates a new ClientGUI with given parameters
     *
     * @param name given Client's name
     * @param UID  given Client's UID
     */
    public ClientGUI(String name, long UID) {
        this.name = name;
        this.number = 1;
        this.UID = UID;

        setLocation(0, 0);
        setSize(600, 300);
        setTitle(name);
        WindowListener wl = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                ByeMessage byeMessage = new ByeMessage(number++, UID);
                Main.putIntoOutgoing(byeMessage);
                dispose();
            }
        };

        addWindowListener(wl);

        initGUI();
    }

    /**
     * Initializes GUI
     */
    private void initGUI() {
        ScrollPane scrollPane = new ScrollPane();

        textArea = new JTextArea();
        textArea.setEditable(false);

        textField = new JTextField();

        textField.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OutMessage outMessage = new OutMessage(number++, UID, textField.getText());
                Main.putIntoOutgoing(outMessage);
                textField.setText("");
            }
        });

        scrollPane.add(textArea);
        add(textField, BorderLayout.PAGE_START);
        add(scrollPane, BorderLayout.CENTER);
    }

    /**
     * Returns reference to text area
     *
     * @return text area
     */
    JTextArea getTextArea() {
        return textArea;
    }
}
