package oprpp2.hw02.client;

import oprpp2.hw02.messages.*;

import javax.swing.*;
import java.io.IOException;
import java.net.*;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Class which models a client who can send and receive messages
 */
public class Main {
    /**
     * Clients InetAddress
     */
    private static InetAddress address;

    /**
     * Clients port
     */
    private static int port;

    /**
     * Clients name
     */
    private static String name;

    /**
     * Clients randkey
     */
    private static long randkey;

    /**
     * Clients UID
     */
    private static Long UID;

    /**
     * Socket
     */
    private static DatagramSocket socket;

    /**
     * Queue of outgoing messages
     */
    private static final BlockingQueue<Message> outgoingQueue = new LinkedBlockingQueue<>();

    /**
     * Queue of incoming messages
     */
    private static final BlockingQueue<Message> incomingQueue = new LinkedBlockingQueue<>();

    /**
     * Number of last received message
     */
    private static long lastReceivedNumber =0;

    /**
     * Helper thread used for handling OutMessages
     */
    private static OutThread outThread;

    /**
     * Client GUI
     */
    private static ClientGUI clientGUI;

    /**
     * Whether the client is active - true until ByeMessage
     */
    private static boolean isRunning;

    /**
     * Main method which receives message
     *
     * @param args arguments: args[0] - server address, args[1] - server port, args[2] - client name
     */
    public static void main(String[] args) {
        if (args.length != 3) {
            System.out.println("Expected three arguments: address, port and name");
            return;
        }

        try {
            handleArgs(args);
        } catch (UnknownHostException e) {
            System.out.println("Unknown host " + e.getMessage());
            return;
        } catch (NumberFormatException e) {
            System.out.println("Cannot parse port - " + args[1] + " is not a valid port");
            return;
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return;
        }

        try {
            socket = new DatagramSocket();
            isRunning = true;
            sendHelloMessage();
            Object lock = new Object();
            outThread = new OutThread(lock);
            outThread.start();
            clientGUI = new ClientGUI(name, UID);
            SwingUtilities.invokeLater(() -> clientGUI.setVisible(true));

            while (isRunning) {
                byte[] incoming = new byte[500];
                DatagramPacket incomingPacket = new DatagramPacket(incoming, incoming.length);

                try {
                    socket.receive(incomingPacket);
                } catch (IOException e) {
                    continue;
                }

                Message message = decodeMessage(incoming);
                System.out.println(message);
                switch (message.getCode()) {
                    case 2 -> handleAckMessage((AckMessage) message);
                    case 5 -> handleInMessage((InMessage) message);
                    default -> System.out.println("Unexpected message");
                }
            }

        } catch (SocketException e) {
            System.out.println("Something went wrong with creating a DatagramSocket");
            System.out.println(e.getMessage());
        }

        socket.close();
    }

    /**
     * Private helper method used for checking arguments
     *
     * @param args given arguments
     * @throws UnknownHostException     if InetAddress is not known
     * @throws IllegalArgumentException if port not parsable or wrong (less than 1 or greater than 65535)
     */
    private static void handleArgs(String[] args) throws UnknownHostException, IllegalArgumentException {
        address = InetAddress.getByName(args[0]);
        port = Integer.parseInt(args[1]);
        if (port < 1 || port > 65535)
            throw new IllegalArgumentException("Port has to be between 1 and 65535");
        name = args[2];
        randkey = new Random().nextLong();
    }

    /**
     * Private helper method used for decoding messages from given byte array
     *
     * @param array given array
     * @return message representation of the given array
     */
    private static Message decodeMessage(byte[] array) {
        Message message;
        switch (array[0]) {
            case 1 -> message = new HelloMessage();
            case 2 -> message = new AckMessage();
            case 3 -> message = new ByeMessage();
            case 4 -> message = new OutMessage();
            case 5 -> message = new InMessage();
            default -> throw new IllegalArgumentException("Unexpected message type");
        }
        message.byteToMessage(array);
        return message;
    }

    /**
     * Method used for handling AckMessages.
     * Puts AckMessage into incoming queue and notifies OutThread
     *
     * @param message given AckMessage
     * @throws NullPointerException if given AckMessage is null
     */
    private static void handleAckMessage(AckMessage message) {
        if (message == null) throw new NullPointerException("Given message cannot be null");
        synchronized (outThread.getLock()) {
            putIntoIncoming(message);
            outThread.getLock().notify();
        }
    }

    /**
     * Method used for handling InMessage
     * If InMessage isn't present in last 10 messages, displays it on client's GUI
     * Sends an AckMessage to the server
     *
     * @param message given InMessage
     * @throws NullPointerException if given InMessage is null
     */
    private static void handleInMessage(InMessage message) {
        if (message == null) throw new NullPointerException("Given InMessage cannot be null");

        if (lastReceivedNumber < message.getNumber()) {
            String sb = "[/" + address + ":" + port + "]" +
                    " Poruka od korisnika: " + message.getName() +
                    "\n" + message.getText() + "\n";

            clientGUI.getTextArea().append(sb);
            lastReceivedNumber =message.getNumber();
        }
        AckMessage ackMessage = new AckMessage(message.getNumber(), UID);
        byte[] buffer = ackMessage.messageToByte();
        DatagramPacket outgoing = new DatagramPacket(buffer, buffer.length);
        outgoing.setAddress(address);
        outgoing.setPort(port);

        try {
            socket.send(outgoing);
        } catch (IOException e) {
            System.out.println("Something went wrong with sending AKC");
        }
    }

    /**
     * Method used for initiating communication with the server
     * Sends HelloMessages until the server responds with an AckMessage
     */
    private static void sendHelloMessage() {
        HelloMessage helloMessage = new HelloMessage(0, name, randkey);
        System.out.println("Sending hello: " + helloMessage);


        byte[] bufferOutgoing = helloMessage.messageToByte();
        DatagramPacket outgoing = new DatagramPacket(bufferOutgoing, bufferOutgoing.length);
        outgoing.setAddress(address);
        outgoing.setPort(port);

        AckMessage ackMessage = new AckMessage();
        byte[] bufferIncoming = ackMessage.messageToByte();
        DatagramPacket incoming = new DatagramPacket(bufferIncoming, bufferIncoming.length);


        int counter = 0;
        while (counter < 10) {
            counter++;

            try {
                socket.send(outgoing);
                System.out.println("Sent hello");
                try {
                    socket.setSoTimeout(5000);
                } catch (SocketException e) {
                    System.out.println("Socket Exception while trying to set timeout");
                    System.out.println(e.getMessage());
                    break;
                }

                try {
                    socket.receive(incoming);
                } catch (SocketTimeoutException e) {
                    continue;
                } catch (IOException e) {
                    System.out.println("IOException while receiving");
                    System.out.println(e.getMessage());
                    break;
                }

                ackMessage.byteToMessage(incoming.getData());
                if (ackMessage.getNumber() == helloMessage.getNumber())
                    System.out.println("Received right ack");
                else
                    System.out.println("Wrong ack number");
                UID = ackMessage.getUID();
                break;
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        System.out.println();
    }

    /**
     * Adds given message at the end of the outgoing queue
     *
     * @param message given message
     * @throws NullPointerException if given message is null
     */
    public static void putIntoOutgoing(Message message) {
        while (true) {
            try {
                outgoingQueue.put(message);
                break;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Adds given message at the end of the incoming queue
     *
     * @param message given message
     * @throws NullPointerException if given message is null
     */
    public static void putIntoIncoming(Message message) {
        while (true) {
            try {
                incomingQueue.put(message);
                break;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Check if there's a message in the incoming queue with number same as given number
     *
     * @param number given number
     * @return true if such a message is present, otherwise false
     */
    private static boolean hasInIncoming(long number) {
        for (Message message : incomingQueue) {
            if (message.getNumber() == number)
                return true;
        }
        return false;
    }

    /**
     * Private class which models a daemon OutThread
     * While the client is active tries to send messages from the outgoing queue
     * while checking the incoming queue for the right AckMessage
     */
    private static class OutThread extends Thread {
        /**
         * Thread lock
         */
        private final Object lock;

        /**
         * Creates a new OutThread daemon thread with given lock
         *
         * @param lock given lock
         */
        public OutThread(Object lock) {
            if (lock == null) throw new NullPointerException("Lock cannot be null");
            this.lock = lock;
            this.setDaemon(true);
        }

        /**
         * Returns this thread's lock
         *
         * @return lock
         */
        public Object getLock() {
            return lock;
        }

        @Override
        public void run() {
            while (isRunning) {
                if (outgoingQueue.isEmpty())
                    continue;

                Message message = outgoingQueue.poll();
                byte[] outgoing = message.messageToByte();
                DatagramPacket outgoingPacket = new DatagramPacket(outgoing, outgoing.length);
                outgoingPacket.setAddress(address);
                outgoingPacket.setPort(port);

                int retransmission = 0;
                while (retransmission < 10) {
                    retransmission++;
                    try {
                        socket.send(outgoingPacket);
                        System.out.println("Sent " + message);
                        try {
                            synchronized (lock) {
                                lock.wait(5000);
                                if (hasInIncoming(message.getNumber())) {
                                    incomingQueue.removeIf(m -> m.getNumber() == message.getNumber());
                                    System.out.println("Got right ack");
                                    if (message instanceof ByeMessage) {
                                        isRunning = false;
                                    }
                                    break;
                                }
                            }
                        } catch (InterruptedException ignored) {
                        }
                    } catch (IOException e) {
                        break;
                    }
                }
            }
        }
    }
}
