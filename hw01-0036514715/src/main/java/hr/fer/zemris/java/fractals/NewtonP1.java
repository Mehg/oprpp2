package hr.fer.zemris.java.fractals;

import hr.fer.oprpp1.math.Complex;
import hr.fer.oprpp1.math.ComplexRootedPolynomial;
import hr.fer.zemris.java.fractals.viewer.FractalViewer;
import hr.fer.zemris.java.fractals.viewer.IFractalProducer;
import hr.fer.zemris.java.fractals.viewer.IFractalResultObserver;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * Class for drawing Newton-Raphson iteration based fractal viewer with multiple workers and tracks with FixedThreadPool
 */
public class NewtonP1 {
    /**
     * Main method which extracts number of workers and number of tracks and starts the implementation of IFractalProducer
     *
     * @param args number of workers and number of tracks
     *             if not defined - number of workers = number of available processors
     *             - number of tracks = number of available processors*4
     */
    public static void main(String[] args) {
        System.out.println("Welcome to Newton-Raphson iteration-based fractal viewer.");
        System.out.println("Please enter at least two roots, one root per line. Enter 'done' when done.");

        int numberOfWorkers = Runtime.getRuntime().availableProcessors();
        int numberOfTracks = numberOfWorkers * 4;

        int userWorkers = -1;
        int userTracks = -1;

        for (int i = 0; i < args.length; i++) {
            String argument = args[i].trim();

            if (argument.startsWith("--tracks=") && userTracks == -1) {
                userTracks = Integer.parseInt(argument.substring(9));
            } else if (argument.startsWith("-t") && userTracks == -1) {
                userTracks = Integer.parseInt(args[++i]);
            } else if (argument.startsWith("--workers=") && userWorkers == -1) {
                userWorkers = Integer.parseInt(argument.substring(10));
            } else if (argument.startsWith("-w") && userWorkers == -1) {
                userWorkers = Integer.parseInt(args[++i]);
            } else {
                System.exit(1);
            }

        }

        if (userWorkers != -1) numberOfWorkers = userWorkers;
        if (userTracks != -1) numberOfTracks = userTracks;

        List<Complex> roots = getUserRoots();

        System.out.println("Image of fractal will appear shortly. Thank you.");
        FractalViewer.show(new NewtonProducer(new ComplexRootedPolynomial(Complex.ONE, roots.toArray(Complex[]::new)),
                numberOfWorkers, numberOfTracks));
    }

    /**
     * Helper method which extracts arguments from user
     *
     * @return List of Complex numbers which represent the roots for ComplexRootedPolynomial
     */
    static List<Complex> getUserRoots() {
        List<Complex> roots = new ArrayList<>();
        int i = 1;
        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.print("Root " + i + "> ");
            String line = sc.nextLine();

            if (line.trim().equalsIgnoreCase("done"))
                if (roots.size() < 2) {
                    System.out.println("Please enter at least two roots");
                    continue;
                } else
                    break;

            try {
                roots.add(Complex.parse(line.trim()));
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
                i--;
            }

            i++;
        }

        sc.close();
        return roots;
    }


    /**
     * Implementation of work for Newton-Raphson fractals
     */
    public static class NewtonWork implements Runnable {
        /**
         * minimum of real axis
         */
        private final double reMin;
        /**
         * maximum of real axis
         */
        private final double reMax;
        /**
         * minimum of imaginary axis
         */
        private final double imMin;
        /**
         * maximum of imaginary axis
         */
        private final double imMax;
        /**
         * width
         */
        private final int width;
        /**
         * height
         */
        private final int height;
        /**
         * minimum y value
         */
        private final int yMin;
        /**
         * maximum y value
         */
        private final int yMax;
        /**
         * data
         */
        private final short[] data;
        /**
         * cancel
         */
        private final AtomicBoolean cancel;
        /**
         * ComplexRootedPolynomial which is used as base function
         */
        private final ComplexRootedPolynomial polynomial;
        /**
         * Maximum number of iterations
         */
        private final int maxIterations;
        /**
         * Convergence threshold
         */
        private final double convergenceThreshold;
        /**
         * Root threshold
         */
        private final double rootThreshold;


        /**
         * Creates a new NewtonWork from given parameters
         *
         * @param reMin                minimum of real axis
         * @param reMax                maximum of real axis
         * @param imMin                minimum of imaginary axis
         * @param imMax                maximum of imaginary axis
         * @param width                width
         * @param height               height
         * @param yMin                 minimum of y coordinate
         * @param yMax                 maximum of y coordinate
         * @param maxIterations        maximum number of iterations
         * @param data                 data
         * @param cancel               cancel
         * @param polynomial           polynomial
         * @param convergenceThreshold convergence threshold
         * @param rootThreshold        root threshold
         */
        public NewtonWork(double reMin, double reMax, double imMin,
                          double imMax, int width, int height, int yMin, int yMax,
                          int maxIterations, short[] data, AtomicBoolean cancel, ComplexRootedPolynomial polynomial,
                          double convergenceThreshold, double rootThreshold) {
            this.reMin = reMin;
            this.reMax = reMax;
            this.imMin = imMin;
            this.imMax = imMax;
            this.width = width;
            this.height = height;
            this.yMin = yMin;
            this.yMax = yMax;
            this.maxIterations = maxIterations;
            this.data = data;
            this.cancel = cancel;
            this.polynomial = polynomial;
            this.convergenceThreshold = convergenceThreshold;
            this.rootThreshold = rootThreshold;

        }

        @Override
        public void run() {
            int offset = width * yMin;
            for (int y = yMin; y <= yMax; y++) {
                if (cancel.get()) break;
                for (int x = 0; x < width; x++) {
                    Complex zn = mapToComplexPlain(x, y);
                    Complex znOld;

                    int i = 0;

                    do {
                        znOld = zn;
                        zn = zn.sub(polynomial.apply(zn).divide(polynomial.toComplexPolynom().derive().apply(zn)));
                        i++;
                    } while (znOld.sub(zn).module() > convergenceThreshold && i < maxIterations);

                    data[offset++] = (short) (polynomial.indexOfClosestRootFor(zn, rootThreshold) + 1);
                }
            }
        }

        /**
         * Private helper function for mapping a point to the Complex plain
         *
         * @param x given x coordinate
         * @param y given y coordinate
         * @return Complex
         */
        private Complex mapToComplexPlain(int x, int y) {
            double real = x * (reMax - reMin) / (width - 1.0) + reMin;
            double imaginary = (height - 1.0 - y) * (imMax - imMin) / (height - 1.0) + imMin;

            return new Complex(real, imaginary);
        }
    }

    private static class NewtonProducer implements IFractalProducer {
        /**
         * ComplexRootedPolynomial which is used as base function
         */
        private final ComplexRootedPolynomial polynomial;
        /**
         * FixedThreadPool
         */
        private ExecutorService pool;
        /**
         * Maximum number of iterations
         */
        private final int maxIterations;
        /**
         * Convergence threshold
         */
        private final double convergenceThreshold;
        /**
         * Root threshold
         */
        private final double rootThreshold;
        /**
         * Number of workers
         */
        private final int numberOfWorkers;
        /**
         * Number of tracks
         */
        private int numberOfTracks;

        /**
         * Creates a new NewtonParallelProducer from given parameters
         *
         * @param polynomial      given polynomial
         * @param numberOfWorkers given numberOfWorkers
         * @param numberOfTracks  given numberOfTracks
         */
        public NewtonProducer(ComplexRootedPolynomial polynomial, int numberOfWorkers, int numberOfTracks) {
            this.polynomial = polynomial;
            this.maxIterations = 16 * 16 * 16;
            this.convergenceThreshold = 0.001;
            this.rootThreshold = 0.002;
            this.numberOfWorkers = numberOfWorkers;
            this.numberOfTracks = numberOfTracks;
        }

        @Override
        public void produce(double reMin, double reMax, double imMin, double imMax, int width, int height, long requestNo,
                            IFractalResultObserver iFractalResultObserver, AtomicBoolean cancel) {

            int numberForOneTrack = height / numberOfTracks;
            if (numberOfTracks > width) numberOfTracks = width;

            System.out.println("Effective number of threads: " + numberOfWorkers);
            System.out.println("Effective number of jobs: " + numberOfTracks);

            short[] data = new short[width * height];

            List<Future<?>> futureResults = new ArrayList<>();

            for (int i = 0; i < numberOfTracks; i++) {
                int yMin = i * numberForOneTrack;
                int yMax = (i + 1) * numberForOneTrack - 1;

                if (i == numberOfTracks - 1) {
                    yMax = height - 1;
                }

                NewtonWork job = new NewtonWork(reMin, reMax, imMin, imMax, width, height, yMin, yMax,
                        maxIterations, data, cancel, polynomial, convergenceThreshold, rootThreshold);

                futureResults.add(pool.submit(job));
            }

            for (Future<?> result : futureResults) {
                if (cancel.get()) {
                    return;
                }

                try {
                    result.get();
                } catch (InterruptedException | ExecutionException ignored) {
                }
            }

            System.out.println("Calculation finished, drawing fractal...");
            iFractalResultObserver.acceptResult(data, (short) (polynomial.toComplexPolynom().order() + 1), requestNo);
        }

        @Override
        public void setup() {
            pool = Executors.newFixedThreadPool(numberOfWorkers);
        }

        @Override
        public void close() {
            pool.shutdown();
        }
    }
}
