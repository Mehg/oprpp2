package hr.fer.zemris.java.fractals;

import hr.fer.oprpp1.math.Complex;
import hr.fer.oprpp1.math.ComplexRootedPolynomial;
import hr.fer.zemris.java.fractals.viewer.FractalViewer;
import hr.fer.zemris.java.fractals.viewer.IFractalProducer;
import hr.fer.zemris.java.fractals.viewer.IFractalResultObserver;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class for drawing Newton-Raphson iteration based fractal viewer with multiple workers and tracks with ForkJoinPool
 */
public class NewtonP2 {
    /**
     * Main method which extracts number of workers and number of tracks and starts the implementation of IFractalProducer
     *
     * @param args number of workers and number of tracks
     *             if not defined - number of workers = number of available processors
     *             - number of tracks = number of available processors*4
     */
    public static void main(String[] args) {
        System.out.println("Welcome to Newton-Raphson iteration-based fractal viewer.");
        System.out.println("Please enter at least two roots, one root per line. Enter 'done' when done.");

        if (args.length > 2)
            throw new IllegalArgumentException("Minimum number of tracks must be defined only once");

        int minTracks = args.length == 0 ? 16 : getUserTracks(args);
        System.out.println("MinTracks "+minTracks);

        List<Complex> roots = getUserRoots();

        System.out.println("Image of fractal will appear shortly. Thank you.");
        FractalViewer.show(new NewtonProducer(new ComplexRootedPolynomial(Complex.ONE, roots.toArray(Complex[]::new)),
                minTracks));
    }

    /**
     * Helper method which extracts minimal number of tracks from user
     *
     * @return minimal number of tracks
     */
    static int getUserTracks(String[] args) {
        int userTracks;

        String argument = args[0].trim();

        if (argument.startsWith("--mintracks=") && args.length==1) {
            userTracks = Integer.parseInt(argument.substring(12));
        } else if (argument.startsWith("-m") && args.length==2) {
            userTracks = Integer.parseInt(args[1]);
        } else throw new IllegalArgumentException("Cannot read number of tracks");

        if (userTracks < 1) userTracks = 16;

        return userTracks;
    }


    /**
     * Helper method which extracts arguments from user
     *
     * @return List of Complex numbers which represent the roots for ComplexRootedPolynomial
     */
    static List<Complex> getUserRoots() {
        List<Complex> roots = new ArrayList<>();
        int i = 1;
        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.print("Root " + i + "> ");
            String line = sc.nextLine();

            if (line.trim().equalsIgnoreCase("done"))
                if (roots.size() < 2) {
                    System.out.println("Please enter at least two roots");
                    continue;
                } else
                    break;

            try {
                roots.add(Complex.parse(line.trim()));
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
                i--;
            }

            i++;
        }

        sc.close();
        return roots;
    }


    /**
     * Implementation of work for Newton-Raphson fractals
     */
    public static class NewtonWork extends RecursiveAction {
        /**
         * minimum of real axis
         */
        private final double reMin;
        /**
         * maximum of real axis
         */
        private final double reMax;
        /**
         * minimum of imaginary axis
         */
        private final double imMin;
        /**
         * maximum of imaginary axis
         */
        private final double imMax;
        /**
         * width
         */
        private final int width;
        /**
         * height
         */
        private final int height;
        /**
         * minimum y value
         */
        private final int yMin;
        /**
         * maximum y value
         */
        private final int yMax;

        /**
         * data
         */
        private final short[] data;
        /**
         * cancel
         */
        private final AtomicBoolean cancel;
        /**
         * ComplexRootedPolynomial which is used as base function
         */
        private final ComplexRootedPolynomial polynomial;
        /**
         * Maximum number of iterations
         */
        private final int maxIterations;
        /**
         * Convergence threshold
         */
        private final double convergenceThreshold;
        /**
         * Root threshold
         */
        private final double rootThreshold;

        /**
         * Minimum number of tracks which will not be decomposed
         */
        private final int minTracks;

        /**
         * Creates a new NewtonWork from given parameters
         *
         * @param reMin                minimum of real axis
         * @param reMax                maximum of real axis
         * @param imMin                minimum of imaginary axis
         * @param imMax                maximum of imaginary axis
         * @param width                width
         * @param height               height
         * @param maxIterations        maximum number of iterations
         * @param data                 data
         * @param cancel               cancel
         * @param polynomial           polynomial
         * @param convergenceThreshold convergence threshold
         * @param rootThreshold        root threshold
         */
        public NewtonWork(double reMin, double reMax, double imMin, double imMax, int width, int height, int yMin, int yMax,
                          int maxIterations, short[] data, AtomicBoolean cancel, ComplexRootedPolynomial polynomial,
                          double convergenceThreshold, double rootThreshold, int minTracks) {
            this.reMin = reMin;
            this.reMax = reMax;
            this.imMin = imMin;
            this.imMax = imMax;
            this.width = width;
            this.height = height;
            this.yMin = yMin;
            this.yMax = yMax;
            this.maxIterations = maxIterations;
            this.data = data;
            this.cancel = cancel;
            this.polynomial = polynomial;
            this.convergenceThreshold = convergenceThreshold;
            this.rootThreshold = rootThreshold;
            this.minTracks = minTracks;
        }


        @Override
        protected void compute() {
            int numberForOneTrack = (yMax-yMin) / 2;
            int middle = (yMax + yMin)/2;

            if (numberForOneTrack*2 <= minTracks) {
                computeDirect();
                return;
            }

            NewtonWork work1 = new NewtonWork(reMin, reMax, imMin, imMax, width, height, yMin,
                    middle, maxIterations, data, cancel, polynomial, convergenceThreshold,
                    rootThreshold, minTracks);

            NewtonWork work2 = new NewtonWork(reMin, reMax, imMin, imMax, width, height,
                    middle+1, yMax, maxIterations, data, cancel, polynomial, convergenceThreshold,
                    rootThreshold, minTracks);

            invokeAll(work1, work2);

        }

        private void computeDirect() {
            int offset = width * yMin;

            for (int y = yMin; y <= yMax; y++) {
                if (cancel.get()) break;
                for (int x = 0; x < width; x++) {
                    Complex zn = mapToComplexPlain(x, y);
                    Complex znOld;

                    int i = 0;

                    do {
                        znOld = zn;
                        zn = zn.sub(polynomial.apply(zn).divide(polynomial.toComplexPolynom().derive().apply(zn)));
                        i++;
                    } while (znOld.sub(zn).module() > convergenceThreshold && i < maxIterations);

                    data[offset++] = (short) (polynomial.indexOfClosestRootFor(zn, rootThreshold) + 1);
                }
            }
        }

        /**
         * Private helper function for mapping a point to the Complex plain
         *
         * @param x given x coordinate
         * @param y given y coordinate
         * @return Complex
         */
        private Complex mapToComplexPlain(int x, int y) {
            double real = x * (reMax - reMin) / (width - 1.0) + reMin;
            double imaginary = (height - 1.0 - y) * (imMax - imMin) / (height - 1.0) + imMin;

            return new Complex(real, imaginary);
        }
    }


    private static class NewtonProducer implements IFractalProducer {
        /**
         * ComplexRootedPolynomial which is used as base function
         */
        private final ComplexRootedPolynomial polynomial;
        /**
         * FixedThreadPool
         */
        private ForkJoinPool pool;
        /**
         * Maximum number of iterations
         */
        private final int maxIterations;
        /**
         * Convergence threshold
         */
        private final double convergenceThreshold;
        /**
         * Root threshold
         */
        private final double rootThreshold;

        /**
         * Minimum number of tracks which will not be decomposed
         */
        private final int minTracks;

        public NewtonProducer(ComplexRootedPolynomial polynomial, int minTracks) {
            this.polynomial = polynomial;
            this.maxIterations = 16 * 16 * 16;
            this.convergenceThreshold = 0.001;
            this.rootThreshold = 0.002;
            this.minTracks = minTracks;
        }

        @Override
        public void produce(double reMin, double reMax, double imMin, double imMax, int width, int height, long requestNo,
                            IFractalResultObserver iFractalResultObserver, AtomicBoolean cancel) {

            short[] data = new short[width * height];


            NewtonWork job = new NewtonWork(reMin, reMax, imMin, imMax, width, height, 0, height - 1,
                    maxIterations, data, cancel, polynomial, convergenceThreshold, rootThreshold, minTracks);

            pool.invoke(job);

            System.out.println("Calculation finished, drawing fractal...");
            iFractalResultObserver.acceptResult(data, (short) (polynomial.toComplexPolynom().order() + 1), requestNo);
        }

        @Override
        public void setup() {
            pool = new ForkJoinPool();
        }

        @Override
        public void close() {
            pool.shutdown();
        }
    }


}
