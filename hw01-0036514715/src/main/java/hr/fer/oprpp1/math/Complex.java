package hr.fer.oprpp1.math;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents an unmodifiable complex number
 */
public class Complex {
    /**
     * Real component of the complex number
     */
    private final double real;
    /**
     * Imaginary component of the complex number
     */
    private final double imaginary;

    /**
     * Complex number with real and imaginary set to zero. Represents zero
     */
    public static final Complex ZERO = new Complex(0, 0);
    /**
     * Complex number with real set to 1 and imaginary set to zero. Represents one
     */
    public static final Complex ONE = new Complex(1, 0);
    /**
     * Complex number with real set to -1 and imaginary set to zero. Represents -1
     */
    public static final Complex ONE_NEG = new Complex(-1, 0);
    /**
     * Complex number with real set to zero and imaginary set to 1. Represents i
     */
    public static final Complex IM = new Complex(0, 1);
    /**
     * Complex number with real set to zero and imaginary set to -1. Represents -i
     */
    public static final Complex IM_NEG = new Complex(0, -1);

    /**
     * Creates a new complex number with real and imaginary set to 0
     */
    public Complex() {
        this(0, 0);
    }

    /**
     * Creates a new complex number
     *
     * @param real      real part
     * @param imaginary imaginary part
     */
    public Complex(double real, double imaginary) {
        this.real = real;
        this.imaginary = imaginary;
    }

    /**
     * Returns the module of the complex number
     *
     * @return the module of the complex number
     */
    public double module() {
        return Math.sqrt(real * real + imaginary * imaginary);
    }

    /**
     * Multiplies this complex number to other Complex c
     *
     * @param c other complex number
     * @return product
     * @throws NullPointerException if other complex number is null
     */
    public Complex multiply(Complex c) {
        if (c == null) throw new NullPointerException("other complex number must not be null");

        double real = c.real * this.real - c.imaginary * this.imaginary;
        double imaginary = c.real * this.imaginary + c.imaginary * this.real;

        return new Complex(real, imaginary);
    }

    /**
     * Divides this complex number to other Complex c
     *
     * @param c other complex number
     * @return Complex result
     * @throws NullPointerException if other complex number is null
     */
    public Complex divide(Complex c) {
        if (c == null) throw new NullPointerException("other complex number must not be null");
        Complex conjugate = new Complex(c.real, -c.imaginary);
        Complex upper = this.multiply(conjugate);
        double lower = c.module() * c.module();

        return new Complex(upper.real / lower, upper.imaginary / lower);
    }

    /**
     * Adds this complex number to other Complex c
     *
     * @param c other complex number
     * @return sum
     * @throws NullPointerException if other complex number is null
     */
    public Complex add(Complex c) {
        if (c == null) throw new NullPointerException("other complex number must not be null");
        return new Complex(c.real + this.real, c.imaginary + this.imaginary);
    }

    /**
     * Subtracts other Complex c from this Complex and returns it as new value
     *
     * @param c other Complex
     * @return new Complex which represents the sub
     * @throws NullPointerException if other complex number is null
     */
    public Complex sub(Complex c) {
        if (c == null) throw new NullPointerException("other complex number must not be null");
        return this.add(c.negate());
    }

    /**
     * Returns new Complex which is result of multiplying current with -1
     *
     * @return new Complex which is result of multiplying current with -1
     */
    public Complex negate() {
        return new Complex(-this.real, -this.imaginary);
    }

    /**
     * Computes Complex to the power of n
     *
     * @param n power
     * @return Complex to the power of n
     * @throws IllegalArgumentException if n is less than 0
     */
    public Complex power(int n) {
        if (n < 0) throw new IllegalArgumentException("n must be at least 0");

        double magnitude = this.module();
        double angle = n * this.getAngle();
        magnitude = Math.pow(magnitude, n);

        return fromMagnitudeAndAngle(magnitude, angle);
    }

    /**
     * Computes all roots of Complex
     *
     * @param n nth root
     * @return List of new Complexs that are nth roots of the original
     * @throws IllegalArgumentException if n is less than 1
     */
    public List<Complex> root(int n) {
        if (n < 1) throw new IllegalArgumentException("n must be at least 1");

        double angle = this.getAngle();
        double magnitude = Math.pow(this.getMagnitude(), 1.0 / n);
        double k_angle;
        List<Complex> array = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            k_angle = (angle + 2.0 * i * Math.PI) / n;
            array.add(fromMagnitudeAndAngle(magnitude, k_angle));
        }

        return array;
    }

    /**
     * Method which returns new Complex from given magnitude and angle
     *
     * @param magnitude magnitude
     * @param angle     angle
     * @return Complex number which is represented from given magnitude and angle
     * @throws IllegalArgumentException if magnitude is negative
     */
    private static Complex fromMagnitudeAndAngle(double magnitude, double angle) {
        if (magnitude < 0) throw new IllegalArgumentException("Magnitude has to be at least 0");
        return new Complex(magnitude * Math.cos(angle), magnitude * Math.sin(angle));
    }


    /**
     * Returns angle from complex number
     *
     * @return angle
     */
    private double getAngle() {
        double angle = Math.atan2(imaginary, real);

        if (angle < 0)
            angle = 2 * Math.PI + angle;

        return angle;
    }

    /**
     * Returns magnitude from complex number
     *
     * @return magnitude
     */
    private double getMagnitude() {
        return Math.sqrt(real * real + imaginary * imaginary);
    }


    @Override
    public String toString() {
        return String.format("(%.1f%+.1fi)", real, imaginary);
    }

    /**
     * Method which returns new Complex from given String
     *
     * @param s given String
     * @return new Complex
     * @throws IllegalArgumentException if string cannot be parsed
     * @throws NullPointerException     if string is null
     */
    public static Complex parse(String s) {
        if (s == null) throw new NullPointerException("String must not be null");
        if (s.isBlank()) throw new NullPointerException("String must not be empty");

        s = s.trim();

        if (!s.matches("([+-]?\\s*\\d+(\\.\\d+)?\\s*([+-]?\\s*i(\\d+(\\.\\d+)?)?\\s*)?)|([+-]?\\s*i(\\d+(\\.\\d+)?)?\\s*)"))
            throw new IllegalArgumentException("Invalid string - cannot convert");

        double pm = 1.0;

        if (s.startsWith("+")) s = s.substring(1);
        else if (s.startsWith("-")) {
            s = s.substring(1);
            pm = -1.0;
        }

        String[] positive = s.split("\\+");
        String[] negative = s.split("-");

        if (positive.length == 2 && negative.length == 2)
            throw new IllegalArgumentException("Invalid string.Cannot convert");
        try {
            if (positive.length == 2) {
                double real = pm * Double.parseDouble(positive[0].trim());
                String img = positive[1].trim().substring(1);
                double imaginary;
                if (img.equals("")) imaginary = 1.0;
                else imaginary = Double.parseDouble(img);
                return new Complex(real, imaginary);
            } else if (negative.length == 2) {
                double real = pm * Double.parseDouble(negative[0].trim());
                String img = negative[1].trim().substring(1);
                double imaginary;
                if (img.equals("")) imaginary = 1.0;
                else imaginary = Double.parseDouble(img);
                return new Complex(real, -imaginary);
            } else if (positive.length == 1 && negative.length == 1) {
                double real = 0, imaginary = 0;
                if (s.startsWith("i")) {
                    if (s.length() == 1)
                        imaginary = pm;
                    else
                        imaginary = pm * Double.parseDouble(s.substring(1));
                } else {
                    real = pm * Double.parseDouble(s);
                }
                return new Complex(real, imaginary);
            } else {
                throw new IllegalArgumentException("Invalid string.Cannot convert");
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid string.Cannot convert");
        }
    }


}
