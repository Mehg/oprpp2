package hr.fer.zemris.java.custom.scripting.parser;

import hr.fer.zemris.java.custom.scripting.elems.*;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptLexer;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptToken;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptTokenType;
import hr.fer.zemris.java.custom.scripting.nodes.*;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Class which represents a parser. It reads through the whole given document and creates Nodes based on context
 */
public class SmartScriptParser {
    private final SmartScriptLexer lexer;
    private final Stack<Object> stack;
    private DocumentNode document;

    /**
     * Returns finished DocumentNode which contains all children
     *
     * @return DocumentNode of given document
     */
    public DocumentNode getDocumentNode() {
        return document;
    }

    /**
     * Constructs a new SmartScriptParser which also parses the given text
     *
     * @param text given text
     */
    public SmartScriptParser(String text) {
        this.lexer = new SmartScriptLexer(text);
        this.stack = new Stack<>();
        parse();
    }

    /**
     * Method whose purpose is to get through the whole text and create Nodes with correct parent Nodes
     *
     * @throws SmartScriptParserException if too many end tags or end tag invalid or if tag cannot be parsed
     */
    public void parse() {
        this.document = new DocumentNode();
        stack.push(document);

        while (!isEnd()) {
            SmartScriptToken token = lexer.getToken();
            if (isText(token)) {
                TextNode node = new TextNode((String) token.getValue());
                ((Node) stack.peek()).addChildNode(node);
            } else if (isOpenTag(token)) {
                SmartScriptToken type = lexer.nextToken();
                if (isEqualsSign(type)) {
                    createAndAddEchoNode();
                } else if (isForSign(type)) {
                    createAndAddForLoopNode();
                } else if (isEndTag(type)) {
                    stack.pop();
                    if (stack.isEmpty())
                        throw new SmartScriptParserException("Error in document - too many end tags");
                    if (!lexer.nextToken().getType().equals(SmartScriptTokenType.CLOSE))
                        throw new SmartScriptParserException("Error in end tag");
                } else throw new SmartScriptParserException("Cannot parse tag");
            } else throw new SmartScriptParserException("Is neither text nor tag");
        }
    }

    /**
     * Private method which creates an EchoNode with correct parameters and adds it as a child to parent Node
     *
     * @throws SmartScriptParserException if invalid equals tag
     */
    private void createAndAddEchoNode() {
        ArrayList<Object> arrayEcho = new ArrayList<>();
        try {
            SmartScriptToken arrayEchoToken = lexer.nextToken();
            while (!arrayEchoToken.getType().equals(SmartScriptTokenType.CLOSE)) {
                switch (arrayEchoToken.getType()) {
                    case EOF, OPEN, EQUALS -> throw new SmartScriptParserException("Invalid Equals tag");
                    case DOUBLE -> arrayEcho.add(new ElementConstantDouble((Double) arrayEchoToken.getValue()));
                    case INTEGER -> arrayEcho.add(new ElementConstantInteger((Integer) arrayEchoToken.getValue()));
                    case FUNCTION -> arrayEcho.add(new ElementFunction((String) arrayEchoToken.getValue()));
                    case OPERATOR -> arrayEcho.add(new ElementOperator((String) arrayEchoToken.getValue()));
                    case STRING -> arrayEcho.add(new ElementString((String) arrayEchoToken.getValue()));
                    case VARIABLE -> arrayEcho.add(new ElementVariable((String) arrayEchoToken.getValue()));
                }
                arrayEchoToken = lexer.nextToken();
            }

            Element[] elements = new Element[arrayEcho.size()];

            int i = 0;
            for (Object o : arrayEcho.toArray())
                if (o instanceof Element)
                    elements[i++] = (Element) o;

            EchoNode echo = new EchoNode(elements);
            ((Node) stack.peek()).addChildNode(echo);
        } catch (Exception e) {
            throw new SmartScriptParserException("Something went wrong in equals tag");
        }
    }

    /**
     * Private method which creates a ForLoopNode and adds it as a child to the parent Node.
     * All Nodes after ForLoopNode until End Token will be added as children of this ForLoopNode
     *
     * @throws SmartScriptParserException if wrong format of for tag, or close tag missing, or wrong format of tag
     */
    private void createAndAddForLoopNode() {
        try {
            SmartScriptToken firstVariableToken = lexer.nextToken();
            if (firstVariableToken.getType() != SmartScriptTokenType.VARIABLE)
                throw new SmartScriptParserException("For tag has to start with variable");
            ElementVariable var1 = new ElementVariable((String) firstVariableToken.getValue());


            SmartScriptToken secondToken = lexer.nextToken();
            Element var2 = extractElement(secondToken);

            SmartScriptToken thirdToken = lexer.nextToken();
            Element var3 = extractElement(thirdToken);
            SmartScriptToken fourthToken = lexer.nextToken();
            Element var4 = null;
            if (!fourthToken.getType().equals(SmartScriptTokenType.CLOSE)) {
                var4 = extractElement(fourthToken);
                if (!lexer.nextToken().getType().equals(SmartScriptTokenType.CLOSE))
                    throw new SmartScriptParserException("Close tag in for loop missing");
            }

            ForLoopNode forLoop = new ForLoopNode(var1, var2, var3, var4);
            ((Node) stack.peek()).addChildNode(forLoop);
            stack.push(forLoop);

        } catch (Exception e) {
            throw new SmartScriptParserException("Wrong format of for tag");
        }
    }

    /**
     * Helper method which checks if it is EOF
     *
     * @return true if EOF, otherwise false
     */
    private boolean isEnd() {
        return lexer.nextToken().getType().equals(SmartScriptTokenType.EOF);
    }

    /**
     * Checks if given token is of type SmartScriptTokenType.STRING
     *
     * @param token given token
     * @return true if TokenType is String, otherwise false
     */
    private boolean isText(SmartScriptToken token) {
        return token.getType().equals(SmartScriptTokenType.STRING);
    }

    /**
     * Checks if given token is of type SmartScriptTokenType.OPEN
     *
     * @param token given token
     * @return true if TokenType is OPEN, otherwise false
     */
    private boolean isOpenTag(SmartScriptToken token) {
        return token.getType().equals(SmartScriptTokenType.OPEN);
    }

    /**
     * Checks if given token is of type SmartScriptTokenType.EQUALS
     *
     * @param token given token
     * @return true if TokenType is EQUALS, otherwise false
     */
    private boolean isEqualsSign(SmartScriptToken token) {
        return token.getType().equals(SmartScriptTokenType.EQUALS);
    }

    /**
     * Checks if given token is a for tag
     *
     * @param token given token
     * @return true if for tag, otherwise false
     */
    private boolean isForSign(SmartScriptToken token) {
        return token.getType().equals(SmartScriptTokenType.VARIABLE) && token.getValue().toString().toLowerCase().equals("for");
    }

    /**
     * Checks if given token is an end tag
     *
     * @param token given token
     * @return true if end tag, otherwise false
     */
    private boolean isEndTag(SmartScriptToken token) {
        return token.getType().equals(SmartScriptTokenType.VARIABLE) && token.getValue().toString().toLowerCase().equals("end");
    }

    /**
     * Returns Element form given token
     *
     * @param token given token
     * @return Element based on token type
     * - can be ElementVariable, ElementConstantDouble, ElementConstantInteger, ElementString
     * @throws SmartScriptParserException if token type isn't one of the previously stated
     */
    private Element extractElement(SmartScriptToken token) {
        Element var;
        switch (token.getType()) {
            case VARIABLE -> var = new ElementVariable((String) token.getValue());
            case DOUBLE -> var = new ElementConstantDouble((Double) token.getValue());
            case INTEGER -> var = new ElementConstantInteger((Integer) token.getValue());
            case STRING -> var = new ElementString((String) token.getValue());
            default -> throw new SmartScriptParserException("Cant parse second argument of for statement");
        }
        return var;
    }
}
