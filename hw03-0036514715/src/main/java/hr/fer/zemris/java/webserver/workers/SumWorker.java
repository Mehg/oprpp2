package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * Class which reads parameters a and b from given request and returns the sum
 * If a parameter is not given uses default: "a=1" and "b=2"
 * The sum is returned as a temporary parameter, and calc.smscr script is run
 */
public class SumWorker implements IWebWorker {
    @Override
    public void processRequest(RequestContext context) throws Exception {
        int a = 1;
        int b = 2;

        try {
            a = Integer.parseInt(context.getParameter("a"));
        } catch (Exception ignored) {
        }

        try {
            b = Integer.parseInt(context.getParameter("b"));
        } catch (Exception ignored) {
        }

        int sum = a + b;
        boolean even = (sum % 2) == 0;
        String imgName = even ? "even.jpg" : "odd.jpg";

        context.setTemporaryParameter("zbroj", String.valueOf(sum));
        context.setTemporaryParameter("varA", String.valueOf(a));
        context.setTemporaryParameter("varB", String.valueOf(b));
        context.setTemporaryParameter("imgName", imgName);

        context.getDispatcher().dispatchRequest("/private/pages/calc.smscr");
    }
}
