package hr.fer.zemris.java.custom.scripting.nodes;

import java.util.ArrayList;
import java.util.function.Consumer;

/**
 * Base class for all graph nodes
 */
public abstract class Node implements Consumer<INodeVisitor> {
    private ArrayList<Object> children;

    /**
     * Adds another Node as a child of this node
     *
     * @param child other node
     */
    public void addChildNode(Node child) {
        if (child == null) throw new NullPointerException("Child must not be null");
        if (children == null) {
            children = new ArrayList<>();
        }
        children.add(child);
    }

    /**
     * Returns the number of children the current Node has
     *
     * @return number of children
     */
    public int numberOfChildren() {
        if (children == null) return 0;
        return children.size();
    }

    /**
     * Returns Node which was added as a child of this node on given position
     *
     * @param index given position
     * @return child Node at given position
     */
    public Node getChild(int index) {
        return (Node) children.get(index);
    }
}
