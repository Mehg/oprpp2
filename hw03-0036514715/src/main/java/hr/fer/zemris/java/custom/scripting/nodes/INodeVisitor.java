package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * Interface of a visitor for node classes
 */
public interface INodeVisitor {
    /**
     * Visits text node
     *
     * @param node given text node
     */
    void visitTextNode(TextNode node);

    /**
     * Visits for loop node
     *
     * @param node given for loop node
     */
    void visitForLoopNode(ForLoopNode node);

    /**
     * Visits echo node
     *
     * @param node given echo node
     */
    void visitEchoNode(EchoNode node);

    /**
     * Visits document node
     *
     * @param node given document node
     */
    void visitDocumentNode(DocumentNode node);
}
