package hr.fer.zemris.java.webserver;

/**
 * Interface used for dispatching requests
 */
public interface IDispatcher {
    /**
     * Dispatches request towards given urlPath
     * @param urlPath given urlPath
     * @throws Exception if something went wrong
     */
    void dispatchRequest(String urlPath) throws Exception;
}