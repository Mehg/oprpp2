package hr.fer.zemris.java.custom.scripting.exec;

import hr.fer.zemris.java.webserver.RequestContext;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Stack;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * Class used for handling functions by {@link hr.fer.zemris.java.custom.scripting.elems.ElementFunction} for {@link SmartScriptEngine}
 */
public class Functions {
    /**
     * Private static map of all function names and corresponding functions
     */
    private static final HashMap<String, BiConsumer<Stack<ValueWrapper>, RequestContext>> functions = new HashMap<>() {{
        put("sin", Functions::sin);
        put("decfmt", Functions::decfmt);
        put("dup", Functions::dup);
        put("swap", Functions::swap);
        put("setMimeType", Functions::setMimeType);
        put("paramGet", Functions::paramGet);
        put("pparamGet", Functions::pparamGet);
        put("pparamSet", Functions::pparamSet);
        put("pparamDel", Functions::pparamDel);
        put("tparamGet", Functions::tparamGet);
        put("tparamSet", Functions::tparamSet);
        put("tparamDel", Functions::tparamDel);
    }};

    /**
     * Method used for decoding function from function name and executing it
     *
     * @param name    given name
     * @param stack   given stack needed for executing
     * @param context given context needed for executing
     */
    public static void decodeFunction(String name, Stack<ValueWrapper> stack, RequestContext context) {
        functions.get(name).accept(stack, context);
    }

    /**
     * Private method used for handling sin function
     *
     * @param stack   given stack for getting parameters and placing result
     * @param context given context - ignored
     */
    private static void sin(Stack<ValueWrapper> stack, RequestContext context) {
        ValueWrapper x = stack.pop();
        Number xNumber = (Number) x.getValue();
        double r = Math.sin(Math.toRadians(xNumber.doubleValue()));
        stack.push(new ValueWrapper(r));
    }

    /**
     * Private method used for handling decfmt function
     *
     * @param stack   given stack for getting parameters and placing result
     * @param context given context - ignored
     */
    private static void decfmt(Stack<ValueWrapper> stack, RequestContext context) {
        ValueWrapper f = stack.pop();
        ValueWrapper x = stack.pop();
        DecimalFormat decimalFormat = new DecimalFormat(f.getValue().toString());
        String r = decimalFormat.format(x.getValue());
        stack.push(new ValueWrapper(r));
    }

    /**
     * Private method used for handling dup function
     *
     * @param stack   given stack for getting parameters and placing result
     * @param context given context - ignored
     */
    private static void dup(Stack<ValueWrapper> stack, RequestContext context) {
        ValueWrapper x = stack.pop();
        stack.push(x);
        stack.push(new ValueWrapper(x.getValue()));
    }

    /**
     * Private method used for handling swap function
     *
     * @param stack   given stack for getting parameters and placing result
     * @param context given context - ignored
     */
    private static void swap(Stack<ValueWrapper> stack, RequestContext context) {
        ValueWrapper a = stack.pop();
        ValueWrapper b = stack.pop();
        stack.push(a);
        stack.push(b);
    }

    /**
     * Private method used for handling setMimeType function
     *
     * @param stack   given stack for getting parameters
     * @param context given context - for setting mime type
     */
    private static void setMimeType(Stack<ValueWrapper> stack, RequestContext context) {
        ValueWrapper x = stack.pop();
        context.setMimeType(x.getValue().toString());
    }

    /**
     * Private helper method used for handling getting functions
     *
     * @param stack    given stack for getting parameters and placing result
     * @param function given function for getting value
     */
    private static void pGet(Stack<ValueWrapper> stack, Function<String, String> function) {
        ValueWrapper defaultValue = stack.pop();
        ValueWrapper name = stack.pop();
        String value = function.apply(name.getValue().toString());
        stack.push(value == null ? defaultValue : new ValueWrapper(value));
    }

    /**
     * Private method used for handling paramGet function
     *
     * @param stack   given stack for getting parameters
     * @param context given context - for getting parameters map entry
     */
    private static void paramGet(Stack<ValueWrapper> stack, RequestContext context) {
        pGet(stack, context::getParameter);
    }

    /**
     * Private method used for handling pparamGet function
     *
     * @param stack   given stack for getting parameters
     * @param context given context - for getting permanent parameters map entry
     */
    private static void pparamGet(Stack<ValueWrapper> stack, RequestContext context) {
        pGet(stack, context::getPersistentParameter);
    }

    /**
     * Private method used for handling tparamGet function
     *
     * @param stack   given stack for getting parameters
     * @param context given context - for getting temporary parameters map entry
     */
    private static void tparamGet(Stack<ValueWrapper> stack, RequestContext context) {
        pGet(stack, context::getTemporaryParameter);
    }

    /**
     * Private helper method used for handling setting functions
     *
     * @param stack    given stack for getting parameters and placing result
     * @param function given function for setting value
     */
    private static void pSet(Stack<ValueWrapper> stack, BiConsumer<String, String> function) {
        ValueWrapper name = stack.pop();
        ValueWrapper value = stack.pop();
        function.accept(name.getValue().toString(), value.getValue().toString());
    }

    /**
     * Private method used for handling pparamSet function
     *
     * @param stack   given stack for getting parameters
     * @param context given context - for setting permanent parameters map entry
     */
    private static void pparamSet(Stack<ValueWrapper> stack, RequestContext context) {
        pSet(stack, context::setPersistentParameter);
    }

    /**
     * Private method used for handling tparamSet function
     *
     * @param stack   given stack for getting parameters
     * @param context given context - for setting temporary parameters map entry
     */
    private static void tparamSet(Stack<ValueWrapper> stack, RequestContext context) {
        pSet(stack, context::setTemporaryParameter);
    }

    /**
     * Private method used for handling pparamDel function
     *
     * @param stack   given stack for getting parameters
     * @param context given context - for deleting permanent parameters map entry
     */
    private static void pparamDel(Stack<ValueWrapper> stack, RequestContext context) {
        ValueWrapper name = stack.pop();
        context.removePersistentParameter(name.getValue().toString());
    }

    /**
     * Private method used for handling tparamDel function
     *
     * @param stack   given stack for getting parameters
     * @param context given context - for deleting temporary parameters map entry
     */
    private static void tparamDel(Stack<ValueWrapper> stack, RequestContext context) {
        ValueWrapper name = stack.pop();
        context.removeTemporaryParameter(name.getValue().toString());
    }

}
