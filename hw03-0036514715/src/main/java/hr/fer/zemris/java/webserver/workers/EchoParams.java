package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * Class which displays gotten parameters from given request
 */
public class EchoParams implements IWebWorker {
    @Override
    public void processRequest(RequestContext context) throws Exception {
        context.setMimeType("text/html");
        context.write("<html><body><table>");

        context.write("<tr><th>Parameter name</th><th>Parameter</th></tr>");
        for (String parameter : context.getParameterNames()) {
            context.write("<tr>")
                .write("<td>" + parameter + "</td>")
                .write("<td>" + context.getParameter(parameter) + "</td>")
                .write("</tr>");
        }

        context.write("</table></body></html>");
    }
}