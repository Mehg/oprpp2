package hr.fer.zemris.java.custom.scripting.exec;

import java.util.Objects;
import java.util.function.DoubleBinaryOperator;
import java.util.function.IntBinaryOperator;

/**
 * Class used for wrapping values and performing simple operations on them
 * <p>
 * If performing operations value stored should be null, Integer, Double or String
 * When performing operations value stored will be converted as follows:
 * <pre>
 *      null is converted to 0
 *      String is parsed and converted to either Integer or Double
 *      Integer is treated as Double if operating with another Double
 * </pre>
 */
public class ValueWrapper {
    /**
     * The stored value
     */
    private Object value;

    /**
     * Constructs a new ValueWrapper with stored value set to given value
     *
     * @param value given value
     */
    public ValueWrapper(Object value) {
        this.value = value;
    }

    /**
     * Returns the stored value
     *
     * @return value
     */
    public Object getValue() {
        return value;
    }

    /**
     * Sets current value to given value
     *
     * @param value given value
     */
    public void setValue(Object value) {
        this.value = value;
    }

    /**
     * Adds current value to incValue
     * Result is stored in current value - this changes this objects value
     *
     * @param incValue given incValue
     * @throws RuntimeException if current value or incValue arent either null, Integer, Double or String
     */
    public void add(Object incValue) {
        value = performAction(incValue, Double::sum, Integer::sum);
    }

    /**
     * Subtracts decValue from current value
     * Result is stored in current value - this changes this objects value
     *
     * @param decValue given decValue
     * @throws RuntimeException if current value or decValue arent either null, Integer, Double or String
     */
    public void subtract(Object decValue) {
        value = performAction(decValue, (a, b) -> (a - b), (a, b) -> (a - b));
    }

    /**
     * Multiplies current value and mulValue
     * Result is stored in current value - this changes this objects value
     *
     * @param mulValue given mulValue
     * @throws RuntimeException if current value or mulValue arent either null, Integer, Double or String
     */
    public void multiply(Object mulValue) {
        value = performAction(mulValue, (a, b) -> a * b, (a, b) -> a * b);
    }

    /**
     * Divides current value and divValue
     * Result is stored in currentValue - this changes this objects value
     *
     * @param divValue given divValue
     * @throws RuntimeException if this value or divValue arent either null, Integer, Double or String
     */
    public void divide(Object divValue) {
        value = performAction(divValue, (a, b) -> a / b, (a, b) -> a / b);
    }

    /**
     * Compares current value and withValue
     *
     * @param withValue given withValue
     * @return result of comparison:
     * <pre>
     *      1 if current value is greater than withValue
     *      0 if they are equal
     *      -1 if withValue is greater than current value
     * </pre>
     * @throws RuntimeException if this value or withValue arent either null, Integer, Double or String
     */
    public int numCompare(Object withValue) {
        return performAction(withValue, Double::compare, Integer::compare).intValue();
    }

    /**
     * Private helper method which handles the logic of add, subtract, multiply, divide and numCompare methods
     *
     * @param otherValue      given other value
     * @param doubleFunction  given DoubleBinaryOperator
     * @param integerFunction given IntBinaryOperator
     * @return result
     */
    private Number performAction(Object otherValue, DoubleBinaryOperator doubleFunction, IntBinaryOperator integerFunction) {
        checkClass(value);
        checkClass(otherValue);

        Number valueNumber = value == null || value instanceof String ?
                readFromString((String) value) : (Number) value;

        Number otherValueNumber = otherValue == null || otherValue instanceof String ?
                readFromString((String) otherValue) : (Number) otherValue;

        if (valueNumber instanceof Double || otherValueNumber instanceof Double) {
            return doubleFunction.applyAsDouble(valueNumber.doubleValue(), otherValueNumber.doubleValue());
        } else return integerFunction.applyAsInt(valueNumber.intValue(), otherValueNumber.intValue());


    }

    /**
     * Private helper method which checks whether the given value is null, Integer, Double or String
     *
     * @param value given value
     * @throws RuntimeException if given value is neither null, Integer, Double or String
     */
    private void checkClass(Object value) {
        if (value != null && !(value instanceof Integer)
                && !(value instanceof Double)
                && !(value instanceof String))
            throw new RuntimeException("Value should be null, Integer, Double or String");
    }

    /**
     * Private helper method which parses Number from given String
     *
     * @param value given String
     * @return Number representation of given String
     * @throws RuntimeException if parsing is impossible
     */
    private Number readFromString(String value) {
        if (value == null) return 0;
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            try {
                return Double.parseDouble(value);
            } catch (NumberFormatException ignored) {
            }
        }
        throw new RuntimeException("String doesn't define a number");
    }

    @Override
    public String toString() {
        return "ValueWrapper{" +
                "value=" + value +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValueWrapper that = (ValueWrapper) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
