package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Element which represents a String
 */
public class ElementString extends Element {
    private final String value;

    /**
     * Returns value of ElementString
     *
     * @return vlaue of current ElementString
     */
    public String getValue() {
        return value;
    }

    /**
     * Constructs a new ElementString from given symbol
     *
     * @param value given String
     */

    public ElementString(String value) {
        this.value = value;
    }

    @Override
    public String asText() {
        return "\"" +
                getValue().replace("\\", "\\\\").replace("\"", "\\\"") +
                "\"" + " ";
    }

    /**
     * Returns String value of ElementString
     *
     * @return string value
     */
    @Override
    public String toString() {
        return "\"" +
                getValue().replace("\\", "\\\\").replace("\"", "\\\"") +
                "\"" + " ";
    }
}
