package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Exception thrown by SmartScriptLexer
 */
public class SmartScriptLexerException extends RuntimeException {
    public SmartScriptLexerException(String message) {
        super(message);
    }
}
