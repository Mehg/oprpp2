package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Element which represents an Operator
 */
public class ElementVariable extends Element {
    private final String name;

    /**
     * Constructs a new ElementVariable from given name
     *
     * @param name given name
     */
    public ElementVariable(String name) {
        this.name = name;
    }

    /**
     * Returns value of ElementVariable
     *
     * @return value of current ElementVariable
     */
    public String getName() {
        return name;
    }

    /**
     * Returns String value of variable name
     *
     * @return string value
     */
    @Override
    public String asText() {
        return name;
    }
}
