package hr.fer.zemris.java.webserver;

/**
 * Interface which models all WebWorkers
 */
public interface IWebWorker {
    /**
     * Method which processes given request
     * @param context given context used for getting parameters
     * @throws Exception if something went wrong
     */
    void processRequest(RequestContext context) throws Exception;
}
