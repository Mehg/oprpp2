package hr.fer.zemris.java.custom.scripting.demo;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.nodes.*;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class TreeWriter {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Expected one argument - filepath");
            return;
        }
        Path path = Path.of(args[0]);

        String docBody;
        try {
            docBody = Files.readString(path);
        } catch (IOException e) {
            System.out.println("Couldn't read file");
            return;
        }
        SmartScriptParser p = new SmartScriptParser(docBody);
        WriterVisitor visitor = new WriterVisitor();
        p.getDocumentNode().accept(visitor);
    }

    private static class WriterVisitor implements INodeVisitor {
        private final StringBuilder sb;

        public WriterVisitor() {
            this.sb = new StringBuilder();
        }

        @Override
        public void visitTextNode(TextNode node) {
            sb.append(node);
        }

        @Override
        public void visitForLoopNode(ForLoopNode node) {
            sb.append("{$for ");
            sb.append(node.getVariable().asText());
            sb.append(" ");
            sb.append(node.getStartExpression().asText());
            sb.append(node.getEndExpression().asText());
            if (node.getStepExpression() != null)
                sb.append(node.getStepExpression().asText());
            sb.append(" ");
            sb.append(" $}");
            visitChildNodes(node);
            sb.append("{$end$}");
        }

        @Override
        public void visitEchoNode(EchoNode node) {
            sb.append("{$=");
            for (Element element : node.getElements()) {
                sb.append(element.asText()).append(" ");
            }
            sb.append("$}");
        }

        @Override
        public void visitDocumentNode(DocumentNode node) {
            visitChildNodes(node);
            System.out.println(sb.toString());
        }

        private void visitChildNodes(Node node) {
            for (int i = 0; i < node.numberOfChildren(); i++) {
                node.getChild(i).accept(this);
            }
        }

    }
}
