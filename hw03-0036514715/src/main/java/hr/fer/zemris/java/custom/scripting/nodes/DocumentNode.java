package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * A node representing an entire document
 */
public class DocumentNode extends Node {
    /**
     * Creates a new DocumentNode
     */
    public DocumentNode() {
        super();
    }

    @Override
    public void accept(INodeVisitor iNodeVisitor) {
        iNodeVisitor.visitDocumentNode(this);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < numberOfChildren(); i++) {
            sb.append(getChild(i));
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DocumentNode echoNode = (DocumentNode) o;
        for (int i = 0; i < numberOfChildren(); i++) {
            if (!this.getChild(i).equals(echoNode.getChild(i)))
                return false;
        }
        return true;
    }

}
