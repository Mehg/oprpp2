package hr.fer.zemris.java.webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * Class which models a request context
 */
public class RequestContext {
    /**
     * Output stream for sending responses
     */
    private final OutputStream outputStream;
    /**
     * Charset used in sending responses
     */
    private Charset charset = StandardCharsets.UTF_8;
    /**
     * Encoding used in sending responses
     */
    private String encoding = "UTF-8";
    /**
     * Status code of the response
     */
    private int statusCode = 200;
    /**
     * Status text of the response
     */
    private String statusText = "OK";
    /**
     * Mime type of the response
     */
    private String mimeType = "text/html";
    /**
     * Content length - can be null if not specified
     */
    private Long contentLength = null;
    /**
     * Parameters from request - GET parameters
     */
    private final Map<String, String> parameters;
    /**
     * Temporary parameters
     */
    private final Map<String, String> temporaryParameters;
    /**
     * Persistent parameters
     */
    private final Map<String, String> persistentParameters;
    /**
     * List of output cookies
     */
    private final List<RCCookie> outputCookies;
    /**
     * Boolean value which tells if header was already generated
     */
    private boolean headerGenerated = false;
    /**
     * Given IDispatcher for dispatching requests
     */
    private final IDispatcher dispatcher;
    /**
     * Given session ID
     */
    private final String sid;

    /**
     * Creates a new RequestContext from given parameters
     *
     * @param outputStream         given output stream
     * @param parameters           given GET parameters
     * @param temporaryParameters  given temporary parameters
     * @param persistentParameters given persistent parameters
     * @param outputCookies        given output cookies
     * @param dispatcher           given dispatcher
     * @param sid                  given session ID
     * @throws NullPointerException if given output stream is null
     */
    public RequestContext(OutputStream outputStream, Map<String, String> parameters, Map<String, String> temporaryParameters, Map<String, String> persistentParameters, List<RCCookie> outputCookies, IDispatcher dispatcher, String sid) {
        if (outputStream == null) throw new NullPointerException("Given OutputStream cannot be null");
        this.outputStream = outputStream;
        this.parameters = parameters == null ? new HashMap<>() : parameters;
        this.temporaryParameters = temporaryParameters == null ? new HashMap<>() : temporaryParameters;
        this.persistentParameters = persistentParameters == null ? new HashMap<>() : persistentParameters;
        this.outputCookies = outputCookies == null ? new ArrayList<>() : outputCookies;
        this.dispatcher = dispatcher;
        this.sid = sid;
    }

    /**
     * Creates a new RequestContext from given parameters
     *
     * @param outputStream         given output stream
     * @param parameters           given GET parameters
     * @param persistentParameters given persistent parameters
     * @param outputCookies        given output cookies
     */
    public RequestContext(OutputStream outputStream, Map<String, String> parameters, Map<String, String> persistentParameters, List<RCCookie> outputCookies) {
        this(outputStream, parameters, null, persistentParameters, outputCookies, null, null);
    }

    /**
     * Sets encoding adn charset to given encoding
     *
     * @param encoding given encoding
     * @throws RuntimeException if header was already generated
     */
    public void setEncoding(String encoding) {
        if (headerGenerated) throw new RuntimeException("Header already generated");
        this.encoding = encoding;
        this.charset = Charset.forName(encoding);
    }

    /**
     * Sets status code to given status code
     *
     * @param statusCode given status code
     * @throws RuntimeException if header was already generated
     */
    public void setStatusCode(int statusCode) {
        if (headerGenerated) throw new RuntimeException("Header already generated");
        this.statusCode = statusCode;
    }

    /**
     * Sets status code to given status code
     *
     * @param statusText given status code
     * @throws RuntimeException if header was already generated
     */
    public void setStatusText(String statusText) {
        if (headerGenerated) throw new RuntimeException("Header already generated");
        this.statusText = statusText;
    }

    /**
     * Sets mime type to given mime type
     *
     * @param mimeType given mime type
     * @throws RuntimeException if header was already generated
     */
    public void setMimeType(String mimeType) {
        if (headerGenerated) throw new RuntimeException("Header already generated");
        this.mimeType = mimeType;
    }

    /**
     * Sets content length to given content length
     *
     * @param contentLength given content length
     * @throws RuntimeException if header was already generated
     */
    public void setContentLength(Long contentLength) {
        if (headerGenerated) throw new RuntimeException("Header already generated");
        this.contentLength = contentLength;
    }

    /**
     * Adds given RCCookie to list of output cookies
     *
     * @param cookie given RCCookie
     * @throws RuntimeException if header was already generated
     */
    public void addRCCookie(RCCookie cookie) {
        if (headerGenerated) throw new RuntimeException("Header already generated");
        outputCookies.add(cookie);
    }

    /**
     * Returns parameter from GET parameters for given name
     *
     * @param name given name
     * @return parameter from GET parameters
     */
    public String getParameter(String name) {
        return parameters.get(name);
    }

    /**
     * Returns list of parameter names
     *
     * @return unmodifiable list of parameter names
     */
    public Set<String> getParameterNames() {
        return Collections.unmodifiableSet(parameters.keySet());
    }

    /**
     * Returns parameter from persistent parameters for given name
     *
     * @param name given name
     * @return parameter from persistent parameters
     */
    public String getPersistentParameter(String name) {
        return persistentParameters.get(name);
    }

    /**
     * Returns list of permanent parameter names
     *
     * @return unmodifiable list of permanent parameter names
     */
    public Set<String> getPersistentParameterNames() {
        return Collections.unmodifiableSet(persistentParameters.keySet());
    }

    /**
     * Sets a persistent parameter for given name to given value
     *
     * @param name  given name
     * @param value given value
     */
    public void setPersistentParameter(String name, String value) {
        persistentParameters.put(name, value);
    }

    /**
     * Removes a persistent parameter with given name
     *
     * @param name given name
     */
    public void removePersistentParameter(String name) {
        persistentParameters.remove(name);
    }

    /**
     * Returns parameter from temporary parameters for given name
     *
     * @param name given name
     * @return parameter from temporary parameters
     */
    public String getTemporaryParameter(String name) {
        return temporaryParameters.get(name);
    }

    /**
     * Returns list of temporary parameter names
     *
     * @return unmodifiable list of temporary parameter names
     */
    public Set<String> getTemporaryParameterNames() {
        return Collections.unmodifiableSet(temporaryParameters.keySet());
    }

    /**
     * Returns session ID
     *
     * @return session ID
     */
    public String getSessionID() {
        return sid;
    }

    /**
     * Returns dispatcher
     *
     * @return dispatcher
     */
    public IDispatcher getDispatcher() {
        return dispatcher;
    }

    /**
     * Sets a temporary parameter for given name to given value
     *
     * @param name  given name
     * @param value given value
     */
    public void setTemporaryParameter(String name, String value) {
        temporaryParameters.put(name, value);
    }


    /**
     * Removes a temporary parameter with given name
     *
     * @param name given name
     */
    public void removeTemporaryParameter(String name) {
        temporaryParameters.remove(name);
    }

    /**
     * Writes given data to output stream with encoding
     * If header wasn't generated, generates it
     *
     * @param data given data
     * @return this
     * @throws IOException if something went wrong with writing
     */
    public RequestContext write(byte[] data) throws IOException {
        return write(data, 0, data.length);
    }

    /**
     * Writes given data to output stream with given offset and length
     * If header wasn't generated, generates it
     *
     * @param data   given data
     * @param offset given offset
     * @param len    given length
     * @return this
     * @throws IOException if something went wrong with writing
     */
    public RequestContext write(byte[] data, int offset, int len) throws IOException {
        if (!headerGenerated) {
            String header = generateHeader();
            outputStream.write(header.getBytes(StandardCharsets.ISO_8859_1));
        }

        outputStream.write(data, offset, len);
        return this;
    }

    /**
     * Writes given String to output stream with encoding
     * If header wasn't generated, generates it
     *
     * @param text given String
     * @return this
     * @throws IOException if something went wrong with writing
     */
    public RequestContext write(String text) throws IOException {
        return write(text.getBytes(charset));
    }

    /**
     * Private helper method which generates a header from already specified parameters
     *
     * @return String representation of header
     */
    private String generateHeader() {
        headerGenerated = true;
        StringBuilder sb = new StringBuilder();

        sb.append("HTTP/1.1 ").append(statusCode).append(" ").append(statusText).append("\r\n");
        sb.append("Content-Type: ").append(formatMimeType()).append("\r\n");

        if (contentLength != null)
            sb.append("Content-Length: ").append(contentLength).append("\r\n");

        if (!outputCookies.isEmpty())
            outputCookies.forEach(c -> sb.append("Set-Cookie: ").append(c));


        sb.append("\r\n");

        return sb.toString();
    }

    /**
     * Private helper method used for formatting mime types
     *
     * @return String representation of formatted mime type
     */
    public String formatMimeType() {
        StringBuilder sb = new StringBuilder();
        sb.append(mimeType);
        if (mimeType.startsWith("text/")) {
            sb.append("; charset=").append(encoding);
        }
        return sb.toString();
    }

    /**
     * Class which models a cookie from request context
     */
    public static class RCCookie {
        /**
         * Cookie name
         */
        private final String name;
        /**
         * Cookie value
         */
        private final String value;
        /**
         * Domain where cookie is valid
         */
        private final String domain;
        /**
         * Path where cookie is valid
         */
        private final String path;
        /**
         * Cookie max age
         */
        private final Integer maxAge;

        /**
         * Creates a new cookie from given parameters
         *
         * @param name   given name
         * @param value  given value
         * @param maxAge given max age
         * @param domain given domain
         * @param path   given path
         */
        public RCCookie(String name, String value, Integer maxAge, String domain, String path) {
            this.name = name;
            this.value = value;
            this.maxAge = maxAge;
            this.domain = domain;
            this.path = path;
        }

        /**
         * Return the name of this cookie
         *
         * @return name
         */
        public String getName() {
            return name;
        }

        /**
         * Returns the value of this cookie
         *
         * @return value
         */
        public String getValue() {
            return value;
        }

        /**
         * Returns the domain of this cookie
         *
         * @return domain
         */
        public String getDomain() {
            return domain;
        }

        /**
         * Returns the path of this cookie
         *
         * @return path
         */
        public String getPath() {
            return path;
        }

        /**
         * Returns the max age of this cookie
         *
         * @return max age
         */
        public Integer getMaxAge() {
            return maxAge;
        }


        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(name).append("=\"").append(value).append("\"");
            if (domain != null)
                sb.append("; Domain=").append(domain);
            if (path != null)
                sb.append("; Path=").append(path);
            if (maxAge != null)
                sb.append("; Max-Age=").append(maxAge);
            sb.append("\r\n");
            return sb.toString();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            RCCookie cookie = (RCCookie) o;
            return Objects.equals(name, cookie.name) && Objects.equals(value, cookie.value) && Objects.equals(domain, cookie.domain) && Objects.equals(path, cookie.path) && Objects.equals(maxAge, cookie.maxAge);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, value, domain, path, maxAge);
        }
    }
}
