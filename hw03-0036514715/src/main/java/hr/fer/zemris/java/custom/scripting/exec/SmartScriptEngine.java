package hr.fer.zemris.java.custom.scripting.exec;

import hr.fer.zemris.java.custom.scripting.elems.*;
import hr.fer.zemris.java.custom.scripting.nodes.*;
import hr.fer.zemris.java.webserver.RequestContext;

import java.io.IOException;
import java.util.Stack;

/**
 * Class used as engine for executing SmartScript
 */
public class SmartScriptEngine {
    /**
     * Document node of this SmartScriptEngine
     */
    private final DocumentNode documentNode;
    /**
     * Request context
     */
    private final RequestContext requestContext;
    /**
     * Multistack used in this SmartScriptEngine - for variables and their values
     */
    private final ObjectMultistack multistack = new ObjectMultistack();
    /**
     * INodeVisitor of this SmartScriptEngine
     */
    private final INodeVisitor visitor = new INodeVisitor() {

        @Override
        public void visitTextNode(TextNode node) {
            try {
                requestContext.write(node.getText());
            } catch (IOException e) {
                System.err.println("Cannot write " + e.getMessage());
            }
        }

        @Override
        public void visitForLoopNode(ForLoopNode node) {
            String start = node.getStartExpression().asText();
            String end = node.getEndExpression().asText();
            String step = node.getStepExpression() == null ? "1" : node.getStepExpression().asText();

            String variable = node.getVariable().getName();

            multistack.push(variable, new ValueWrapper(start));

            while (multistack.peek(variable).numCompare(end) <= 0) {
                visitChildNodes(node);
                ValueWrapper valueWrapper = multistack.pop(variable);
                valueWrapper.add(step);
                multistack.push(variable, valueWrapper);
            }

            multistack.pop(variable);
        }

        @Override
        public void visitEchoNode(EchoNode node) {
            Stack<ValueWrapper> echoStack = new Stack<>();

            for (Element element : node.getElements()) {
                if (element instanceof ElementConstantInteger) {
                    ValueWrapper wrapper = new ValueWrapper(((ElementConstantInteger) element).getValue());
                    echoStack.push(wrapper);
                } else if (element instanceof ElementConstantDouble) {
                    ValueWrapper wrapper = new ValueWrapper(((ElementConstantDouble) element).getValue());
                    echoStack.push(wrapper);
                } else if (element instanceof ElementString) {
                    ValueWrapper wrapper = new ValueWrapper(((ElementString) element).getValue());
                    echoStack.push(wrapper);
                } else if (element instanceof ElementVariable) {
                    String name = ((ElementVariable) element).getName();
                    echoStack.push(multistack.peek(name));
                } else if (element instanceof ElementOperator) {
                    String symbol = ((ElementOperator) element).getSymbol();
                    ValueWrapper first = echoStack.pop();
                    ValueWrapper second = echoStack.pop();
                    ValueWrapper result = new ValueWrapper(first.getValue());

                    switch (symbol) {
                        case "+" -> result.add(second.getValue());
                        case "-" -> result.subtract(second.getValue());
                        case "*" -> result.multiply(second.getValue());
                        case "/" -> result.divide(second.getValue());
                        default -> throw new IllegalArgumentException("Unsupported operator " + symbol);
                    }

                    echoStack.push(result);
                } else if (element instanceof ElementFunction) {
                    String name = ((ElementFunction) element).getName();
                    Functions.decodeFunction(name, echoStack, requestContext);
                }
            }

            for (ValueWrapper wrapper : echoStack) {
                try {
                    requestContext.write(wrapper.getValue().toString());
                } catch (IOException e) {
                    System.err.println("Cannot write " + e.getMessage());
                }
            }
        }

        @Override
        public void visitDocumentNode(DocumentNode node) {
            visitChildNodes(node);
        }

        private void visitChildNodes(Node node) {
            int numberOfChildren = node.numberOfChildren();
            for (int i = 0; i < numberOfChildren; i++)
                node.getChild(i).accept(this);
        }
    };

    /**
     * Constructs a new SmartScriptEngine from given parameters
     *
     * @param documentNode   given DocumentNode
     * @param requestContext given RequestContext
     */
    public SmartScriptEngine(DocumentNode documentNode, RequestContext requestContext) {
        if (documentNode == null || requestContext == null)
            throw new NullPointerException("Given DocumentNode and RequestContext cannot be null");

        this.documentNode = documentNode;
        this.requestContext = requestContext;
    }

    /**
     * Executes the SmartScriptEngine
     */
    public void execute() {
        documentNode.accept(visitor);
    }
}
