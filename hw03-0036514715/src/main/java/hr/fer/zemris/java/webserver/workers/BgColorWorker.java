package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

import java.awt.*;

/**
 * Class which parses background color from given request's parameters and displays message
 * Processed background color is set as a persistent parameter
 */
public class BgColorWorker implements IWebWorker {
    @Override
    public void processRequest(RequestContext context) throws Exception {
        String color = context.getParameter("bgcolor");
        context.setMimeType("text/html");
        if (checkValidHex(color)) {
            context.setPersistentParameter("bgcolor", color);
            context.write(getMessage(true));
        } else {
            context.write(getMessage(false));
        }
    }

    /**
     * Private helper method which checks if given String representation of color is valid
     *
     * @param color given String representation of color
     * @return true if valid color, otherwise false
     */
    private boolean checkValidHex(String color) {
        try {
            Color.decode("#" + color);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * Private helper method which generates HTML depending on whether color decoding was successful or not
     *
     * @param updated whether color decoding was successful
     * @return HTML
     */
    private String getMessage(boolean updated) {
        StringBuilder sb = new StringBuilder();
        sb.append("<html><body><h1>");
        if (updated)
            sb.append("Background color has been updated");
        else sb.append("Background color has not been updated");
        sb.append("</h1><a href=\"/index2.html\">/index2.html</a></body></html>");
        return sb.toString();
    }

}