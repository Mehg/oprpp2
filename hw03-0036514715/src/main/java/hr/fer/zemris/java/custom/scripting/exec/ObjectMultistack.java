package hr.fer.zemris.java.custom.scripting.exec;

import java.util.HashMap;
import java.util.Map;

/**
 * Class which represents an Map, but allows the user to store multiple values for same key
 * It provides a stack-like abstraction for values of some key
 */
public class ObjectMultistack {
    /**
     * Internal map
     */
    private final Map<String, MultistackEntry> map;

    /**
     * Creates a new ObjectMultistack
     */
    public ObjectMultistack() {
        this.map = new HashMap<>();
    }

    /**
     * Pushes given valueWrapper as top of values for given key
     *
     * @param keyName      given key
     * @param valueWrapper given valueWrapper
     * @throws NullPointerException ig given key or valueWrapper are null
     */
    public void push(String keyName, ValueWrapper valueWrapper) {
        if (keyName == null || valueWrapper == null)
            throw new NullPointerException("Given key and value wrapper cannot be null");

        MultistackEntry currentTop = map.get(keyName);
        map.put(keyName, new MultistackEntry(valueWrapper, currentTop));
    }

    /**
     * Pops, removes and returns, the top of the values for given key
     *
     * @param keyName given key
     * @return the top of the values for given key
     * @throws NullPointerException          if given key is null
     * @throws EmptyMultistackEntryException if the values are empty for given key
     */
    public ValueWrapper pop(String keyName) {
        MultistackEntry entry = getTop(keyName);
        if (entry.next == null)
            map.remove(keyName);
        else
            map.put(keyName, entry.next);
        return entry.value;
    }

    /**
     * Peeks and returns the top of the values for given key
     *
     * @param keyName given key
     * @return the top of the values for given key
     * @throws NullPointerException          if given key is null
     * @throws EmptyMultistackEntryException if the values are empty for given key
     */
    public ValueWrapper peek(String keyName) {
        return getTop(keyName).value;
    }

    /**
     * Checks if the values are empty for given key
     *
     * @param keyName given key
     * @return true if values are empty, otherwise false
     */
    public boolean isEmpty(String keyName) {
        return !map.containsKey(keyName);
    }

    /**
     * Private helper method which returns the top of values for given key
     *
     * @param keyName given key
     * @return MultistackEntry that is the top of values for given key
     */
    private MultistackEntry getTop(String keyName) {
        if (keyName == null)
            throw new NullPointerException("Given key cannot be null");

        if (isEmpty(keyName))
            throw new EmptyMultistackEntryException();

        return map.get(keyName);
    }


    /**
     * Private class which models a node of the values for ObjectMultistack
     */
    private static class MultistackEntry {
        /**
         * Current ValueWrapper
         */
        private final ValueWrapper value;
        /**
         * Next MultiStackEntry node
         */
        private final MultistackEntry next;

        /**
         * Constructs a new MultistackEntry from given parameters
         *
         * @param value given value
         * @param next  given next MultistackEntry node
         */
        public MultistackEntry(ValueWrapper value, MultistackEntry next) {
            this.value = value;
            this.next = next;
        }
    }
}
