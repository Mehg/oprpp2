package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Enumeration which represents different modes of work for SmartScriptLexer. Possible values - TEXT, TAG
 */
public enum SmartScriptLexerState {
    TEXT, TAG
}
