package hr.fer.zemris.java.webserver;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * Class which models a HTTP server which can read and execute SmartScript
 */
public class SmartHttpServer {
    /**
     * Servers address
     */
    private String address;
    /**
     * Servers domain name
     */
    private String domainName;
    /**
     * Servers port
     */
    private int port;
    /**
     * Number of worker threads
     */
    private int workerThreads;
    /**
     * Servers session timeout
     */
    private int sessionTimeout;
    /**
     * Mime types
     */
    private final Map<String, String> mimeTypes = new HashMap<>();
    /**
     * Server thread
     */
    private final ServerThread serverThread;
    /**
     * Thread pool
     */
    private ExecutorService threadPool;
    /**
     * Document root
     */
    private Path documentRoot;
    /**
     * Workers map
     */
    private final Map<String, IWebWorker> workersMap = new HashMap<>();
    /**
     * Sessions map
     */
    private final Map<String, SessionMapEntry> sessions = new ConcurrentHashMap<>();
    /**
     * Random used for generating nes session IDs
     */
    private final Random sessionRandom = new Random();

    /**
     * Constructs a new SmartHttpServer form given config filename
     *
     * @param configFileName given filename
     */
    public SmartHttpServer(String configFileName) {
        loadProperties(configFileName);
        serverThread = new ServerThread();
    }

    /**
     * Starts current server
     */
    protected synchronized void start() {
        if (!serverThread.isAlive())
            serverThread.start();
        threadPool = Executors.newFixedThreadPool(workerThreads);
        new GarbageCollector().start();
    }

    /**
     * Stops server
     */
    protected synchronized void stop() {
        serverThread.stopRunning();
        threadPool.shutdown();
    }


    /**
     * Method used for loading configurations form given configFileName
     *
     * @param configFileName given configFileName
     */
    private void loadProperties(String configFileName) {
        try {
            Path serverPath = Path.of(configFileName);
            InputStream inputStream = Files.newInputStream(serverPath);

            Properties properties = new Properties();
            properties.load(inputStream);
            this.address = properties.getProperty("server.address");
            this.domainName = properties.getProperty("server.domainName");
            this.port = Integer.parseInt(properties.getProperty("server.port"));
            this.workerThreads = Integer.parseInt(properties.getProperty("server.workerThreads"));
            this.documentRoot = Path.of(properties.getProperty("server.documentRoot"));
            this.sessionTimeout = Integer.parseInt(properties.getProperty("session.timeout"));

            Path mimePath = Path.of(properties.getProperty("server.mimeConfig"));
            Path workersPath = Path.of(properties.getProperty("server.workers"));

            inputStream = Files.newInputStream(mimePath);
            properties.load(inputStream);
            for (Map.Entry<Object, Object> entry : properties.entrySet()) {
                mimeTypes.put(entry.getKey().toString(), entry.getValue().toString());
            }

            inputStream = Files.newInputStream(workersPath);
            properties.load(inputStream);
            for (Map.Entry<Object, Object> entry : properties.entrySet()) {
                String key = entry.getKey().toString();
                String fqcn = entry.getValue().toString();
                if (workersMap.containsKey(key)) {
                    throw new IllegalArgumentException("Worker for given path already exists");
                }

                try {
                    Class<?> referenceToWorker = this.getClass().getClassLoader().loadClass(fqcn);
                    Object newObject = referenceToWorker.newInstance();
                    IWebWorker iWebWorker = (IWebWorker) newObject;
                    workersMap.put(key, iWebWorker);

                } catch (Exception ignored) {
                }

            }

        } catch (IOException e) {
            System.err.println("Something went wrong with reading properties " + e.getMessage());
        }
    }

    /**
     * Class which models a ServerThread for SmartHttpServer
     */
    protected class ServerThread extends Thread {
        /**
         * Whether the server is still running
         */
        private AtomicBoolean running;

        @Override
        public void run() {
            running = new AtomicBoolean(true);

            try (ServerSocket serverSocket = new ServerSocket()) {
                serverSocket.bind(new InetSocketAddress(InetAddress.getByName(address), port));

                while (running.get()) {
                    try {
                        serverSocket.setSoTimeout(1000);
                        Socket client = serverSocket.accept();
                        ClientWorker clientWorker = new ClientWorker(client);
                        threadPool.submit(clientWorker);
                    } catch (SocketTimeoutException ignored) {
                    }
                }
            } catch (IOException e) {
                System.err.println("IOException in server thread " + e.getMessage());
            }
        }

        /**
         * Sets running to false
         */
        public void stopRunning() {
            running.set(false);
        }
    }

    /**
     * Private static class which models a session entry for session maps
     */
    private static class SessionMapEntry {
        /**
         * Session ID
         */
        private final String sid;
        /**
         * Host
         */
        private final String host;
        /**
         * Valid until - when session expires
         */
        private long validUntil;
        /**
         * Session map - persistent parameters
         */
        private final Map<String, String> map;

        /**
         * Creates a new SessionMapEntry from given parameters
         *
         * @param sid        given session ID
         * @param host       given host
         * @param validUntil given time until valid
         */
        public SessionMapEntry(String sid, String host, long validUntil) {
            this.sid = sid;
            this.host = host;
            this.validUntil = validUntil;
            this.map = new HashMap<>();
        }

        @Override
        public String toString() {
            return "SessionMapEntry{" +
                    "sid='" + sid + '\'' +
                    ", host='" + host + '\'' +
                    ", validUntil=" + validUntil +
                    ", map=" + map +
                    '}';
        }
    }

    /**
     * Class which models a ClientWorker who handles client requests
     */
    private class ClientWorker implements Runnable, IDispatcher {
        /**
         * Client socket
         */
        private final Socket clientSocket;
        /**
         * Client input stream
         */
        private InputStream is;
        /**
         * Client output stream
         */
        private OutputStream os;
        /**
         * HTTP version
         */
        private String version;
        /**
         * HTTP method
         */
        private String method;
        /**
         * Host
         */
        private String host;
        /**
         * Parameters map - GET parameters
         */
        private final Map<String, String> parameters = new HashMap<>();
        /**
         * Temporary parameters
         */
        private final Map<String, String> temporaryParameters = new HashMap<>();
        /**
         * Persistent parameters
         */
        private Map<String, String> persistentParameters = new HashMap<>();
        /**
         * List of output cookies
         */
        private final List<RequestContext.RCCookie> outputCookies = new ArrayList<>();
        /**
         * Session ID
         */
        private String SID;
        /**
         * Request context
         */
        private RequestContext context = null;

        /**
         * Constructs a new ClientWorker with given client socket
         *
         * @param clientSocket given client socket
         */
        public ClientWorker(Socket clientSocket) {
            super();
            this.clientSocket = clientSocket;
        }

        @Override
        public void run() {
            try {
                is = clientSocket.getInputStream();
                os = clientSocket.getOutputStream();
                List<String> request = extractHeaders();
                if (request.isEmpty()) {
                    sendEmptyResponse(400, "Bad request");
                    return;
                }

                String firstLine = request.get(0);
                String[] parts = firstLine.split(" ");

                if (parts.length != 3) {
                    sendEmptyResponse(400, "Bad request");
                    return;
                }

                method = parts[0];
                String requestedPathAndParameters = parts[1]; //requestedPath
                version = parts[2];

                if (!method.equalsIgnoreCase("GET") ||
                        !(version.equalsIgnoreCase("HTTP/1.0")
                                || version.equalsIgnoreCase("HTTP/1.1"))) {
                    sendEmptyResponse(400, "Bad request");
                    return;
                }

                Optional<String> line = request.stream().filter(l -> l.startsWith("Host:")).findAny();

                if (line.isPresent()) {
                    host = line.get().substring(5).trim();
                    if (host.contains(":")) {
                        host = host.split(":")[0].trim();
                    }
                } else
                    host = domainName;

                String path = requestedPathAndParameters.split("\\?")[0];
                if (requestedPathAndParameters.contains("?")) {
                    String paramString = requestedPathAndParameters.split("\\?")[1];
                    parseParameters(paramString);
                }

                checkSession(request);
                internalDispatchRequest(path, true);

                os.flush();
                os.close();
                is.close();
                clientSocket.close();
            } catch (IOException e) {
                System.err.println("Something went wrong in client worker" + e.getMessage());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        /**
         * Method which reads request and extracts headers
         *
         * @return List of header lines
         * @throws IOException if unable to read or something wrong
         */
        private List<String> extractHeaders() throws IOException {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            int state = 0;
            l:
            while (true) {
                int b = is.read();
                if (b == -1) {
                    if (bos.size() != 0) {
                        throw new IOException("Incomplete header received.");
                    }
                    return new ArrayList<>();
                }
                if (b != 13) {
                    bos.write(b);
                }
                switch (state) {
                    case 0:
                        if (b == 13) {
                            state = 1;
                        } else if (b == 10) state = 4;
                        break;
                    case 1:
                        if (b == 10) {
                            state = 2;
                        } else state = 0;
                        break;
                    case 2:
                        if (b == 13) {
                            state = 3;
                        } else state = 0;
                        break;
                    case 3:
                    case 4:
                        if (b == 10) {
                            break l;
                        } else state = 0;
                        break;
                }
            }
            String[] lines = bos.toString(StandardCharsets.US_ASCII).split("\n");
            return List.of(lines);
        }

        /**
         * Method used for parsing parameters from given String
         *
         * @param paramString given parameter String
         */
        private void parseParameters(String paramString) {
            if (paramString.isBlank())
                return;

            String[] pairs = paramString.split("&");
            for (String pair : pairs) {
                String[] parts = pair.split("=", 2);
                if (parts.length == 2)
                    parameters.put(parts[0], parts[1]);
                else
                    parameters.put(parts[0], null);
            }
        }

        /**
         * Private helper function for handling sessions
         * Creates a new session if SID is invalid or not given
         * Gets session if SID is valid
         *
         * @param request given request
         */
        private void checkSession(List<String> request) {
            List<String> cookies = request.stream().filter(l -> l.startsWith("Cookie: ")).collect(Collectors.toList());
            String sidCandidate = null;

            for (String cookie : cookies) {
                cookie = cookie.split("Cookie: ", 2)[1];
                String[] entries = cookie.split(";");
                for (String entry : entries) {
                    String name = entry.split("=")[0].trim();
                    String value = entry.split("=")[1].trim();

                    if (name.equalsIgnoreCase("sid")) {
                        sidCandidate = value.substring(1, value.length() - 1);
                    } else {
                        outputCookies.add(new RequestContext.RCCookie(name, value, null, host, null));
                    }
                }
            }

            synchronized (sessions) {
                if (sidCandidate == null) {
                    sidCandidate = createNewSession();
                } else {
                    SessionMapEntry entry = sessions.get(sidCandidate);
                    if (entry == null || !entry.host.equals(host)) {
                        sidCandidate = createNewSession();
                    } else if (entry.validUntil < System.currentTimeMillis()) {
                        sessions.remove(sidCandidate);
                        sidCandidate = createNewSession();
                    } else {
                        entry.validUntil = System.currentTimeMillis() + sessionTimeout * 1000L;
                    }
                }

                outputCookies.add(new RequestContext.RCCookie("sid", sidCandidate, null, host, "/"));
                SID = sidCandidate;
                persistentParameters = sessions.get(sidCandidate).map;
            }
        }


        /**
         * Private helper method which creates a new Session
         *
         * @return session ID
         */
        private String createNewSession() {
            String sidCandidate = getNextSessionID();
            long timeout = sessionTimeout * 1000L + System.currentTimeMillis();
            SessionMapEntry entry = new SessionMapEntry(sidCandidate, host, timeout);
            sessions.put(sidCandidate, entry);
            return sidCandidate;
        }

        /**
         * Private helper method used for generating a new SID
         *
         * @return a new SID
         */
        private String getNextSessionID() {
            int leftLimit = 48; // numeral '0'
            int rightLimit = 122; // letter 'z'
            int targetStringLength = 20;

            return sessionRandom.ints(leftLimit, rightLimit + 1)
                    .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                    .limit(targetStringLength)
                    .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                    .toString();
        }

        /**
         * Method used for sending an empty response
         *
         * @param code given code
         * @param text given text
         * @throws IOException if unable to send
         */
        private void sendEmptyResponse(int code, String text) throws IOException {
            getContext();
            context.setStatusCode(code);
            context.setStatusText(text);
            context.write("");
            os.flush();
            os.close();
            is.close();
            clientSocket.close();
        }

        /**
         * Method used for dispatching requests internally
         *
         * @param urlPath    given urlPath
         * @param directCall whether the call was a direct call
         * @throws Exception if something went wrong
         */
        private void internalDispatchRequest(String urlPath, boolean directCall) throws Exception {
            if (urlPath.startsWith("/ext/")) {
                handleExternalWorker(urlPath.split("/ext/")[1]);
                return;
            } else if (urlPath.startsWith("/private/") || urlPath.equals("/private")) {
                if (directCall) {
                    sendEmptyResponse(404, "Not found");
                    return;
                }
            }
            if (workersMap.containsKey(urlPath)) {
                workersMap.get(urlPath).processRequest(getContext());
                return;
            }
            Path requestedPath = documentRoot.resolve(urlPath.substring(1));
            if (!requestedPath.toString().startsWith(documentRoot.toString())) {
                sendEmptyResponse(403, "Forbidden");
                return;
            }
            File file = requestedPath.toFile();
            if (!file.canRead() || !file.exists() || !file.isFile()) {
                sendEmptyResponse(404, "Not found");
                return;
            }

            String mime;
            int lastIndexOf = urlPath.lastIndexOf('.');
            if (lastIndexOf == -1) {
                mime = "application/octet-stream";
            } else {
                String extension = urlPath.substring(lastIndexOf + 1);
                if (extension.equalsIgnoreCase("smscr")) {
                    handleSMSCR(requestedPath);
                    return;
                } else
                    mime = mimeTypes.get(urlPath.substring(lastIndexOf + 1));
            }

            getContext();
            context.setMimeType(mime);
            context.setStatusCode(200);
            context.setStatusText("OK");
            context.setContentLength(file.length());
            context.write(Files.readAllBytes(requestedPath));
        }

        /**
         * Helper method specific for handling SmartScript - executes it
         *
         * @param script given Path to script
         * @throws IOException if something went wrong
         */
        private void handleSMSCR(Path script) throws IOException {
            String file = Files.readString(script);
            SmartScriptParser parser = new SmartScriptParser(file);

            SmartScriptEngine engine = new SmartScriptEngine(parser.getDocumentNode(), getContext());
            engine.execute();
        }

        /**
         * Method used for handling /ext/ paths and workers
         *
         * @param path given path
         */
        private void handleExternalWorker(String path) {
            String workerName = path.split("\\?")[0];
            String fqcn = "hr.fer.zemris.java.webserver.workers." + workerName;

            try {
                Class<?> referenceToWorker = this.getClass().getClassLoader().loadClass(fqcn);
                Object newObject = referenceToWorker.newInstance();
                IWebWorker iWebWorker = (IWebWorker) newObject;

                iWebWorker.processRequest(getContext());
            } catch (Exception ignored) {
            }
        }

        @Override
        public void dispatchRequest(String urlPath) throws Exception {
            internalDispatchRequest(urlPath, false);
        }

        private RequestContext getContext(){
            if(context==null){
                context = new RequestContext(os, parameters, temporaryParameters, persistentParameters, outputCookies, this, SID);
            }
            return context;
        }

    }


    /**
     * Thread which removes session if they are expired
     */
    private class GarbageCollector extends Thread {
        public GarbageCollector() {
            setDaemon(true);
        }

        @Override
        public void run() {
            while (true) {
                synchronized (sessions) {
                    sessions.entrySet().stream()
                            .filter(e -> e.getValue().validUntil < System.currentTimeMillis())
                            .forEach(e -> sessions.remove(e.getKey()));
                }
                try {
                    Thread.sleep(300000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public static void main(String[] args) {
        String arg;
        if (args.length != 1) {
            arg = "config/server.properties";
        }else
            arg = args[0];

        SmartHttpServer server = new SmartHttpServer(arg);
        server.start();
    }
}
