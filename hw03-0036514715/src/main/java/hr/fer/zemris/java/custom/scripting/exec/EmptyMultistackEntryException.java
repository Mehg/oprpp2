package hr.fer.zemris.java.custom.scripting.exec;

/**
 * Thrown by methods in {@link ObjectMultistack} class to indicate that the stack for given key is empty
 */
public class EmptyMultistackEntryException extends RuntimeException {

    /**
     * Constructs a new EmptyMultistackEntryException
     */
    public EmptyMultistackEntryException() {
    }
}
