package hr.fer.zemris.java.custom.scripting.nodes;


import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementString;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;

/**
 * A node representing a single for-loop constructor
 */
public class ForLoopNode extends Node {
    private final ElementVariable variable;
    private final Element startExpression;
    private final Element endExpression;
    private final Element stepExpression;

    /**
     * Constructs a new ForLoopNode from given parameters
     *
     * @param variable        - ElementVariable which represents given variable
     * @param startExpression - ElementString, ElementConstantInteger, ElementConstantDouble or ElementVariable. Represents starting expression
     * @param endExpression   - ElementString, ElementConstantInteger, ElementConstantDouble or ElementVariable. Represents ending expression
     * @param stepExpression  -  ElementString, ElementConstantInteger, ElementConstantDouble or ElementVariable. Represents step
     */
    public ForLoopNode(ElementVariable variable, Element startExpression, Element endExpression, Element stepExpression) {
        this.variable = variable;
        this.startExpression = startExpression;
        this.endExpression = endExpression;
        this.stepExpression = stepExpression;
    }

    @Override
    public void accept(INodeVisitor iNodeVisitor) {
        iNodeVisitor.visitForLoopNode(this);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{$for ");
        sb.append(variable.asText());
        sb.append(" ");

        if (startExpression instanceof ElementString) {
            sb.append("\"");
            sb.append(((ElementString) startExpression).getValue().replace("\\", "\\\\").replace("\"", "\\\""));
            sb.append("\"");
        } else
            sb.append(startExpression.asText());
        sb.append(" ");
        if (endExpression instanceof ElementString) {
            sb.append("\"");
            sb.append(((ElementString) endExpression).getValue().replace("\\", "\\\\").replace("\"", "\\\""));
            sb.append("\"");
        } else
            sb.append(endExpression.asText());
        sb.append(" ");

        if (stepExpression != null && stepExpression instanceof ElementString) {
            sb.append("\"");
            sb.append(((ElementString) stepExpression).getValue().replace("\\", "\\\\").replace("\"", "\\\""));
            sb.append("\"");
        } else if (stepExpression != null)
            sb.append(stepExpression.asText());
        sb.append(" ");
        sb.append(" $}");
        sb.append(toStringChildren());
        sb.append("{$end$}");
        return sb.toString();
    }

    /**
     * Private method which runs through all children of children and returns their string representation
     *
     * @return string representation of all children
     */
    private String toStringChildren() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < numberOfChildren(); i++) {
            sb.append(getChild(i).toString());
        }
        return sb.toString();
    }

    /**
     * Returns the variable of this ForLoopNode
     *
     * @return variable
     */
    public ElementVariable getVariable() {
        return variable;
    }

    /**
     * Returns the start expression of this ForLoopNode
     *
     * @return start expression
     */
    public Element getStartExpression() {
        return startExpression;
    }

    /**
     * Returns the end expression of this ForLoopNode
     *
     * @return end expression
     */
    public Element getEndExpression() {
        return endExpression;
    }

    /**
     * Returns the step expression of this ForLoopNode
     *
     * @return step expression
     */
    public Element getStepExpression() {
        return stepExpression;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ForLoopNode that = (ForLoopNode) o;
        if (!(this.variable.asText().equals(that.variable.asText()) &&
                this.startExpression.asText().equals(that.startExpression.asText()) &&
                this.endExpression.asText().equals(that.endExpression.asText()) &&
                (this.stepExpression == null && that.stepExpression == null ||
                        this.stepExpression != null && that.stepExpression != null &&
                                this.stepExpression.asText().equals(that.stepExpression.asText())))) return false;

        if (this.numberOfChildren() != that.numberOfChildren()) return false;
        for (int i = 0; i < numberOfChildren(); i++) {
            if (!this.getChild(i).equals(that.getChild(i))) return false;
        }
        return true;
    }

}
