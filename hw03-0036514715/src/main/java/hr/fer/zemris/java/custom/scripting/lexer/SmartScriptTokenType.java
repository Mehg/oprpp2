package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Enumeration of Token Types
 * EOF - end of file
 * DOUBLE - double
 * INTEGER - integer
 * FUNCTION - function
 * OPERATOR - operator - +,-,*,/,^
 * STRING - string
 * VARIABLE - variable
 * OPEN - represents opening of a tag - {$
 * CLOSE - represents closing of a tag - $}
 * EQUALS - special token for equals tag
 */
public enum SmartScriptTokenType {
    EOF, DOUBLE, INTEGER, FUNCTION, OPERATOR, STRING, VARIABLE, OPEN, CLOSE, EQUALS
}
