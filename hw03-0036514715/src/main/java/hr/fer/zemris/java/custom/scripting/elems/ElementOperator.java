package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Element which represents an Operator
 */
public class ElementOperator extends Element {
    private final String symbol;

    /**
     * Returns value of ElementOperator
     *
     * @return vlaue of current ElementOperator
     */
    public String getSymbol() {
        return symbol;
    }

    /**
     * Constructs a new ElementOperator from given symbol
     *
     * @param symbol given symbol
     */
    public ElementOperator(String symbol) {
        this.symbol = symbol;
    }

    /**
     * Returns String value of symbol
     *
     * @return string value
     */
    @Override
    public String asText() {
        return symbol;
    }

    @Override
    public String toString() {
        return symbol;
    }
}
