package hr.fer.zemris.java.custom.scripting.nodes;

import java.util.Objects;

/**
 * A node representing a piece of textual data
 */
public class TextNode extends Node {
    private final String text;

    /**
     * Return text which is stored in TextNode
     *
     * @return text stored in TextNode
     */
    public String getText() {
        return text;
    }

    /**
     * Constructs a new Text node from given string
     *
     * @param text given string
     */
    public TextNode(String text) {
        this.text = text;
    }

    @Override
    public void accept(INodeVisitor iNodeVisitor) {
        iNodeVisitor.visitTextNode(this);
    }

    @Override
    public String toString() {
        String newerString;
        newerString = text.replace("\\", "\\\\");
        newerString = newerString.replace("{", "\\{");
        return newerString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TextNode node = (TextNode) o;
        return Objects.equals(text, node.text);
    }
}

