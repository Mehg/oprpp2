package hr.fer.zemris.java.custom.scripting.exec;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class ValueWrapperTest {
    @Test
    public void testTwoNulls() {
        ValueWrapper valueWrapper = new ValueWrapper(null);
        valueWrapper.add(null);
        assertEquals(0, valueWrapper.getValue());
        valueWrapper = new ValueWrapper(null);
        assertEquals(0, valueWrapper.numCompare(null));
    }

    @Test
    public void testNullAndInteger() {
        ValueWrapper valueWrapper = new ValueWrapper(null);
        valueWrapper.subtract(-3);
        assertEquals(3, valueWrapper.getValue());

        assertEquals(1, valueWrapper.numCompare(null));
    }

    @Test
    public void testNullAndDouble() {
        ValueWrapper valueWrapper = new ValueWrapper(null);
        valueWrapper.multiply(5.0);
        assertEquals(0.0, valueWrapper.getValue());
    }

    @Test
    public void testIntegerAndDouble() {
        ValueWrapper valueWrapper = new ValueWrapper(1);
        assertEquals(0.0, valueWrapper.numCompare(1.0));

        valueWrapper.divide(2.0);
        assertEquals(0.5, valueWrapper.getValue());
    }

    @Test
    public void testStringInteger() {
        ValueWrapper valueWrapper = new ValueWrapper("5");
        valueWrapper.add(0);
        assertEquals(5, valueWrapper.getValue());
    }

    @Test
    public void testStringDouble() {
        ValueWrapper valueWrapper = new ValueWrapper("5.0");
        valueWrapper.divide(1.0);
        assertEquals(5.0, valueWrapper.getValue());
    }

    @Test
    public void testStringDoubleAndNull() {
        ValueWrapper valueWrapper = new ValueWrapper("52.9");
        valueWrapper.subtract(null);
        assertEquals(52.9, valueWrapper.getValue());
    }

    @Test
    public void testStringIntegerE() {
        ValueWrapper valueWrapper = new ValueWrapper("10e-1");
        assertEquals(0, valueWrapper.numCompare(10e-1));
    }

    @Test
    public void testStringDoubleE() {
        ValueWrapper valueWrapper = new ValueWrapper(10.1e10);
        assertEquals(0, valueWrapper.numCompare(10.1e10));

    }

    @Test
    public void wrongString() {
        ValueWrapper valueWrapper = new ValueWrapper("asdfg");
        assertThrows(RuntimeException.class, () -> valueWrapper.add(0));
    }

    @Test
    public void wrongClass() {
        ValueWrapper valueWrapper = new ValueWrapper(true);
        assertThrows(RuntimeException.class, () -> valueWrapper.add(0));
    }
}
