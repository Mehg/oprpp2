package hr.fer.zemris.java.custom.scripting.exec;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class ObjectMultistackTest {
    public static ObjectMultistack objectMultistack;

    @BeforeAll
    static void beforeAll() {
        objectMultistack = new ObjectMultistack();
    }

    @Test
    public void peekNonExistentKey() {
        assertThrows(EmptyMultistackEntryException.class, () -> objectMultistack.peek(""));
    }

    @Test
    public void popNonExistentKey() {
        assertThrows(EmptyMultistackEntryException.class, () -> objectMultistack.pop(""));
    }


    @Test
    public void peekAndPop() {
        objectMultistack.push("key1", new ValueWrapper(1));
        objectMultistack.push("key1", new ValueWrapper(2));
        objectMultistack.push("key1", new ValueWrapper(3));

        assertEquals(new ValueWrapper(3), objectMultistack.peek("key1"));
        assertEquals(new ValueWrapper(3), objectMultistack.pop("key1"));
        assertEquals(new ValueWrapper(2), objectMultistack.pop("key1"));
        assertEquals(new ValueWrapper(1), objectMultistack.pop("key1"));
        assertThrows(EmptyMultistackEntryException.class, () -> objectMultistack.peek("key1"));
        assertThrows(EmptyMultistackEntryException.class, () -> objectMultistack.pop("key1"));
    }

    @Test
    public void peekAndPopWithMultipleKeys() {
        objectMultistack.push("key1", new ValueWrapper(1));
        objectMultistack.push("key1", new ValueWrapper(2));
        objectMultistack.push("key2", new ValueWrapper(3));

        assertEquals(new ValueWrapper(3), objectMultistack.peek("key2"));
        assertEquals(new ValueWrapper(2), objectMultistack.peek("key1"));
        assertEquals(new ValueWrapper(3), objectMultistack.pop("key2"));
        assertEquals(new ValueWrapper(2), objectMultistack.pop("key1"));
        assertEquals(new ValueWrapper(1), objectMultistack.pop("key1"));
        assertThrows(EmptyMultistackEntryException.class, () -> objectMultistack.peek("key2"));
        assertThrows(EmptyMultistackEntryException.class, () -> objectMultistack.pop("key1"));

    }
}
