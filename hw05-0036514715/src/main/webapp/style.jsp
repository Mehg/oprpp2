<%@ page contentType="text/css;charset=UTF-8" pageEncoding="UTF-8" %>

body {
    background-color: #e8e8e8;
    font-family: 'Roboto', sans-serif;
}

a {
    color: black;
    text-decoration: none;
}

a:hover {
    text-decoration: underline;
}