<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <c:url value="/style.jsp" var="style" />
    <link rel="stylesheet" href=${style}>
    <title>Something went wrong!</title>
</head>
<body>
<h1>Error</h1>
<p><%= request.getAttribute("message")%></p>
<a href="${pageContext.request.contextPath}/servleti/index.html">Home page</a>
</body>
</html>
