<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <c:url value="/style.jsp" var="style"/>
    <link rel="stylesheet" href=${style}>
    <title>${poll.title}</title>
</head>
<body>
<h1>${poll.title}</h1>

<p>${poll.message}</p>
<ol>
    <c:forEach var="option" items="${options}">
        <li><a href="${pageContext.request.contextPath}/servleti/glasanje-glasaj?id=${option.id}">${option.optionTitle}</a></li>
    </c:forEach>
</ol>
</body>
</html>
