<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <c:url value="/style.jsp" var="style"/>
    <link rel="stylesheet" href=${style}>
    <title>Polls</title>
</head>
<body>

<dl>
    <c:forEach var="poll" items="${polls}">
        <dt><a href="${pageContext.request.contextPath}/servleti/glasanje?pollID=${poll.id}">${poll.title}</a></dt>
        <dd>${poll.message}</dd>
    </c:forEach>
</dl>

</body>
</html>
