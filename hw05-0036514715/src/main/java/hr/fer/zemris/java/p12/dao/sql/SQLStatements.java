package hr.fer.zemris.java.p12.dao.sql;

/**
 * Class used for storing SQL statements used in SQLDAO class
 */
public class SQLStatements {
    static final String CREATE_POLLS = """
            CREATE TABLE Polls(
                id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
                title VARCHAR(150) NOT NULL,
                message CLOB(2048) NOT NULL
            )""";

    static final String CREATE_POLL_OPTIONS = """
            CREATE TABLE PollOptions(
                id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
                optionTitle VARCHAR(100) NOT NULL,
                optionLink VARCHAR(150) NOT NULL,
                pollID BIGINT,
                votesCount BIGINT,
                FOREIGN KEY (pollID) REFERENCES Polls(id)
            )""";

    static final String POLLS_NO = """
            SELECT COUNT(*)
            FROM Polls""";

    static final String INSERT_POLL = """
            INSERT INTO Polls (title, message) VALUES (?,?)""";

    static final String INSERT_POLL_OPTION = """
            INSERT INTO PollOptions (optionTitle, optionLink, pollID, votesCount) VALUES (?,?,?,?)""";

    static final String SELECT_POLLS = """
            SELECT *
            FROM Polls""";

    static final String SELECT_POLL = """
            SELECT *
            FROM Polls
            WHERE id = ?""";

    static final String SELECT_POLL_OPTINS = """
            SELECT *
            FROM PollOptions
            WHERE pollID = ?""";

    static final String ADD_VOTE = """
            UPDATE PollOptions
            SET votesCount = votesCount+1
            WHERE id = ?""";

    static final String GET_POLL_OPTION = """
            SELECT *
            FROM PollOptions
            WHERE id = ?""";

    static final String GET_POLL_ID_FROM_OPTION = """
            SELECT pollID
            FROM PollOptions
            WHERE id = ?""";
}
