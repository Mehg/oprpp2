package hr.fer.zemris.java.p12;

import hr.fer.zemris.java.p12.dao.sql.SQLConnectionProvider;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.sql.DataSource;

@WebFilter(filterName = "f1", urlPatterns = {"/servleti/*"})
public class ConnectionSetterFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {

        Connection con = get(request.getServletContext());

        try {
            chain.doFilter(request, response);
        } finally {
            close(con);
        }
    }

    public static Connection get(ServletContext context) {
        DataSource ds = (DataSource) context.getAttribute("hr.fer.zemris.dbpool");
        Connection con;
        try {
            con = ds.getConnection();
        } catch (SQLException e) {
            throw new IllegalStateException("Can't connect to database");
        }
        SQLConnectionProvider.setConnection(con);
        return con;
    }

    public static void close(Connection con) {
        SQLConnectionProvider.setConnection(null);
        try {
            con.close();
        } catch (SQLException ignored) {
        }
    }

}