package hr.fer.zemris.java.p12.dao.sql;

import hr.fer.zemris.java.p12.dao.DAO;
import hr.fer.zemris.java.p12.dao.DAOException;
import hr.fer.zemris.java.p12.model.Poll;
import hr.fer.zemris.java.p12.model.PollOption;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static hr.fer.zemris.java.p12.dao.sql.SQLStatements.*;

/**
 * Implementation of a DAO for SQL
 */
public class SQLDAO implements DAO {
    public void createTables() throws DAOException {
        if (tableExists("Polls")) {
            createTable(CREATE_POLLS);
        }
        if (tableExists("PollOptions")) {
            createTable(CREATE_POLL_OPTIONS);
        }
        if (pollsIsEmpty())
            insertInitialPolls();
    }

    /**
     * Private helper method for checking if a table with given tableName exists
     *
     * @param tableName given table name
     * @return true if exists, otherwise false
     */
    private boolean tableExists(String tableName) {
        boolean exists = false;
        Connection con = SQLConnectionProvider.getConnection();
        try (ResultSet resultSet = con.getMetaData().getTables(null, null, null, null)) {
            while (resultSet != null && resultSet.next()) {
                if (resultSet.getString(3).equals(tableName)) {
                    exists = true;
                }
            }
        } catch (SQLException ignored) {
        }
        return exists;
    }

    /**
     * Private helper method for creating tables from given queries
     *
     * @param query given query
     */
    private void createTable(String query) {
        Connection con = SQLConnectionProvider.getConnection();

        try (PreparedStatement preparedStatement = con.prepareStatement(query)) {
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Error while creating table " + e.getMessage());
        }
    }

    /**
     * Private helper method for checking if the Polls table is empty
     *
     * @return true if empty, otherwise false
     */
    private boolean pollsIsEmpty() {
        Connection con = SQLConnectionProvider.getConnection();

        try (PreparedStatement preparedStatement = con.prepareStatement(POLLS_NO)) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet == null || !resultSet.next())
                    return true;
                return resultSet.getInt(1) == 0;
            }
        } catch (SQLException e) {
            throw new DAOException("Error while counting polls " + e.getMessage());
        }
    }

    /**
     * Private helper method for inserting values into the Polls table
     */
    private void insertInitialPolls() {
        Poll poll1 = new Poll("Glasanje za omiljeni bend:", "Od sljedećih bendova, koji Vam je bend najdraži? Kliknite na link kako biste glasali!");
        List<PollOption> pollOptions1 = List.of(
                new PollOption("The Beatles", "https://www.youtube.com/watch?v=z9ypq6_5bsg", 150),
                new PollOption("The Platters", "https://www.youtube.com/watch?v=H2di83WAOhU", 150),
                new PollOption("The Beach Boys", "https://www.youtube.com/watch?v=2s4slliAtQU", 60),
                new PollOption("The Four Seasons", "https://www.youtube.com/watch?v=y8yvnqHmFds", 33),
                new PollOption("The Marcels", "https://www.youtube.com/watch?v=qoi3TH59ZEs", 28),
                new PollOption("The Everly Brothers", "https://www.youtube.com/watch?v=tbU3zdAgiX8", 25),
                new PollOption("The Mamas And The Papas", "https://www.youtube.com/watch?v=N-aK6JnyFmk", 20)
        );
        addNewPoll(poll1);
        for (PollOption option : pollOptions1) {
            option.setPollID(poll1.getId());
            addNewPollOption(option);
        }

        Poll poll2 = new Poll("Glasanje za omiljeni film:", "Od sljedećih filmova, koji Vam je film najdraži? Kliknite na link kako biste glasali!");
        List<PollOption> pollOptions2 = List.of(
                new PollOption("Grave Of The Fireflies (1988)", "https://www.imdb.com/title/tt0095327/", 100),
                new PollOption("Howl's Moving Castle (2004)", "https://www.imdb.com/title/tt0347149/", 101),
                new PollOption("My Neighbor Totoro (1988)", "https://www.imdb.com/title/tt0096283/?ref_=tt_sims_tt", 90),
                new PollOption("Kiki's Delivery Service (1989)", "https://www.imdb.com/title/tt0097814/?ref_=tt_sims_tt", 70)
        );
        addNewPoll(poll2);
        for (PollOption option : pollOptions2) {
            option.setPollID(poll2.getId());
            addNewPollOption(option);
        }
    }

    @Override
    public void addNewPoll(Poll poll) throws DAOException {
        Connection con = SQLConnectionProvider.getConnection();

        try (PreparedStatement preparedStatement = con.prepareStatement(INSERT_POLL, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, poll.getTitle());
            preparedStatement.setString(2, poll.getMessage());

            preparedStatement.executeUpdate();

            try (ResultSet set = preparedStatement.getGeneratedKeys()) {
                set.next();
                poll.setId(set.getInt(1));
            }
        } catch (SQLException e) {
            throw new DAOException("Error while creating poll " + e.getMessage());
        }
    }


    @Override
    public void addNewPollOption(PollOption pollOption) throws DAOException {
        Connection con = SQLConnectionProvider.getConnection();

        try (PreparedStatement preparedStatement = con.prepareStatement(INSERT_POLL_OPTION, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, pollOption.getOptionTitle());
            preparedStatement.setString(2, pollOption.getOptionLink());
            preparedStatement.setInt(3, pollOption.getPollID());
            preparedStatement.setInt(4, pollOption.getVotesCount());

            preparedStatement.executeUpdate();

            try (ResultSet set = preparedStatement.getGeneratedKeys()) {
                set.next();
                pollOption.setId(set.getInt(1));
            }
        } catch (SQLException e) {
            throw new DAOException("Error while creating poll option " + e.getMessage());
        }
    }

    @Override
    public List<Poll> getAllPolls() throws DAOException {
        Connection con = SQLConnectionProvider.getConnection();
        ArrayList<Poll> results = new ArrayList<>();

        try (PreparedStatement preparedStatement = con.prepareStatement(SELECT_POLLS)) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                while (resultSet != null && resultSet.next()) {
                    results.add(new Poll(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getString(3))
                    );
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Error while reading polls " + e.getMessage());
        }
        return results;
    }

    @Override
    public Poll getPoll(int id) throws DAOException {
        Poll retPoll = new Poll("title", "message");
        Connection con = SQLConnectionProvider.getConnection();

        try (PreparedStatement preparedStatement = con.prepareStatement(SELECT_POLL)) {
            preparedStatement.setInt(1, id);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet != null && resultSet.next()) {
                    retPoll = new Poll(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getString(3)
                    );
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Error while updating poll " + e.getMessage());
        }
        return retPoll;
    }

    @Override
    public List<PollOption> getAllOptions(int pollId) throws DAOException {
        List<PollOption> retPollOptions = new ArrayList<>();
        Connection con = SQLConnectionProvider.getConnection();

        try (PreparedStatement preparedStatement = con.prepareStatement(SELECT_POLL_OPTINS)) {
            preparedStatement.setInt(1, pollId);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet != null && resultSet.next()) {
                    retPollOptions.add(new PollOption(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getString(3),
                            resultSet.getInt(4),
                            resultSet.getInt(5)
                    ));
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Error while updating poll " + e.getMessage());
        }
        return retPollOptions;
    }

    @Override
    public void addVote(int id) throws DAOException {
        Connection con = SQLConnectionProvider.getConnection();

        try (PreparedStatement preparedStatement = con.prepareStatement(ADD_VOTE)) {
            preparedStatement.setInt(1, id);

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Error while updating poll " + e.getMessage());
        }
    }

    @Override
    public PollOption getPollOption(int optionID) throws DAOException {
        Connection con = SQLConnectionProvider.getConnection();
        try (PreparedStatement preparedStatement = con.prepareStatement(GET_POLL_OPTION)) {
            preparedStatement.setInt(1, optionID);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet != null && resultSet.next())
                    return new PollOption(resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getString(3),
                            resultSet.getInt(4),
                            resultSet.getInt(5));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
