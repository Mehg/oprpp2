package hr.fer.zemris.java.p12.model;

/**
 * Simple class which models a PollOption
 */
public class PollOption {
    /**
     * Unique identifier of the PollOption
     */
    private int id;
    /**
     * Option title
     */
    private final String optionTitle;
    /**
     * Option link
     */
    private final String optionLink;
    /**
     * Poll id of the Poll which this PollOption belongs to
     */
    private int pollID;
    /**
     * Option votes
     */
    private final int votesCount;

    /**
     * Creates a new PollOption from the given parameters
     *
     * @param id          given id
     * @param optionTitle given option title
     * @param optionLink  given option link
     * @param pollID      given poll id
     * @param votesCount  given votes
     */
    public PollOption(int id, String optionTitle, String optionLink, int pollID, int votesCount) {
        this.id = id;
        this.optionTitle = optionTitle;
        this.optionLink = optionLink;
        this.pollID = pollID;
        this.votesCount = votesCount;
    }

    /**
     * Creates a poll option from the given parameters
     * Id, and pollId are set to -1
     *
     * @param optionTitle given option title
     * @param optionLink  given option link
     * @param votesCount  given votes count
     */
    public PollOption(String optionTitle, String optionLink, int votesCount) {
        this(-1, optionTitle, optionLink, -1, votesCount);
    }

    /**
     * Returns id
     *
     * @return id
     */
    public long getId() {
        return id;
    }

    /**
     * Returns option title
     *
     * @return option title
     */
    public String getOptionTitle() {
        return optionTitle;
    }

    /**
     * Returns option link
     *
     * @return option link
     */
    public String getOptionLink() {
        return optionLink;
    }

    /**
     * Returns poll id
     *
     * @return poll id
     */
    public int getPollID() {
        return pollID;
    }

    /**
     * Returns votes count
     *
     * @return votes count
     */
    public int getVotesCount() {
        return votesCount;
    }

    /**
     * Sets id to given id
     *
     * @param id given id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Sets poll id to given id
     *
     * @param pollID given id
     */
    public void setPollID(int pollID) {
        this.pollID = pollID;
    }
}
