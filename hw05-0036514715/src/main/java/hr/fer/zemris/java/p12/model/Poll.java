package hr.fer.zemris.java.p12.model;

/**
 * Class which models a simple Poll
 */
public class Poll {
    /**
     * unique id of the poll
     */
    private int id;
    /**
     * Title of the poll
     */
    private final String title;
    /**
     * Message of the poll
     */
    private final String message;

    /**
     * Creates a new Poll from given parameters
     *
     * @param id      given id
     * @param title   given title
     * @param message given message
     */
    public Poll(int id, String title, String message) {
        this.id = id;
        this.title = title;
        this.message = message;
    }

    /**
     * Creates a new Poll from given parameters
     * Sets id to -1
     *
     * @param title   given title
     * @param message given message
     */
    public Poll(String title, String message) {
        this(-1, title, message);
    }

    /**
     * Returns Poll id
     *
     * @return Poll id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets Poll id to given id
     *
     * @param id given id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Returns Poll title
     *
     * @return Poll title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Returns Poll message
     *
     * @return Poll message
     */
    public String getMessage() {
        return message;
    }
}
