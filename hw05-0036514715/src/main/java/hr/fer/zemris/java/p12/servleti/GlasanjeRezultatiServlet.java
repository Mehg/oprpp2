package hr.fer.zemris.java.p12.servleti;

import hr.fer.zemris.java.p12.dao.DAOProvider;
import hr.fer.zemris.java.p12.model.Poll;
import hr.fer.zemris.java.p12.model.PollOption;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Servlet which renders voting results for a specific poll
 * Receives pollID as parameter
 */
@WebServlet("/servleti/glasanje-rezultati")
public class GlasanjeRezultatiServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("pollID") == null) {
            req.setAttribute("message", "Not a valid pollID");
            req.getRequestDispatcher("/WEB-INF/pages/error.jsp")
                    .forward(req, resp);
        }

        int pollID = Integer.parseInt(req.getParameter("pollID"));
        Poll poll = DAOProvider.getDao().getPoll(pollID);
        List<PollOption> options = DAOProvider.getDao().getAllOptions(pollID);
        options = options.stream().sorted(Comparator.comparing(PollOption::getVotesCount).reversed()).collect(Collectors.toList());

        req.setAttribute("poll", poll);
        req.setAttribute("options", options);

        req.getRequestDispatcher("/WEB-INF/pages/glasanjeRez.jsp").forward(req, resp);

    }
}
