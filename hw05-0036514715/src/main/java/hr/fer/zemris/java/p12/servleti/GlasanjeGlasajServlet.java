package hr.fer.zemris.java.p12.servleti;

import hr.fer.zemris.java.p12.dao.DAOProvider;
import hr.fer.zemris.java.p12.model.PollOption;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet which handles adding a vote for a specific option
 * Receives a parameter id which represents option id
 * For given option id adds vote to that option
 */
@WebServlet("/servleti/glasanje-glasaj")
public class GlasanjeGlasajServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("id") == null) {
            sendError("Didn't receive poll id", req, resp);
        }

        int id = Integer.parseInt(req.getParameter("id"));
        DAOProvider.getDao().addVote(id);
        PollOption option = DAOProvider.getDao().getPollOption(id);
        if (option == null) {
            sendError("Not a valid option id", req, resp);
            return;
        }

        resp.sendRedirect(req.getContextPath() + "/servleti/glasanje-rezultati?pollID=" + option.getPollID());

    }

    /**
     * Private method for sending error message
     *
     * @param message given message
     * @param req     request
     * @param res     response
     * @throws ServletException exception
     * @throws IOException      exception
     */
    private static void sendError(String message, HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.setAttribute("message", message);
        req.getRequestDispatcher("/WEB-INF/pages/error.jsp")
                .forward(req, res);
    }
}
