package hr.fer.zemris.java.p12;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.DataSources;
import hr.fer.zemris.java.p12.dao.DAO;
import hr.fer.zemris.java.p12.dao.DAOProvider;
import hr.fer.zemris.java.p12.dao.sql.SQLConnectionProvider;
import hr.fer.zemris.java.p12.dao.sql.SQLDAO;

@WebListener
public class InitialisationListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        String connectionURL = getConnectionURL(sce);

        ComboPooledDataSource cpds = new ComboPooledDataSource();
        try {
            cpds.setDriverClass("org.apache.derby.client.ClientAutoloadedDriver");
        } catch (PropertyVetoException e) {
            throw new RuntimeException("Error while initializing database pool: " + e.getMessage());
        }

        cpds.setJdbcUrl(connectionURL);
        sce.getServletContext().setAttribute("hr.fer.zemris.dbpool", cpds);
        Connection connection = ConnectionSetterFilter.get(sce.getServletContext());
        ((SQLDAO) DAOProvider.getDao()).createTables();
        ConnectionSetterFilter.close(connection);
    }

    private String getConnectionURL(ServletContextEvent event) {
        Path path = Paths.get(event.getServletContext().getRealPath("/WEB-INF/dbsettings.properties"));

        if (!Files.exists(path)) {
            throw new IllegalArgumentException("Can't fint the properties file - path should be " + path);
        }

        Properties properties = new Properties();
        try {
            properties.load(Files.newInputStream(path));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "jdbc:derby://" + properties.getProperty("host") + ":" + properties.getProperty("port") +
                "/" + properties.getProperty("name") + ";user=" + properties.getProperty("user") +
                ";password=" + properties.getProperty("password");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ComboPooledDataSource cpds = (ComboPooledDataSource) sce.getServletContext().getAttribute("hr.fer.zemris.dbpool");
        if (cpds != null) {
            try {
                DataSources.destroy(cpds);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
