package hr.fer.zemris.java.p12.dao;

import hr.fer.zemris.java.p12.model.Poll;
import hr.fer.zemris.java.p12.model.PollOption;

import java.util.List;

/**
 * Interface which models a DAO
 */
public interface DAO {
    /**
     * Method which inserts given poll into Polls table
     * @param poll given poll
     * @throws DAOException if something went wrong
     */
    void addNewPoll(Poll poll) throws DAOException;

    /**
     * Method which adds a pollOption to the PollOptions table
     * @param pollOption given pollOption
     * @throws DAOException if something went wrong
     */
    void addNewPollOption(PollOption pollOption) throws DAOException;

    /**
     * Method which reads all polls from the Poll table and returns them as a list
     * @return all polls from Poll table
     * @throws DAOException if something went wrong
     */
    List<Poll> getAllPolls() throws DAOException;

    /**
     * Method which returns a poll with given id
     * @param id given id
     * @return poll with given id
     * @throws DAOException if something went wrong
     */
    Poll getPoll(int id) throws DAOException;

    /**
     * Method which returns all PollOptions with given pollId
     * @param pollId given pollId
     * @return List of PollOptions with given pollId
     * @throws DAOException if something went wrong
     */
    List<PollOption> getAllOptions(int pollId) throws DAOException;

    /**
     * Method which adds a vote to the PollOption with given id
     * @param id given id
     * @throws DAOException if something went wrong
     */
    void addVote(int id) throws DAOException;

    /**
     * Method which returns poll option for given poll option id
     * @param optionID poll option id
     * @return poll option
     * @throws DAOException if something went wrong
     */
    PollOption getPollOption(int optionID) throws DAOException;
}
