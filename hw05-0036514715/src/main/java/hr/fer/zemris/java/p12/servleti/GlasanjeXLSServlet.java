package hr.fer.zemris.java.p12.servleti;

import hr.fer.zemris.java.p12.dao.DAOProvider;
import hr.fer.zemris.java.p12.model.PollOption;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Servlet which generates an XLS file with all options, their votes and their representative links for a specific Poll
 * Receives pollID as parameter
 */
@WebServlet("/servleti/glasanje-xls")
public class GlasanjeXLSServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pollIDString = req.getParameter("pollID");
        if (pollIDString == null) {
            req.setAttribute("message", "Didn't receive pollID");
            req.getRequestDispatcher("/WEB-INF/pages/error.jsp")
                    .forward(req, resp);
            return;
        }

        int pollID = Integer.parseInt(pollIDString);
        List<PollOption> options = DAOProvider.getDao().getAllOptions(pollID);
        options = options.stream().sorted(Comparator.comparing(PollOption::getVotesCount).reversed()).collect(Collectors.toList());

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Rezultati glasanja");
        HSSFRow head = sheet.createRow((short) 0);

        head.createCell((short) 0).setCellValue("Option Title");
        head.createCell((short) 1).setCellValue("Votes");
        head.createCell((short) 2).setCellValue("Option Link");

        for (int i = 0; i < options.size(); i++) {
            PollOption option = options.get(i);
            HSSFRow row = sheet.createRow((short) i + 1);
            row.createCell((short) 0).setCellValue(option.getOptionTitle());
            row.createCell((short) 1).setCellValue(option.getVotesCount());
            row.createCell((short) 2).setCellValue(option.getOptionLink());
        }

        resp.setContentType("application/vnd.ms-excel");
        resp.setHeader("Content-Disposition", "attachment; filename=\"table.xls\"");
        workbook.write(resp.getOutputStream());
    }
}
