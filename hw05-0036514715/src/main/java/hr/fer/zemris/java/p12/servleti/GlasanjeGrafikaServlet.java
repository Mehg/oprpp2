package hr.fer.zemris.java.p12.servleti;

import hr.fer.zemris.java.p12.dao.DAOProvider;
import hr.fer.zemris.java.p12.model.Poll;
import hr.fer.zemris.java.p12.model.PollOption;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Servlet which creates a PieChart from votes for a specific poll
 * Receives a pollID as a parameter
 */
@WebServlet("/servleti/glasanje-grafika")
public class GlasanjeGrafikaServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("pollID") == null) {
            req.setAttribute("message", "Didn't receive pollID");
            req.getRequestDispatcher("/WEB-INF/pages/error.jsp")
                    .forward(req, resp);
        }

        int pollID = Integer.parseInt(req.getParameter("pollID"));
        Poll poll = DAOProvider.getDao().getPoll(pollID);
        List<PollOption> options = DAOProvider.getDao().getAllOptions(pollID);
        options = options.stream().sorted(Comparator.comparing(PollOption::getVotesCount).reversed()).collect(Collectors.toList());

        DefaultPieDataset dataset = new DefaultPieDataset();
        for (PollOption option : options) {
            dataset.setValue(option.getOptionTitle(), option.getVotesCount());
        }

        JFreeChart chart = ChartFactory.createPieChart(
                poll.getTitle(),
                dataset,
                true,
                true,
                false
        );

        BufferedImage img = chart.createBufferedImage(500, 270);

        resp.setContentType("image/png");
        ImageIO.write(img, "png", resp.getOutputStream());
    }
}
