package hr.fer.zemris.java.p12.servleti;

import hr.fer.zemris.java.p12.dao.DAOProvider;
import hr.fer.zemris.java.p12.model.Poll;
import hr.fer.zemris.java.p12.model.PollOption;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Servlet which returns a list of options for a specific poll
 * Receives pollID
 */
@WebServlet("/servleti/glasanje")
public class GlasanjeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("pollID") == null) {
            req.setAttribute("message", "Didn't receive pollID");
            req.getRequestDispatcher("/WEB-INF/pages/error.jsp")
                    .forward(req, resp);
        }

        int id = Integer.parseInt(req.getParameter("pollID"));
        Poll poll = DAOProvider.getDao().getPoll(id);

        List<PollOption> pollOptions = DAOProvider.getDao().getAllOptions(id);

        req.setAttribute("poll", poll);
        req.setAttribute("options", pollOptions);

        req.getRequestDispatcher("/WEB-INF/pages/glasanjeIndex.jsp").forward(req, resp);
    }
}
